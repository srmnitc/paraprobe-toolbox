# paraprobe-toolbox

The paraprobe-toolbox is a collection of open-source tools for efficient analyses of point cloud data where each point can
represent atoms or molecular ions. A key application of the toolbox has been for research in the field of Atom Probe Tomography
(APT) and related Field Ion Microscopy (FIM). The toolbox does not replace but complements existent software tools in this
research field. Given its capabilities of handling points as objects with properties and enabling analyses of the spatial
arrangement of and intersections between three-dimensional geometric primitives, the software can equally be used for analyzing
data in Materials Science and Engineering. A documentation of the tools is available https://paraprobe-toolbox.readthedocs.io/en/latest/

## Version history:
**v0.5*
Completely refactored NeXus, code cleanup, new feature supporting exporting of all individual delocalized fields for each iontype, many bugfixes, instructions and test with Intel oneAPI compiler, all examples updated, refactoring to enable building with conda based on feedback from @srmnitc (NFDI-MatWerk/MPIE).

**v0.4.1**
Bug fixes

**v0.4**
Bug fixes, a new HDF5 wrapper supporting compression, NeXus data schemas for all input and output files.  

**v0.3.1**
A version reflecting development for the NOMAD OASIS/FAIRmat project in September 2022.  

**v0.3**, the version with tools for delocalization, iso-surface analyses, mesh intersections  
https://doi.org/10.48550/arXiv.2205.13510  

**v0.2**, a specific release with CPU/GPU/MPI/OpenMP tools for doing crystal structure analyses  
https://doi.org/10.1107/S1600576721008578  

**v0.1**, the version published with the detailed paper about the paraprobe-toolbox and its methods  
https://doi.org/10.1038/s41524-020-00486-1  
https://gitlab.mpcdf.mpg.de/mpie-aptfim-toolbox/paraprobe  
https://paraprobe-toolbox.readthedocs.io  

**Initial ideas**, which thanks to motivating questions by Andrew Breen (UNSW Sydney), sparked my interest in atom probe  
https://github.com/mkuehbach/PARAPROBE  
https://doi.org/10.1017/S1431927619002228  

Maybe of interest to some of you, related work with F. Roters which motivated the toolbox as well.  
https://dx.doi.org/10.1088/1361-651X/ab7f8c  
https://doi.org/10.1007/978-3-319-48770-0_6  

## Getting started:

1. Get the code from an archive or its GitLab repository via cloning it from gitlab using the following command:
   git clone --recurse-submodules https://gitlab.com/paraprobe/paraprobe-toolbox.git.
   Once cloned, go into the paraprobe-toolbox directory. This is the so-called paraprobe-toolbox root directory, i.e. the home of the toolbox.
   Make the scripts in there executable chmod +x \*.sh
2. Inspect the steps in PARAPROBE.Step01.InstallOsDeps.sh which guides you through the installation of the GNU compiler suite,
   the Message Passing Interface and other required third-party libraries at the operation system level.
   This step will require administrator privileges as compiler, MPI, and some low-level numerical libraries are not part
   of a standard Linux installation (at least as it reads for Ubuntu v18.x, v20.x, and v22.x).
3. Make sure to have a recent version of conda installed. Using miniconda3 and making a fresh environment is recommended.
   Good manuals on how to install miniconda are available online. The Digital Ocean documentations from e.g. Lisa Tagliaferri
   are one good guide to get you started. Keep in mind that anaconda is not exclusively useful for the paraprobe-toolbox only.
   Conda is nowadays the preferred way how to do development in Python and isn't this what many of us like to do?
   Once you have miniconda installed, you should follow the steps in PARAPROBE.Step02.CondaWithJupyterLab.sh.
   By default, the installation will use a CPU-parallelism-only version of the toolbox.
4. Follow the steps in the PARAPROBE.Step03.InstallThirdPartyDeps.sh manual.
   This will compile and configure local versions of relevant libraries, like HDF5, CGAL, and others.
   The configuration file in code/PARAPROBE.Dependencies.cmake points to the locations which cmake will need when
   configuring and build the C/C++ executables. The toolbox is a combination of multiple tools.
   Some tools are written in Python. All other tools are written in C/C++.
5. Execute the PARAPROBE.Step04.BuildTools.sh script (e.g. source PARAPROBE.Step04.BuildTools.sh).
   This step will compile all paraprobe-toolbox C/C++ executables so that these can be used with jupyter-lab or Python scripts.
6. Execute the PARAPROBE.Step05.InstallTools.sh script (e.g. source PARAPROBE.Step05.InstallTools.sh).
   This will copy all paraprobe-toolbox C/C++ executables in the default directory (paraprobe-toolbox/code/).
   The examples/teaching material is configured such that it expects the executables inside the code directory.
7. Inspect the PARAPROBE.Step06.GetExamples.sh script which will point you to example data to get you started.
8. While still being in the home directory paraprobe-toolbox, start a jupyter-lab session via jupyter-lab (e.g. jupyter-lab).
   An URL will be displayed in the console that you can copy and paste into your browser to interact with jupyter-lab and use
   it to spawn paraprobe-toolbox workflows. You are ready now and can analyze your own data.
   If you are using Windows10/11 and WSL2, it may happen that the console reports below the relevant link:
   *This command cannot be run due to the error: The system cannot find the file specified*.
   This should not be a problem though and in newer versions of Win/WSL2 one can even click the link and get redirected to the page.
   Typically the jupyter notebook is served via the following page localhost:8888/lab.
9. It is recommended that you inspect the paraprobe-toolbox/teaching directory, especially example_analyses/usa_denton_smith
   or usa_portland_wang to get started. The teaching directory provides a directory structure where you can place your
   individual data. Most importantly, the teaching directory offers a set of example_analyses, workflows ready to be
   used to show and explain some of the analyses which you can do with the tools in the paraprobe-toolbox.
   For the sake of reducing the size of the installation it is always worthwhile to inspect the README.md documents inside
   respective example_data directories. These give hints where certain raw data files can be downloaded as the project's
   GitLab space is limited.

## Examples:
The paraprobe-toolbox comes with a teaching directory (paraprobe-toolbox/teaching/) which has many examples how to
use the tools of the toolbox. These examples show different workflows, exemplified as interactively executable jupyter
notebooks which bring everything together. That is Python/C++ standalone tools/JupyterLab and NeXus/HDF5 for storing
configurations and numerical data, not just somehow into HDF5 but structured according to data schemas for
each task and tool.

## Questions, comments, suggestions, problems ?
Feel free to contact me. We wish you all the best and success with your atom probe analyses.

## How to cite:
Only the code in this repository is under active development.
All utilization of code portions or ideas from it should be cited::  

    Markus Kühbach, Vitor Vieira Rielli, Sophie Primig, Alaukik Saxena,  
    David Mayweg, Benjamin Jenkins, Stoichkov Antonov, Alexander Reichmann,  
    Stefan Kardos, Lorenz Romaner, Sandor Brockhauser  
    *On Strong-Scaling and Open-Source Tools for High-Throughput*  
    *Quantification of Material Point Cloud Data: Composition Gradients,*  
    *Microstructural Object Reconstruction, and Spatial Correlations*  
    https://doi.org/10.48550/arXiv.2205.13510  

## License:
The toolbox uses third-party tools with an open license for academic purposes.

## Supported community file formats:
The toolbox has readers for POS, EPOS, (AMETEK/Cameca) APT file format, RRNG, and RNG file formats.
Since v0.3.1 the tools support examples for NOMAD OASIS working with NXapm NeXus/HDF5 files.
The reference documentation for these data schemas is available as a git submodule.
Specifically the submodule data are located in code/thirdparty/mandatory/nexus
