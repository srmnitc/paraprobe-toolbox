#!/bin/bash

# DEFAULT/MANDATORY/CPU-PARALLEISM-ONLY INSTALLATION ROUTE to install and use every tool of the toolbox
# make sure to have conda activate i.e. execute within the conda base environment
conda create -n paraprobe-toolbox-py3.10 -c conda-forge python=3.10 jupyterlab-h5web jupyterlab hdbscan gitpython nodejs ase radioactivedecay pandas sphinx

# activate/i.e. use this environment
conda activate paraprobe-toolbox-py3.10

# make a few more installations in this specific environment
python3 -m pip install --upgrade pip
python3 -m pip install ecdf pytictoc ifes-apt-tc-data-modeling>=0.2.1 python-docs-theme xmltodict odfpy pylint


# ALTERNATIVE/OPTIONAL/CPU-AND-GPU-PARALLELISM INSTALLATION ROUTE
# USING THIS INSTALLATION ROUTE MAKES SENSE ONLY if you would like to use paraprobe-clusterer AND here its interface to NVidia's RAPIDSAI package
# USING THIS INSTALLATION ROUTE MAKES SENSE ONLY when you have an NVidia GPU. ONLY IN THIS CASE, you should follow this alternative installation route!
# make sure to have conda activate i.e. execute within the conda base environment
# DEPENDING ON YOUR SPECIFIC GPU and DRIVERS installed this installation line may need modifications
# conda create -n paraprobe-toolbox-r23.12-cu12.0-py3.10 -c rapidsai -c conda-forge -c nvidia rapids=23.12 python=3.10 cuda-version=12.0

# activate/i.e. use this environment
# conda activate paraprobe-toolbox-r23.12-cu12.0-py3.10

# conda install -c conda-forge jupyterlab-h5web jupyterlab hdbscan gitpython nodejs ase radioactivedecay pandas sphinx


# make a few more installations in this specific environment
# python3 -m pip install --upgrade pip
# python3 -m pip install ecdf pytictoc ifes-apt-tc-data-modeling>=0.2.1 python-docs-theme xmltodict odfpy pylint

# YOUR ENVIRONMENT IS READY

# SOME HINTS to solve some issues which you may encounter or not
# if collapsible headings are not working install it via the extension panel, rebuild and restart the kernel
# but this should no longer be an issue with recent jupyterlab versions

# make sure that your environment is active in your shell session, to be safe close the console and start it again
# go to your location of the paraprobe-toolbox

# test if jupyter-lab works successfully, this is also how you can start the service
jupyter-lab --NotebookApp.token=''
# by default the notebook will be served on localhost:8888 so just type this address in your browser
# for WSL typing jupyter-lab will usually leave you with a link on the console stating that the
# jupyter server was started. You can then click the link (e.g. with holding Ctrl and clicking the link)
# this will redirect you to the browser and spin up the jupyter-lab for you then

# IF YOU ARE DONE and would like to do something else in say another environment
# consider to deactivate the paraprobe environment
conda deactivate
# in this way you will again be in the base environment
