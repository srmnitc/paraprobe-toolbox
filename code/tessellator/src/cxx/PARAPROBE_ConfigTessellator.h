/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_CONFIG_TESSELLATOR_H__
#define __PARAPROBE_CONFIG_TESSELLATOR_H__

#include "../../../utils/src/cxx/PARAPROBE_ToolsBaseHdl.h"

/*
enum SPATIAL_SPLITTING_METHOD {
	EQUAL_COUNTS,
	EQUAL_WEIGHTS
};
*/

struct edge_distances_info
{
	string h5fn;					//hdf5 file where to read distance values from
	string dsnm;					//dataset name where to read distance values from
	edge_distances_info(): h5fn(""), dsnm("") {}
	edge_distances_info( const string _h5fn, const string _dsnm  ) :
		h5fn(_h5fn), dsnm(_dsnm) {}
};

ostream& operator << (ostream& in, edge_distances_info const & val);


struct tess_task_info
{
	apt_uint taskid;					//task ID
	bool hasVolume;						//compute volume of tessellated cells
	bool hasNeighbors;					//compute (neighbors) of tessellated cells
	bool hasGeometry;					//compute and cache geometry of cells
	bool hasWeinbergVector;				//compute Weinberg pvector for cell
	bool hasEdgeThresholds; 			//compute for cell facet an attribute
										//that tells when the facet will form a portion of the mesh of an edge of the dataset

	tess_task_info() : taskid(UMX),
		hasVolume(true), hasNeighbors(false),
		hasGeometry(false), hasWeinbergVector(false),
		hasEdgeThresholds(false) {}
};

ostream& operator << (ostream& in, tess_task_info const & val);


class ConfigTessellator
{
public:
	static bool read_config_from_nexus( const string nx5fn );

	static string InputfileDataset;
	static string ReconstructionDatasetName;
	static string MassToChargeDatasetName;
	static string InputfileIonTypes;
	static string IonTypesGroupName;

	static edge_distances_info InputfileIonToEdgeDistances;

	//add ROI filtering and sub-sampling functionalities
	static WINDOWING_METHOD WindowingMethod;
	static vector<roi_sphere> WindowingSpheres;
	static vector<roi_rotated_cylinder> WindowingCylinders;
	static vector<roi_rotated_cuboid> WindowingCuboids;
	static vector<roi_polyhedron> WindowingPolyhedra;
	//sub-sampling
	static lival<apt_uint> LinearSubSamplingRange;
	//match filters
	static match_filter<unsigned char> IontypeFilter;
	static match_filter<unsigned char> HitMultiplicityFilter;

	static vector<tess_task_info> TessellationTasks;

	//sensible defaults
	static apt_real GuardZoneFactor;
	static apt_int IonsPerBlock;
};

#endif
