#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
# Python class to be used in Jupyter notebooks for creating
# HDF5 configuration files for paraprobe-intersector

from string import digits

import numpy as np

from utils.src.python.numerics import EPSILON
from utils.src.python.hashing import get_file_hashvalue
from utils.src.python.nodeinfo import NodeInfo
from utils.src.python.supertool import ParmsetupBase, ParmsetupTaskBase


class VolumeFeatureSubSet:
    """##MK"""

    def __init__(self,
                 feature_type: str = "",
                 fpath: str = "",
                 groupname_geometry: str = "",
                 feature_identifier=None):
        if not isinstance(feature_type, str):
            raise TypeError("Argument feature_type needs to be a string!")
        allowed = ["objects_far_from_edge", "objects_close_to_edge",
                   "proxies_far_from_edge", "proxies_close_to_edge",
                   "other"]
        if feature_type not in allowed:
            raise ValueError(f"Argument feature_type needs to be in {allowed}!")
        if not isinstance(fpath, str):
            raise TypeError("Argument fpath needs to be a string!")
        if fpath == "":
            raise ValueError("Argument fpath needs to be a non-empty string!")
        if not isinstance(groupname_geometry, str):
            raise TypeError("Argument groupname_geometry_prefix needs to be a string!")
        if groupname_geometry == "":
            raise ValueError("Argument groupname_geometry_prefix needs to be a non-empty string!")
        if not isinstance(feature_identifier, np.ndarray):
            raise TypeError("Argument feature_identifier needs to be a np.ndarray!")
        if feature_identifier.dtype != np.uint32:
            raise TypeError("Argument feature_identifier needs to be a np.uint32 array!")
        if len(feature_identifier) < 1 or np.shape(feature_identifier)[0] == 0:
            raise ValueError("Argument feature_identifier needs to be a (N >= 1,) array!")
        self.cfg = {}
        self.cfg["/@NX_class"] = NodeInfo("NXserialized")
        self.cfg["/feature_type"] = NodeInfo(feature_type)
        self.cfg["/type"] = NodeInfo("file")
        self.cfg["/path"] = NodeInfo(fpath)
        self.cfg["/checksum"] = NodeInfo(get_file_hashvalue(fpath))
        self.cfg["/algorithm"] = NodeInfo("sha256")
        self.cfg["/geometry"] = NodeInfo(groupname_geometry)
        self.cfg["/feature_identifier"] \
            = NodeInfo(feature_identifier, "", np.uint32)


class VolumeFeatureSet:
    """##MK"""
    def __init__(self, set_name: str = "", set_identifier: int = -1):
        if not isinstance(set_name, str):
            raise TypeError("Argument set_name needs to be a string!")
        allowed = ["current", "next"]
        if set_name not in allowed:
            raise ValueError(f"Argument set_name needs to be in {allowed}!")
        if not isinstance(set_identifier, int):
            raise TypeError("Argument set_identifier needs to be an integer!")
        if set_identifier < 0:
            raise ValueError("Argument set_identifier needs to be 0 or positive!")
        self.name = set_name
        self.cfg = {}
        self.cfg["/@NX_class"] = NodeInfo("NXobject")
        self.cfg["/set_identifier"] = NodeInfo(set_identifier, "", np.uint32)
        self.feature_types = {}

    def add_feature_type(self, feature_sub_set):
        if not isinstance(feature_sub_set, VolumeFeatureSubSet):
            raise TypeError("Argument feature_sub_set needs to be a VolumeFeatureSubSet!")
        if len(self.feature_types.keys()) > 4:
            raise KeyError("At most four feature_types are supported!")
        sub_set_id = len(self.feature_types.keys()) + 1  # identifier start at 1
        self.feature_types[sub_set_id] = feature_sub_set


class VolumeVolumeTask(ParmsetupTaskBase):
    """Metadata of a task for spatial statistics."""

    def __init__(self):
        super().__init__("intersector", "")
        self.cfg = {}
        self.cfg["/@NX_class"] = NodeInfo("NXapm_paraprobe_tool_config")
        self.cfg["/intersection_detection_method"] = NodeInfo("shared_ion")
        self.report_named_option("/has_current_to_next_links", True)
        self.report_named_option("/has_next_to_current_links", True)
        self.set_analyze_overlap(True)
        self.set_analyze_proximity(True)
        self.curr_set = None
        self.next_set = None

    def set_analyze_overlap(self, boolean):
        """Define if to analyze intersections."""
        self.report_named_option("/analyze_intersection", boolean)

    def set_analyze_proximity(self, boolean):
        """Define if to analyze proximity of objects."""
        self.report_named_option("/analyze_proximity", boolean)

    def set_analyze_coprecipitation(self, boolean):
        """Define if to analyze coprecipitation (graph analytics)."""
        self.report_named_option("/analyze_coprecipitation", boolean)

    def set_threshold_proximity(self, distance: float = EPSILON):
        """Define the maximum distance to consider to objects within proximity."""
        if not isinstance(distance, float):
            raise TypeError("Argument distance needs to be a float !")
        if distance < EPSILON:
            raise ValueError(f"Argument distance needs to be at least as large as {EPSILON} nm !")
        self.cfg["/threshold_proximity"] = NodeInfo(distance, "nm", np.float32)

    def add_feature_sets(self, current_set, next_set):
        """##MK"""
        if not isinstance(current_set, VolumeFeatureSet):
            raise TypeError("Argument current_set needs to be a VolumeFeatureSet !")
        if current_set.name != "current":
            raise ValueError("Argument current_set.name needs to be current !")
        if not isinstance(next_set, VolumeFeatureSet):
            raise TypeError("Argument next_set needs to be a VolumeFeatureSet !")
        if next_set.name != "next":
            raise ValueError("Argument next_set.name needs to be next !")
        self.curr_set = current_set
        self.next_set = next_set


class ParmsetupIntersector(ParmsetupBase):
    """Be a wizard by creating a config NeXus file for the intersector tool."""

    def __init__(self):
        super().__init__("intersector", "v_v_spatial_correlation1")
        self.number_of_tasks = 0

    def add_task(self, task_type):
        """Register the task as a process in the task list."""
        if not isinstance(task_type, VolumeVolumeTask):
            raise TypeError("Argument task_type needs to be a VolumeVolumeTask !")
        self.number_of_tasks += 1
        self.prefix = f"/ENTRY[entry{self.entry_id}]/APM_PARAPROBE_TOOL_CONFIG" \
                      f"[{self.taskname.rstrip(digits)}{self.number_of_tasks}]"
        # metadata of the task
        for trailing_path, value in task_type.cfg.items():
            full_path = f"{self.prefix}{trailing_path}"
            if full_path in self.nodes:
                raise KeyError(f"Keyword {full_path} was already defined !")
            self.nodes[full_path] = value  # will this induce a deep-copy?

        # metadata of the current set ...
        for trailing_path, value in task_type.curr_set.cfg.items():
            full_path = f"{self.prefix}/current_set{trailing_path}"
            if full_path in self.nodes:
                raise KeyError(f"Keyword {full_path} was already defined !")
            self.nodes[full_path] = value
        # ... and details for all its feature sub_sets
        full_path = f"{self.prefix}/current_set/number_of_feature_types"
        self.nodes[full_path] \
            = NodeInfo(len(task_type.curr_set.feature_types.keys()), "", np.uint32)
        for key, sub_set in task_type.curr_set.feature_types.items():
            for trailing_path, value in sub_set.cfg.items():
                full_path = f"{self.prefix}/current_set/feature{key}{trailing_path}"
                if full_path in self.nodes:
                    raise KeyError(f"Keyword {full_path} was already defined !")
                self.nodes[full_path] = value

        # metadata of the next set ...
        for trailing_path, value in task_type.next_set.cfg.items():
            full_path = f"{self.prefix}/next_set{trailing_path}"
            if full_path in self.nodes:
                raise KeyError(f"Keyword {full_path} was already defined !")
            self.nodes[full_path] = value
        # ... and details for all its feature sub_sets
        full_path = f"{self.prefix}/next_set/number_of_feature_types"
        self.nodes[full_path] \
            = NodeInfo(len(task_type.curr_set.feature_types.keys()), "", np.uint32)
        for key, sub_set in task_type.next_set.feature_types.items():
            for trailing_path, value in sub_set.cfg.items():
                full_path = f"{self.prefix}/next_set/feature{key}{trailing_path}"
                if full_path in self.nodes:
                    raise KeyError(f"Keyword {full_path} was already defined !")
                self.nodes[full_path] = value

    def configure(self, simid, verbose=False):
        """Write config file."""
        if not isinstance(simid, int) or simid <= 0:
            raise ValueError("Argument simid needs to be an integer > 0 !")
        self.nodes[f"/ENTRY[entry{self.entry_id}]/number_of_tasks"] \
            = NodeInfo(self.number_of_tasks, "", np.uint32)
        if verbose is True:
            for key, val in self.nodes.items():
                print(f"{key}\t{val.value}")

        print("Writing configuration file ...")
        return super().write_config_to_nexus(self.toolname, simid, self.nodes)
