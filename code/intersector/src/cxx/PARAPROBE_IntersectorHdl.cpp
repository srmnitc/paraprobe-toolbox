/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/


#include "PARAPROBE_IntersectorHdl.h"

VolumeFeatureSet::VolumeFeatureSet()
{
	identifier = UMX;
	objs = vector<polyhedron_triangulated_surface_mesh>();
	bvh_objs = NULL;
}


VolumeFeatureSet::~VolumeFeatureSet()
{
	identifier = UMX;
	clear_objs();
	clear_bvh();
}


void VolumeFeatureSet::clear_objs()
{
	cout << "releasing objs..." << "\n";
	for( size_t i = 0; i < objs.size(); i++ ) {
		//cout << "release msh " << i << "\n";
		if ( objs[i].msh != NULL ) {
			delete objs[i].msh; objs[i].msh = NULL;
		}
		//cout << "release bvh " << i << "\n";
		if ( objs[i].bvh != NULL ) {
			delete objs[i].bvh; objs[i].bvh = NULL;
		}
	}
	objs = vector<polyhedron_triangulated_surface_mesh>();
	cout << "objs released" << "\n";
}


void VolumeFeatureSet::clear_bvh()
{
	if ( bvh_objs != NULL ) {
		delete bvh_objs; bvh_objs = NULL;
	}
}


bool VolumeFeatureSet::read_relevant_v_input( v_feature_set & info )
{
	//load current objects
	double tic = MPI_Wtime();
	double toc = tic;

	clear_objs();

	cout << "read_relevant_v_input set_identifier " << info.set_identifier << "\n";
	for( auto it = info.feature_types.begin(); it != info.feature_types.end(); it++ ) {
		cout << "read_relevant_v_input feature_type " << it->type << "\n";
		cout << "filename " << it->filename << "\n";
		cout << "grpnm_geo_prefix " << it->grpnm_geo_prefix << "\n";

		HdfFiveSeqHdl h5r = HdfFiveSeqHdl( it->filename );
		string grpnm = it->grpnm_geo_prefix;
		for( auto obj_id = it->identifier.begin(); obj_id != it->identifier.end(); obj_id++ ) {

			string subgrpnm = grpnm + "/object" + to_string(*obj_id) + "/polyhedron";

			roi_polyhedron* ply_mesh = NULL;
			ply_mesh = new roi_polyhedron;
			if ( ply_mesh != NULL ) {
				ply_mesh->roiID = *obj_id;
				ply_mesh->ownerID = *obj_id; //##MK::marking there is no owner
				ply_mesh->m1 = 0x01;

				vector<apt_real> real;
				if ( h5r.nexus_read( subgrpnm + "/vertices", real ) == MYHDF5_SUCCESS ) {
					ply_mesh->vrts_surface.reserve( real.size() / 3 );
					for( size_t i = 0; i < real.size(); i += 3 ) {
						ply_mesh->vrts_surface.push_back( p3d( real[i+0], real[i+1], real[i+2] ) );
					}
					real = vector<apt_real>();
					//compute axis-aligned bounding box (AABB) to the polyhedron
					ply_mesh->bvh_volume = aabb3d();
					for( auto kt = ply_mesh->vrts_surface.begin(); kt != ply_mesh->vrts_surface.end(); kt++ ) {
						ply_mesh->bvh_volume.possibly_enlarge_me( *kt );
					}
					ply_mesh->bvh_volume.add_epsilon_guard();
					ply_mesh->bvh_volume.scale();

					vector<apt_uint> uint;
					if ( h5r.nexus_read( subgrpnm + "/faces", uint ) == MYHDF5_SUCCESS ) {
						ply_mesh->fcts_surface.reserve( uint.size() / 3 );
						for( size_t i = 0; i < uint.size(); i += 3 ) {
							ply_mesh->fcts_surface.push_back( tri3u( uint[i+0], uint[i+1], uint[i+2]) );
						}
						uint = vector<apt_uint>();
					}
					else {
						cerr << "Reading " << subgrpnm << "/faces failed, WARNING object " << *obj_id << " will not be in set !" << "\n";
						delete ply_mesh; ply_mesh = NULL; continue;
					}
				}
				else {
					cerr << "Reading " << subgrpnm << "/vertices failed, WARNING object " << *obj_id << " will not be in set !" << "\n";
					delete ply_mesh; ply_mesh = NULL; continue;
				}
			}

			objs.push_back( polyhedron_triangulated_surface_mesh() );
			objs.back().msh = ply_mesh;
			objs.back().bvh = NULL;
			//EVAPID_SHARED
			if ( h5r.nexus_read( subgrpnm + "/ion_identifier", objs.back().support ) == MYHDF5_SUCCESS ) {
				if ( objs.back().support.size() > 0 ) {
					objs.back().support_dict = set(
							objs.back().support.begin(), objs.back().support.end() );
					continue;
				}
				else {
					cerr << "Reading " << subgrpnm << "/ion_identifier, WARNING object " << *obj_id << " has no support !" << "\n";
					continue;
				}
			}
			else { //pop back
				cerr << "Reading " << subgrpnm << "/ion_identifier failed, WARNING object " << *obj_id << " will not be in set !" << "\n";
				if ( objs.back().msh != NULL ) {
					delete objs.back().msh; objs.back().msh = NULL;
				}
				objs.pop_back();
			}
		} //next object
	} //next feature_type

	cout << "objs.size() " << objs.size() << "\n";

	toc = MPI_Wtime();
	cout << "read_relevant_v_input for target " << " took " << (toc-tic) << " s" << "\n";
	return true;
}


bool VolumeFeatureSet::check_input()
{
	bool status = true;
	for( size_t idx = 0; idx < objs.size(); idx++ ) {
		if ( objs[idx].msh != NULL
				&& objs[idx].bvh == NULL
					&& objs[idx].support.size() > 0
						&& objs[idx].support_dict.size() > 0 ) {
			continue;
		}
		else {
			cerr << "set_identifier " << identifier << " polyhedron_triangulated_surface_mesh objs[" << idx << "] is invalid !" << "\n";
			status = false;
		}
	}
	return status;
}


bool VolumeFeatureSet::build_bvh()
{
	if ( bvh_objs != NULL ) {
		delete bvh_objs; bvh_objs = NULL;
	}

	bvh_objs = new Tree();
	if ( bvh_objs != NULL ) {
		for( apt_uint idx = 0; idx < (apt_uint) objs.size(); idx++ ) {
			bvh_objs->insertParticle( idx,
				trpl( objs[idx].msh->bvh_volume.xmi,
					  objs[idx].msh->bvh_volume.ymi,
					  objs[idx].msh->bvh_volume.zmi ),
				trpl( objs[idx].msh->bvh_volume.xmx,
					  objs[idx].msh->bvh_volume.ymx,
					  objs[idx].msh->bvh_volume.zmx)  );
		}
	}
	else {
		cerr << "bvh_objs == NULL !" << "\n"; return false;
	}

	cout << "bvh->getNodeCount() " << bvh_objs->getNodeCount() << "\n";
	return true;
}


intersectorHdl::intersectorHdl()
{
	clear_links();
}


intersectorHdl::~intersectorHdl()
{
	current_set.clear_objs();
	current_set.clear_bvh();
	next_set.clear_objs();
	next_set.clear_bvh();
}


void intersectorHdl::clear_links()
{
	c2n_links_i = vector<forward_link>();
	n2c_links_i = vector<forward_link>();
	c2n_links_p = vector<forward_link>();
	n2c_links_p = vector<forward_link>();
}


unsigned char intersectorHdl::v_v_core( polyhedron_triangulated_surface_mesh const & obj_a, polyhedron_triangulated_surface_mesh const & obj_b,
		const bool check_proximity, const apt_real threshold )
{
	for( auto it = obj_a.support.begin(); it != obj_a.support.end(); it++ ) {
		if( obj_b.support_dict.find(*it) == obj_b.support_dict.end() ) {
			//in most cases we do not expect to find an overlap
			continue;
		}
		else {
			return YES_VOLUMETRIC_OVERLAP;
		}
	}

	if ( check_proximity == true ) {
		//test proximity, currently a naive O(N^2), switch to PQP at some point, or use a binary tree for reducing costs of tests for triangles of obj_b
		if ( obj_a.msh != NULL && obj_b.msh != NULL ) {

			for( auto jt = obj_a.msh->fcts_surface.begin(); jt != obj_a.msh->fcts_surface.end(); jt++ ) {

				tri3d triangleA = tri3d( obj_a.msh->vrts_surface[jt->v1].x, obj_a.msh->vrts_surface[jt->v1].y, obj_a.msh->vrts_surface[jt->v1].z,
										 obj_a.msh->vrts_surface[jt->v2].x, obj_a.msh->vrts_surface[jt->v2].y, obj_a.msh->vrts_surface[jt->v2].z,
										 obj_a.msh->vrts_surface[jt->v3].x, obj_a.msh->vrts_surface[jt->v3].y, obj_a.msh->vrts_surface[jt->v3].z );

				for( auto kt = obj_b.msh->fcts_surface.begin(); kt != obj_b.msh->fcts_surface.end(); kt++ ) {

					tri3d triangleB = tri3d( obj_b.msh->vrts_surface[kt->v1].x, obj_b.msh->vrts_surface[kt->v1].y, obj_b.msh->vrts_surface[kt->v1].z,
											 obj_b.msh->vrts_surface[kt->v2].x, obj_b.msh->vrts_surface[kt->v2].y, obj_b.msh->vrts_surface[kt->v2].z,
											 obj_b.msh->vrts_surface[kt->v3].x, obj_b.msh->vrts_surface[kt->v3].y, obj_b.msh->vrts_surface[kt->v3].z );

					//hand over to PQP
					PQP_REAL PP[3] = {0.0, 0.0, 0.0};
					PQP_REAL QQ[3] = {0.0, 0.0, 0.0};
					PQP_REAL SS[3][3];
					SS[0][0] = triangleA.x1; SS[0][1] = triangleA.y1; SS[0][2] = triangleA.z1;
					SS[1][0] = triangleA.x2; SS[1][1] = triangleA.y2; SS[1][2] = triangleA.z2;
					SS[2][0] = triangleA.x3; SS[2][1] = triangleA.y3; SS[2][2] = triangleA.z3;

					PQP_REAL TT[3][3];
					TT[0][0] = triangleB.x1; TT[0][1] = triangleB.y1; TT[0][2] = triangleB.z1;
					TT[1][0] = triangleB.x2; TT[1][1] = triangleB.y2; TT[1][2] = triangleB.z2;
					TT[2][0] = triangleB.x3; TT[2][1] = triangleB.y3; TT[2][2] = triangleB.z3;

					//compute and check proximity
					apt_real distance = (apt_real) TriDist( PP, QQ, SS, TT );
					if ( distance > threshold ) { //most likely
						continue;
					}
					else {
						//breaking out here, because we are not interest in the absolute shortest possible distance
						//but just one triangle close enough suffices
						//no more testing for triangles of obj_b required
						return WITHIN_PROXIMITY;
					}
				}
			}
			return FAR_AWAY;
		}
		return UNCLEAR_SITUATION;
	}
	//implicit else, not yet returned so proximity was not checked for but also no overlap
	return NO_VOLUMETRIC_OVERLAP;
}


void intersectorHdl::analyze_compare_sets_current_to_next( v_v_spatial_correlation_task const & info )
{
	//check which polyhedral objects in objs_curr overlap or touch objects from objs_next
	//when using TETGEN_TETRAHEDRALIZE we compare overlapping tetrahedra
	//when using EVAPID_COMPARISON we evaluate if an object in curr has at least on point with the same ID as an supporting points of an object in next
	double tic = omp_get_wtime();

	c2n_links_i = vector<forward_link>();
	n2c_links_i = vector<forward_link>();
	c2n_links_p = vector<forward_link>();
	n2c_links_p = vector<forward_link>();

	#pragma omp parallel shared(c2n_links_i, n2c_links_i, c2n_links_p, n2c_links_p)
	{
		//thread-local buffer
		vector<forward_link> c2n_i; //volumetric intersection/overlap
		vector<forward_link> n2c_i;
		vector<forward_link> c2n_p; //proximity
		vector<forward_link> n2c_p;
		apt_real guardzone = info.proximity + EPSILON;

		#pragma omp for schedule(dynamic,1)
		for( size_t curr_idx = 0; curr_idx < current_set.objs.size(); curr_idx++ ) {
			//threads process different objects in parallel

			//first, use the least costly intersection test layer:
			//find with which AABB of next objects the AABB of the current object intersects if any

			hedgesAABB bvh_obj_curr(
						trpl(	current_set.objs[curr_idx].msh->bvh_volume.xmi - guardzone,
								current_set.objs[curr_idx].msh->bvh_volume.ymi - guardzone,
								current_set.objs[curr_idx].msh->bvh_volume.zmi - guardzone),
						trpl(	current_set.objs[curr_idx].msh->bvh_volume.xmx + guardzone,
								current_set.objs[curr_idx].msh->bvh_volume.ymx + guardzone,
								current_set.objs[curr_idx].msh->bvh_volume.zmx + guardzone)  );

			apt_uint curr_obj_id = current_set.objs[curr_idx].msh->roiID;

			vector<apt_uint> objs_next_candidx = next_set.bvh_objs->query( bvh_obj_curr );

			//cout << "curr_idx, evapid_comparison, objs_next_candidx.size() " << curr_idx << "\t\t" << objs_next_candidx.size() << "\n";
			//second, use a more costly intersection test layer but for relevant candidates only
			//inspect in-place for each candidate member of next_set overlap, and if not proximity only
			for( size_t cand_idx = 0; cand_idx < objs_next_candidx.size(); cand_idx++ ) {

				apt_uint nxt_idx = objs_next_candidx[cand_idx];

				unsigned char status = v_v_core( current_set.objs[curr_idx], next_set.objs[nxt_idx], info.IOAnalyzeProximity, info.proximity );

				//cout << (int) status << "\n";

				apt_uint next_obj_id = next_set.objs[nxt_idx].msh->roiID;

				switch(status)
				{
					case YES_VOLUMETRIC_OVERLAP:
					{
						c2n_i.push_back( forward_link(curr_obj_id, next_obj_id, LINK_TYPE_VOLUMETRIC_OVERLAP) );
						n2c_i.push_back( forward_link(next_obj_id, curr_obj_id, LINK_TYPE_VOLUMETRIC_OVERLAP) );
						//evidently these links are bi-directional and symmetric because if a intersects b, also the statement b intersects a is valid
						break;
					}
					case WITHIN_PROXIMITY:
					{
						c2n_p.push_back( forward_link(curr_obj_id, next_obj_id, LINK_TYPE_PROXIMITY) );
						n2c_p.push_back( forward_link(next_obj_id, curr_obj_id, LINK_TYPE_PROXIMITY) );
						break;
					}
					default:
					{
						break;
					}
				}

			} //test next candidate
		} //test next obj_curr

		//all threads done, now fuse results
		#pragma omp critical
		{
			c2n_links_i.insert( c2n_links_i.end(), c2n_i.begin(), c2n_i.end() );
			c2n_i = vector<forward_link>();
			n2c_links_i.insert( n2c_links_i.end(), n2c_i.begin(), n2c_i.end() );
			n2c_i = vector<forward_link>();

			c2n_links_p.insert( c2n_links_p.end(), c2n_p.begin(), c2n_p.end() );
			c2n_i = vector<forward_link>();
			n2c_links_p.insert( n2c_links_p.end(), n2c_p.begin(), n2c_p.end() );
			n2c_p = vector<forward_link>();
		} //fused and released thread-local results

	} //end of parallel region

	double toc = omp_get_wtime();
	cout << "analysis_v_v for task " << info.taskid << " done, took " << (toc-tic) << " s" << "\n";
}


void intersectorHdl::analyze_coprecipitates( v_v_spatial_correlation_task const & info )
{
	double tic = MPI_Wtime();

	size_t larger_than_uint32 = 93836549648400;
	cout << larger_than_uint32 << "\n";
	pair<apt_uint, apt_uint> pair_of_uint32 = unhash_key_to_pair( larger_than_uint32 );
	cout << pair_of_uint32.first << "; " << pair_of_uint32.second << "\n";
	size_t retval = hash_pair_to_key( pair_of_uint32 );
	cout << retval << "\n";
	//MK::the analysis_v_v compare members of set A with B, building such links is especially relevant
	//when we want to understand the spatial evolution of a feature across sets in the e.g. "time" domain/axis
	//i.e. given a set of shapes in set A at time t_i how do they compare to a set of shapes in set B at time t_{i+1}

	//however, coprecipitation combines multiple of such A to B comparisons with a subsequent cluster analysis
	//specifically we should compare how members of the joint set A and B (with each member having the time stamp)
	//as a mark and its triangulated surface mesh as another mark eventually are linkable to which members
	//from the set A+B (overlapping or via proximity). Here pairs between the same objects are omitted
	//for each member a 64-bit wide size_t key object is kept so that as the result of
	//the comparison we arrive at a clustering
	//i.e. which neo4jkey pairs belong to a cluster
	//the total number of cluster is then the total number of "coprecipitates"
	//all clusters with only one member (from either set A or B) are monoliths
	//all clusters with only two members (irrespective from A or B) are duplets
	//higher-order lets can be distinguished
	//if for instance set A is taken as the current set (gamma'')
	//and set B is taken as the next set (gamma') then different types
	//of coprecipitates can be distinguished
	//thanks to the clustering algorithm assignments are unique
	//monoliths can be gamma'' or gamma'
	//duplets can be gamma'' pairs, gamma' pairs, or gamma''/gamma' pairs
	//triplets can be 3x gamma'', 3x gamma', 1x gamma''/2x gamma', 2x gamma''/1x gamma'
	//higher order lets have even more possibilities

	//how to perform such an analysis in principle ?
	//build list of neo4jkey pairs
	//build BVH for meshes of all features/members of the current and the next set
	//run DB clustering of members of set A + B
	//report cluster statistics from the perspective of set A
	//report cluster statistics from the perspective of set B
	//cluster statistics from the perspective of set A + B
	//the latter is the sought after coprecipitation analysis

	if ( current_set.build_bvh() == true && next_set.build_bvh() == true ) {

		//initialize clustering algorithm
		//first argument of the map is a size_t representing a hash_pair_to_key, as many entries as members in set A + B
		//second argument of the map tells to which cluster the member belongs

		//map<size_t, pair<bool, apt_uint>> visited_clusterid;
		visited_clusterid = map<size_t, pair<bool, apt_uint>>();
		cout << "visited_clusterid.size() " << visited_clusterid.size() << "\n";
		//MK::realize that the DBScan clustering and all ID handling for curr and next objects is done on the objects' array idx !
		//not on the features' identifier, realize that these array indices (on current_set.objs) and next_set.objs will be resolved
		//later, using here the indices on the array directly allows to jump directly to respective pieces of information
		//otherwise it would be non trivial to retrieve the array positions of the objects in the objs_curr and
		//objs_next set respectively and thus difficult to retrieve the surface meshes

		for( apt_uint idx = 0; idx < (apt_uint) current_set.objs.size(); idx++ ) {
			size_t key = hash_pair_to_key( info.curr_set.set_identifier, idx ); //current_set.objs[idx].msh->roiID; //"timestep", feature_id/objid
			visited_clusterid[key] = pair<bool, apt_uint>( false, 0 );
		}
		cout << "visited_clusterid.size() " << visited_clusterid.size() << "\n";

		for( apt_uint idx = 0; (apt_uint) idx < next_set.objs.size(); idx++ ) {
			size_t key = hash_pair_to_key( info.next_set.set_identifier, idx ); //next_set.objs[idx].msh->roiID; //"timestep+1 ", feature_id/objid
			visited_clusterid[key] = pair<bool, apt_uint>( false, 0 );
		}
		cout << "visited_clusterid.size() " << visited_clusterid.size() << "\n";

		//initially all objs in the joint set objs_curr + objs_next have not been visited and have assigned cluster id 0
		/*
		cout << "key of set_identifier and feature_id, visited, counts" << "\n";
		for( auto dbgit = visited_clusterid.begin(); dbgit != visited_clusterid.end(); dbgit++ ) {
			cout << dbgit->first << "\t\t" << (int) dbgit->second.first << "\t\t" << dbgit->second.second << "\n";
		}
		*/

		cout << "Starting the clustering algorithm" << "\n";
		apt_uint clid = 1;
		apt_real guardzone = info.proximity + EPSILON;
		for( auto jt = visited_clusterid.begin(); jt != visited_clusterid.end(); jt++ ) {
			if ( jt->second.first == false ) { //joint set member no visited

				//cout << "visiting " << jt->first << "\n";
				//decode which member of either objs_curr or objs_next it is
				pair<apt_uint, apt_uint> state_objidx = unhash_key_to_pair(jt->first);
				jt->second.first = true; //mark P as visited

				vector<size_t> nbors; //array of keys, each encoding state and objs_* (curr/next) array index respectively

				apt_uint state = state_objidx.first; //a member of the sets curr or next
				if ( state == info.curr_set.set_identifier ) {
					apt_uint curidx = state_objidx.second;
					hedgesAABB bvh_obj_curr(
						trpl(	current_set.objs[curidx].msh->bvh_volume.xmi - guardzone,
								current_set.objs[curidx].msh->bvh_volume.ymi - guardzone,
								current_set.objs[curidx].msh->bvh_volume.zmi - guardzone  ),
						trpl(	current_set.objs[curidx].msh->bvh_volume.xmx + guardzone,
								current_set.objs[curidx].msh->bvh_volume.ymx + guardzone,
								current_set.objs[curidx].msh->bvh_volume.zmx + guardzone  ) );

					vector<apt_uint> tmp_nbors_curr = current_set.bvh_objs->query( bvh_obj_curr ); //find possible neighbors among members in current set
					for( auto iit = tmp_nbors_curr.begin(); iit != tmp_nbors_curr.end(); iit++ ) {
						//inspect neighbors of current set
						if ( *iit != curidx ) {

							unsigned char situation = v_v_core(
									current_set.objs[curidx],
									current_set.objs[*iit],
									info.IOAnalyzeProximity,
									info.proximity );
							if ( (situation == YES_VOLUMETRIC_OVERLAP)
									|| (info.IOAnalyzeProximity == true && situation == WITHIN_PROXIMITY) ) {
								size_t iit_key = hash_pair_to_key( info.curr_set.set_identifier, (apt_uint) *iit );
								nbors.push_back( iit_key );
								//cout << "iit_key " << iit_key << "\n";
							}
						}
					}
					tmp_nbors_curr = vector<apt_uint>();

					vector<apt_uint> tmp_nbors_next = next_set.bvh_objs->query( bvh_obj_curr );
					//find possible neighbors among members in next set
					for( auto jit = tmp_nbors_next.begin(); jit != tmp_nbors_next.end(); jit++ ) {
						//inspect neighbors of next set
						unsigned char situation = v_v_core(
								current_set.objs[curidx],
								next_set.objs[*jit],
								info.IOAnalyzeProximity,
								info.proximity );
						if ( (situation == YES_VOLUMETRIC_OVERLAP)
								|| (info.IOAnalyzeProximity == true && situation == WITHIN_PROXIMITY) ) {
							size_t jit_key = hash_pair_to_key( info.next_set.set_identifier, (apt_uint) *jit );
							nbors.push_back( jit_key );
							//cout << "jit_key " << jit_key << "\n";
						}
					}
					tmp_nbors_next = vector<apt_uint>();
				}
				else if ( state == info.next_set.set_identifier ) {
					apt_uint nxtidx = state_objidx.second;
					hedgesAABB bvh_obj_next(
						trpl(	next_set.objs[nxtidx].msh->bvh_volume.xmi - guardzone,
								next_set.objs[nxtidx].msh->bvh_volume.ymi - guardzone,
								next_set.objs[nxtidx].msh->bvh_volume.zmi - guardzone  ),
						trpl(	next_set.objs[nxtidx].msh->bvh_volume.xmx + guardzone,
								next_set.objs[nxtidx].msh->bvh_volume.ymx + guardzone,
								next_set.objs[nxtidx].msh->bvh_volume.zmx + guardzone  ) );

					vector<apt_uint> tmp_nbors_next = next_set.bvh_objs->query( bvh_obj_next );
					for( auto iit = tmp_nbors_next.begin(); iit != tmp_nbors_next.end(); iit++ ) {
						if ( *iit != nxtidx ) {
							unsigned char situation = v_v_core(
									next_set.objs[nxtidx],
									next_set.objs[*iit],
									info.IOAnalyzeProximity,
									info.proximity );
							if ( (situation == YES_VOLUMETRIC_OVERLAP)
									|| (info.IOAnalyzeProximity == true && situation == WITHIN_PROXIMITY) ) {
								size_t iit_key = hash_pair_to_key( info.next_set.set_identifier, (apt_uint) *iit );
								nbors.push_back( iit_key );
								//cout << "iit_key " << iit_key << "\n";
							}
						}
					}
					tmp_nbors_next = vector<apt_uint>();

					vector<apt_uint> tmp_nbors_curr = current_set.bvh_objs->query( bvh_obj_next );
					for( auto jit = tmp_nbors_curr.begin(); jit != tmp_nbors_curr.end(); jit++ ) {
						unsigned char situation = v_v_core(
								next_set.objs[nxtidx],
								current_set.objs[*jit],
								info.IOAnalyzeProximity,
								info.proximity );
						if ( (situation == YES_VOLUMETRIC_OVERLAP) || (info.IOAnalyzeProximity == true && situation == WITHIN_PROXIMITY) ) {
							size_t jit_key = hash_pair_to_key( info.curr_set.set_identifier, (apt_uint) *jit );
							nbors.push_back( jit_key );
							//cout << "jit_key " << jit_key << "\n";
						}
					}
					tmp_nbors_curr = vector<apt_uint>();
				}
				else {
					cerr << "Unexpected value for state_objid.first !" << "\n"; return;
				}
				//clid++; //next cluster, so clid == 0 means not assigned
				jt->second.second = clid; //add P to cluster

				//cout << "adding P to " << jt->first << "\n";
				//cout << "nbors.size() " << nbors.size() << "\n";
				//cout << "nbors are" << "\n";
				for( size_t key_idx = 0; key_idx < nbors.size(); key_idx++ ) {
					size_t key = nbors[key_idx];
					map<size_t, pair<bool, apt_uint>>::iterator nb = visited_clusterid.find(key);
					//cout << "nbors[key_idx] " << key << "\n";

					if ( nb != visited_clusterid.end() ) {
						if ( nb->second.first == false ) { //if P' is not visited
							nb->second.first = true; //mark P' as visited because it is within an either proximity or next relationship
							if ( nb->second.second == 0 ) {
								nb->second.second = clid;
							}
							else {
								cerr << "Invalid clusterid reassociation attempt !" << "\n"; return;
							}

							//region query around the neighbor
							//if ( nb->first != key ) { //nb->first has to be *objit, check if this is the case here for debugging purposes
							//	cerr << "Inconsistent ID management !" << "\n"; return;
							//}
							pair<apt_uint, apt_uint> nb_state_objidx = unhash_key_to_pair( nb->first );

							apt_uint nb_state = nb_state_objidx.first;
							if ( nb_state == info.curr_set.set_identifier ) {
								apt_uint nb_curidx = nb_state_objidx.second;
								hedgesAABB nb_bvh_obj_curr(
									trpl(	current_set.objs[nb_curidx].msh->bvh_volume.xmi - guardzone,
											current_set.objs[nb_curidx].msh->bvh_volume.ymi - guardzone,
											current_set.objs[nb_curidx].msh->bvh_volume.zmi - guardzone  ),
									trpl(	current_set.objs[nb_curidx].msh->bvh_volume.xmx + guardzone,
											current_set.objs[nb_curidx].msh->bvh_volume.ymx + guardzone,
											current_set.objs[nb_curidx].msh->bvh_volume.zmx + guardzone  ) );

								vector<apt_uint> nb_tmp_nbors_curr = current_set.bvh_objs->query( nb_bvh_obj_curr );
								for( auto nb_iit = nb_tmp_nbors_curr.begin(); nb_iit != nb_tmp_nbors_curr.end(); nb_iit++ ) {
									if ( *nb_iit != nb_curidx ) {
										unsigned char situation = v_v_core(
												current_set.objs[nb_curidx],
												current_set.objs[*nb_iit],
												info.IOAnalyzeProximity,
												info.proximity );
										if ( (situation == YES_VOLUMETRIC_OVERLAP)
												|| (info.IOAnalyzeProximity == true && situation == WITHIN_PROXIMITY) ) {
											size_t nb_iit_key = hash_pair_to_key( info.curr_set.set_identifier, (apt_uint) *nb_iit );
											nbors.push_back( nb_iit_key );
											//cout << "nb_iit_key " << nb_iit_key << "\n";
										}
									}
								}
								nb_tmp_nbors_curr = vector<apt_uint>();

								vector<apt_uint> nb_tmp_nbors_next = next_set.bvh_objs->query( nb_bvh_obj_curr ); //find possible neighbors among members in next set
								for( auto nb_jit = nb_tmp_nbors_next.begin(); nb_jit != nb_tmp_nbors_next.end(); nb_jit++ ) {
									//inspect neighbors of next set
									unsigned char situation = v_v_core(
											current_set.objs[nb_curidx],
											next_set.objs[*nb_jit],
											info.IOAnalyzeProximity,
											info.proximity );
									if ( (situation == YES_VOLUMETRIC_OVERLAP)
											|| (info.IOAnalyzeProximity == true && situation == WITHIN_PROXIMITY) ) {
										size_t nb_jit_key = hash_pair_to_key( info.next_set.set_identifier, (apt_uint) *nb_jit );
										nbors.push_back( nb_jit_key );
										//cout << "nb_jit_key " << nb_jit_key << "\n";
									}
								}
								nb_tmp_nbors_next = vector<apt_uint>();
							}
							else if ( nb_state == info.next_set.set_identifier ) {
								apt_uint nb_nxtidx = nb_state_objidx.second;
								hedgesAABB nb_bvh_obj_next(
									trpl(	next_set.objs[nb_nxtidx].msh->bvh_volume.xmi - guardzone,
											next_set.objs[nb_nxtidx].msh->bvh_volume.ymi - guardzone,
											next_set.objs[nb_nxtidx].msh->bvh_volume.zmi - guardzone  ),
									trpl(	next_set.objs[nb_nxtidx].msh->bvh_volume.xmx + guardzone,
											next_set.objs[nb_nxtidx].msh->bvh_volume.ymx + guardzone,
											next_set.objs[nb_nxtidx].msh->bvh_volume.zmx + guardzone  ) );

								vector<apt_uint> nb_tmp_nbors_next = next_set.bvh_objs->query( nb_bvh_obj_next );
								for( auto nb_iit = nb_tmp_nbors_next.begin(); nb_iit != nb_tmp_nbors_next.end(); nb_iit++ ) {
									if ( *nb_iit != nb_nxtidx ) {
										unsigned char situation = v_v_core(
												next_set.objs[nb_nxtidx],
												next_set.objs[*nb_iit],
												info.IOAnalyzeProximity,
												info.proximity );
										if ( (situation == YES_VOLUMETRIC_OVERLAP)
												|| (info.IOAnalyzeProximity == true && situation == WITHIN_PROXIMITY) ) {
											size_t nb_iit_key = hash_pair_to_key( info.next_set.set_identifier, (apt_uint) *nb_iit );
											nbors.push_back( nb_iit_key );
											//cout << "nb_iit_key " << nb_iit_key << "\n";
										}
									}
								}
								nb_tmp_nbors_next = vector<apt_uint>();

								vector<apt_uint> nb_tmp_nbors_curr = current_set.bvh_objs->query( nb_bvh_obj_next );
								for( auto nb_jit = nb_tmp_nbors_curr.begin(); nb_jit != nb_tmp_nbors_curr.end(); nb_jit++ ) {
									unsigned char situation = v_v_core(
											next_set.objs[nb_nxtidx],
											current_set.objs[*nb_jit],
											info.IOAnalyzeProximity,
											info.proximity );
									if ( (situation == YES_VOLUMETRIC_OVERLAP)
											|| (info.IOAnalyzeProximity == true && situation == WITHIN_PROXIMITY) ) {
										size_t nb_jit_key = hash_pair_to_key( info.curr_set.set_identifier, (apt_uint) *nb_jit );
										nbors.push_back( nb_jit_key );
										//cout << "nb_jit_key " << nb_jit_key << "\n";
									}
								}
								nb_tmp_nbors_curr = vector<apt_uint>();
							}
							else {
								cerr << "Unexpected value for nb_state_objid.first !" << "\n"; return;
							}

						} //done processing not-visited neighbor
					}
					else {
						cerr << "Inconsistently failing search for a neighbor !" << "\n";
						//cout << "nbors[key_it] " << key << "\n";
						return;
					}
				} //next neighbor
				clid++;
			} //done processing an unvisited point
		} //next node

		cout << "clid " << clid << "\n";

		cout << "Done with the cluster analysis, reporting results to HDF5" << "\n";

//ANALYZE CLUSTER AND MEMBER TO CLUSTER ASSOCIATION
		//(n_curr, 2) matrix, uint32, first column current_set.objs feature_identifier (here recovered from indices on objs), second column which cluster_identifier
		vector<apt_uint> u32_curr_id_clst_id;
		//(n_next, 2) matrix, uint32, first column next_set.objs feature identifier, second column which cluster_identifier
		vector<apt_uint> u32_next_id_clst_id;
		//(n_clst,) vector, uint32, first column name/ids of each cluster/coprecipitate, i.e. cluster_identifier
		vector<apt_uint> u32_clst_id;
		//(n_clst, 3) matrix, uint32, first column for members from current_set, second column for members from next_set, third column total number of members from curr+next set
		vector<apt_uint> u32_clst_comp;
		//first column type of n-let, second column total count
		vector<apt_uint> u32_clst_stats;

		map<apt_uint, vector<size_t>*> clusters;
		for( auto it = visited_clusterid.begin(); it != visited_clusterid.end(); it++ ) {
			if ( it->second.first == true ) { //visited ? (checking in-place if each member of the joint set has now a cluster assign, even if the member is alone in this cluster

				pair<apt_uint, apt_uint> state_objidx = unhash_key_to_pair(it->first);
				//cout << "i/task/idx/visited/clid/roiID " << ii
				//		<< "\t\t" << state_objidx.first << "\t\t" << state_objidx.second
				//		<< "\t\t" << it->second.first << "\t\t" << it->second.second
				//		<< "\t\t";
				if ( state_objidx.first == info.curr_set.set_identifier ) {
					//cout << current_set.objs[state_objidx.second].msh->roiID;
					u32_curr_id_clst_id.push_back( current_set.objs[state_objidx.second].msh->roiID );
					u32_curr_id_clst_id.push_back( it->second.second );
				}
				if ( state_objidx.first == info.next_set.set_identifier ) {
					//cout << next_set.objs[state_objidx.second].msh->roiID;
					u32_next_id_clst_id.push_back( next_set.objs[state_objidx.second].msh->roiID );
					u32_next_id_clst_id.push_back( it->second.second );
				}
				//cout << "\n";
				//ii++;

				apt_uint cluster_id = it->second.second;
				map<apt_uint, vector<size_t>*>::iterator thisone = clusters.find(cluster_id);
				if ( thisone != clusters.end() ) {
					if ( clusters[cluster_id] != NULL ) {
						clusters[cluster_id]->push_back( it->first );
					}
				}
				else {
					clusters[cluster_id] = new vector<size_t>();
					if ( clusters[cluster_id] != NULL ) {
						clusters[cluster_id]->push_back( it->first );
					}
					else {
						cerr << "Allocation error for members of cluster " << cluster_id << "\n";
						//##MK::continue, memory will be released later
					}
				}
			}
			else {
				cerr << "Found an object which remains unassigned to any cluster.!" << "\n";
				cerr << "This indicates that the cluster analysis was likely faulty and we should not trust the results!" << "\n";
				//##MK::continue, memory will be released later
			}
		}

		//report total number of clusters
		map<size_t, size_t> stats;
		for( auto it = clusters.begin(); it != clusters.end(); it++ ) {

			u32_clst_id.push_back( it->first );
			apt_uint n_curr = 0;
			apt_uint n_next = 0;

			if ( it->second != NULL ) {
				//cout << "Cluster " << it->first << "\t\t" << it->second->size() << "\n";
				map<size_t, size_t>::iterator thisone = stats.find( it->second->size() );
				if ( thisone != stats.end() ) {
					stats[it->second->size()] += 1;
				}
				else {
					stats[it->second->size()] = 1;
				}
				if ( it->second->size() > 0 ) {
					for( auto jt = it->second->begin(); jt != it->second->end(); jt++ ) {
						//decoding from objs_* array indices to object name/roiID
						pair<apt_uint, apt_uint> state_objidx = unhash_key_to_pair( *jt );
						//cout << "(" << state_objidx.first << ", ";
						if ( state_objidx.first == info.curr_set.set_identifier ) {
							//cout << objs_curr[state_objidx.second].msh->roiID << ")";
							n_curr++;
						}
						else if ( state_objidx.first == info.next_set.set_identifier ) {
							//cout << objs_next[state_objidx.second].msh->roiID << ")";
							n_next++;
						}
						else {
							cerr << "There is an inconsistence with the state of a member of a cluster!" << "\n";
						}
						//cout << " ";
					}
					//cout << "\n";
				}
				//release memory
				delete it->second; it->second = NULL;
			}
			else {
				cerr << "memory access error for members of cluster " << it->first << "\n";
			}

			u32_clst_comp.push_back( n_curr );
			u32_clst_comp.push_back( n_next );
			u32_clst_comp.push_back( n_curr + n_next );
		}

		apt_uint n_total = 0;
		for( auto jt = stats.begin(); jt != stats.end(); jt++ ) {
			cout << "n-let " << jt->first << "\t\tcnt\t\t" << jt->second << "\n";
			n_total += jt->second;
		}

		u32_clst_stats.push_back( 0 );
		u32_clst_stats.push_back( n_total );
		for( auto jt = stats.begin(); jt != stats.end(); jt++ ) {
			u32_clst_stats.push_back( jt->first );
			u32_clst_stats.push_back( jt->second );
		}

//WRITE CLUSTER STATISTICS
		HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
		ioAttributes anno = ioAttributes();
		string grpnm = "";
		string dsnm = "";

		apt_uint entry_id = 1;
		grpnm = "/entry" + to_string(entry_id) + "/v_v_spatial_correlation" + to_string(info.taskid) + "/coprecipitation_analysis";
		anno = ioAttributes();
		anno.add( "NX_class", string("NXprocess") );
		if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return; }

		dsnm = grpnm + "/current_set_feature_to_cluster";
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({u32_curr_id_clst_id.size() / 2, 2},
					{u32_curr_id_clst_id.size() / 2, 2},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			u32_curr_id_clst_id,
			anno ) != MYHDF5_SUCCESS ) { return; }
		u32_curr_id_clst_id = vector<apt_uint>();

		dsnm = grpnm + "/next_set_feature_to_cluster";
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({u32_next_id_clst_id.size() / 2, 2},
					{u32_next_id_clst_id.size() / 2, 2},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			u32_next_id_clst_id,
			anno ) != MYHDF5_SUCCESS ) { return; }
		u32_next_id_clst_id = vector<apt_uint>();

		dsnm = grpnm + "/cluster_identifier";
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({u32_clst_id.size()}, {u32_clst_id.size()},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			u32_clst_id,
			anno ) != MYHDF5_SUCCESS ) { return; }
		u32_clst_id = vector<apt_uint>();

		dsnm = grpnm + "/cluster_composition";
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({u32_clst_comp.size() / 3, 3},
					{u32_clst_comp.size() / 3, 3},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			u32_clst_comp,
			anno ) != MYHDF5_SUCCESS ) { return; }
		u32_clst_comp = vector<apt_uint>();

		dsnm = grpnm + "/cluster_statistics";
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({u32_clst_stats.size() / 2, 2},
					{u32_clst_stats.size() / 2, 2},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			u32_clst_stats,
			anno ) != MYHDF5_SUCCESS ) { return; }
		u32_clst_stats = vector<apt_uint>();

	}

	current_set.clear_bvh();
	next_set.clear_bvh();

	visited_clusterid = map<size_t, pair<bool, apt_uint>>();

	double toc = MPI_Wtime();
	cout << "analyze_polyhedron_curr_plus_next took " << (toc-tic) << " seconds" << "\n";
}


void intersectorHdl::execute_v_v_workpackage()
{
	double tic = MPI_Wtime();
	double toc = 0.;

	//##MK::development version does not support distributing of the work so far

	for( auto tskit = ConfigIntersector::SpatialCorrelationTasks.begin(); tskit != ConfigIntersector::SpatialCorrelationTasks.end(); tskit++ ) {

		double ttic = omp_get_wtime();
		//##MK::prep results file if this hasnt happened !

		//##MK::there is optimization potential in that one could check if the next obj ensemble from the previous integration is the curr ensemble
		//in this (next) iteration, if so, check if data for objs_next still exist and if so just swapping of pointers from objs_next to objs_curr
		//rather than reloading

//LOAD RELEVANT MESH DATA
		current_set = VolumeFeatureSet();
		current_set.identifier = tskit->curr_set.set_identifier;

		if ( current_set.read_relevant_v_input( tskit->curr_set ) == true ) {
		//if ( read_relevant_v_input( *tskit, CURRENT_SET ) == true ) {
			cout << "Reading relevant input current was successful" << "\n";
		}
		else {
			cerr << "Reading relevant input current failed !" << "\n";
			continue;
		}

		next_set = VolumeFeatureSet();
		next_set.identifier = tskit->next_set.set_identifier;
		if ( next_set.read_relevant_v_input( tskit->next_set ) == true ) {
		//if ( read_relevant_v_input( *tskit, NEXT_SET ) == true ) {
			cout << "Reading relevant input next was successful" << "\n";
		}
		else {
			cerr << "Reading relevant input next failed !" << "\n";
			continue;
		}

//CHECK CONSISTENCE OF SETS
		//now start the overlap analysis, with which of the members in objs_next does each member in objs_curr overlap?
		//check correctness organization of the objs arrays
		cout << "evapid_comparison" << "\n";
		if ( current_set.check_input() == true ) {
			cout << "current_set " << current_set.identifier << " okay" << "\n";
		}
		else {
			cerr << "current_set " << current_set.identifier << " faulty!" << "\n";
			continue;
		}
		if ( next_set.check_input() == true ) {
			cout << "next_set " << next_set.identifier << " okay" << "\n";
		}
		else {
			cerr << "next_set " << next_set.identifier << " faulty!" << "\n";
			continue;
		}


//BUILD BVH FOR NEXT SET
		//we use a nested sequence of bounded volume hierarchies to reduce the average number of costly tetrahedron-tetrahedron intersection tests to perform per object pair
		//the first layer of this hierarchy of BVHs is an AABB about all polyhedra from objs_next, this enables us
		//to filter quickly relevant intersections between an object from objs_curr with candidates from objs_next
		if ( next_set.build_bvh() == true ) {
		//if ( build_bvh( NEXT_SET ) == true ) {
			cout << "Building BVH of objs_next was successful" << "\n";
		}
		else {
			cerr << "Building BVH of objs_next failed !" << "\n";
			continue;
		}

//PERFORM OVERLAP/INTERSECTION ANALYSIS
//OPTIONAL PROXIMITY ANALYSIS
		if ( tskit->IOAnalyzeOverlap == true || tskit->IOAnalyzeProximity == true ) {

			analyze_compare_sets_current_to_next( *tskit );
		}

		cout << "analyze_compare_sets_current_to_next task " << tskit->taskid << " results" << "\n";
		cout << "c2n_links_i.size() " << c2n_links_i.size() << "\n";
		cout << "n2c_links_i.size() " << n2c_links_i.size() << "\n";
		cout << "c2n_links_p.size() " << c2n_links_p.size() << "\n";
		cout << "n2c_links_p.size() " << n2c_links_p.size() << "\n";

////OPTIONAL COPRECIPIATION TYPE ANALYSIS CLUSTERING OF THE GRAPH NODES
		if ( tskit->IOAnalyzeCoprecipitate == true ) {
			//##MK:: it is not possible to perform simple set comparisons with the following function
			//##MK::without having ConfigIntersector::IOObjectVolume == true because of the
			//##MK::head graph of the individual time steps is instantiated from the objs_properties set
			//however, this set is currently not getting initialized if not at least the volume as a prop
			//of objects is passed over
			//##MK::strictly speaking we do a comparison of ID sets only so it should not be necessary
			//##MK::to load the volume this should be changed in a future revision of this function
			//##MK::for now the workaround is to export also the volume of the proxy objects

			analyze_coprecipitates( *tskit );
		}

		if ( write_results_h5( *tskit ) == true ) {
			cout << "Writing results for taskid " << tskit->taskid << " was successful" << "\n";
		}
		else {
			cerr << "Writing results failed !" << "\n";
		}

		//pointer switch possible to avoid duplicated reading of input data if the new curr is the old next, just swop objs_curr with objs_next
		//release objects from objs_next and load only the next next

		double ttoc = omp_get_wtime();
		cout << "Finished object spatial correlation analyses for task " << tskit->taskid << ", took " << (ttoc-ttic) << " s" << "\n";

	} //next overlap analysis task for a different pair of object ensembles

	toc = MPI_Wtime();
	memsnapshot mm = intersector_tictoc.get_memoryconsumption();
	intersector_tictoc.prof_elpsdtime_and_mem( "ExecuteWorkpackage", APT_XX, APT_IS_PAR, mm, tic, toc);
}


bool intersectorHdl::write_results_h5( v_v_spatial_correlation_task const & info )
{
	double tic = MPI_Wtime();
	double toc = tic;

	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string dsnm = "";

	apt_uint entry_id = 1;
	grpnm = "/entry" +to_string(entry_id) + "/v_v_spatial_correlation" + to_string(info.taskid);
	anno = ioAttributes();
	anno.add( "NX_class", string("NXapm_paraprobe_tool_results") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	vector<forward_link> c2n_lnk = vector<forward_link>();
	c2n_lnk.insert( c2n_lnk.end(), c2n_links_i.begin(), c2n_links_i.end() );
	c2n_lnk.insert( c2n_lnk.end(), c2n_links_p.begin(), c2n_links_p.end() );
	sort( c2n_lnk.begin(), c2n_lnk.end(), SortForwardLinks );

	dsnm = grpnm + "/current_to_next_link";
	vector<apt_uint> uint;
	for( auto it = c2n_lnk.begin(); it != c2n_lnk.end(); it++ ) {
		uint.push_back( it->src_id );
		uint.push_back( it->trg_id );
	}
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({uint.size() / 2, 2}, {uint.size() / 2, 2}, MYHDF5_COMPRESSION_GZIP, 0x01),
		uint,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	uint = vector<apt_uint>();

	dsnm = grpnm + "/current_to_next_link_type";
	vector<unsigned char> u08;
	for( auto it = c2n_lnk.begin(); it != c2n_lnk.end(); it++ ) {
		u08.push_back( it->type );
	}
	c2n_lnk = vector<forward_link>();
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({u08.size()}, {u08.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		u08,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	u08 = vector<unsigned char>();

	vector<forward_link> n2c_lnk = vector<forward_link>();
	n2c_lnk.insert( n2c_lnk.end(), n2c_links_i.begin(), n2c_links_i.end() );
	n2c_lnk.insert( n2c_lnk.end(), n2c_links_p.begin(), n2c_links_p.end() );
	sort( n2c_lnk.begin(), n2c_lnk.end(), SortForwardLinks );

	dsnm = grpnm + "/next_to_current_link";
	for( auto it = n2c_lnk.begin(); it != n2c_lnk.end(); it++ ) {
		uint.push_back( it->src_id );
		uint.push_back( it->trg_id );
	}
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({uint.size() / 2, 2}, {uint.size() / 2, 2}, MYHDF5_COMPRESSION_GZIP, 0x01),
		uint,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	uint = vector<apt_uint>();

	dsnm = grpnm + "/next_to_current_link_type";
	for( auto it = n2c_lnk.begin(); it != n2c_lnk.end(); it++ ) {
		u08.push_back( it->type );
	}
	n2c_lnk = vector<forward_link>();
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({u08.size()}, {u08.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		u08,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	u08 = vector<unsigned char>();

	toc = MPI_Wtime();
	memsnapshot mm = intersector_tictoc.get_memoryconsumption();
	intersector_tictoc.prof_elpsdtime_and_mem( "WriteResultsH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


//deprecated functions, do not use as is, only as examples !, ##MK::which should better be reimplemented in python directly
/*
//map<size_t,map<apt_uint,obj_props>*> objs_properties;
bool intersectorHdl::track_volume_evolution_single_object( const apt_uint taskid, const apt_uint target, vector<timedep_graph_node>* res )
{
	double tic = MPI_Wtime();
	double toc = 0.;

	//##MK::testing automated creation of cypher script
	set<size_t> nodes;
	map<size_t,set<size_t>> fwrelations; //"timestep" and key where a of the key maps -> to b of the key
	//##MK::+1 inference is assumed, no gaps in sampling the snapshot space!

	//vector<timedep_graph_node> target_trajectory;

	//take a specific object with ID target from a specific snapshot objs identified by taskid
	//and go backwards in "time"/states and forward in "time"/states respectively to identify how the volume of the object changes
	size_t tskid_start = (size_t) taskid;
	set<apt_uint> objs_trg; //objects overlapping with target in reference taskid
	set<apt_uint> objs_found; //reidentified objects at locations of any objs_trg in reference taskid + or - 1 respectively

	//register the properties of the current target and the reference point in time/phase-space
	map<size_t,map<apt_uint,obj_props>*>::iterator ref = objs_properties.find(tskid_start);
	if ( ref != objs_properties.end() ) {
		map<apt_uint,obj_props>::iterator keyval = ref->second->find(target);
		if ( ref->second != NULL ) {
			if ( keyval != ref->second->end() ) {
				res->push_back( timedep_graph_node(keyval->second.volume, keyval->second.atoms_total, keyval->second.atoms_cand, tskid_start) );
			}
			else {
				cerr << "Unable to find properties of the target object !" << "\n"; return false;
			}
		}
		else {
			cerr << "ref->second == NULL !" << "\n"; return false;
		}
	}
	else {
		cerr << "Unable to find property table of reference time step !" << "\n"; return false;
	}

	objs_trg.insert( target );
	cout << "track_volume_evolution_single_object " << taskid << " target " << target << "\n";

	//forward tracking
	//cout << "forward state tracking" << "\n";
	for( size_t tskid_fw = tskid_start;        ; tskid_fw++ ) { //this is essentially a while loop
		if ( objs_trg.size() > 0 ) {
			//it will terminate when the map with nodes contains no longer entries for increasing taskid time/states
			map<size_t,vector<forward_link>*>::iterator qry_ovrlp = c2n_links_i.find( tskid_fw );
			if( qry_ovrlp != c2n_links_i.end() ) {
				if ( qry_ovrlp->second != NULL ) { //find the links
					for( auto kt = qry_ovrlp->second->begin(); kt != qry_ovrlp->second->end(); kt++ ) {
						for( auto lt = objs_trg.begin(); lt != objs_trg.end(); lt++ ) {
							//is that candidate any one of those to our interest?
							if ( kt->src_id != *lt ) {
								continue;
							}
							else { //src_id == *lt
								objs_found.insert( kt->trg_id );

								//##MK::begin testing automated creation of cypher script
								nodes.insert( hash_pair_to_key( tskid_fw, kt->src_id ) );
								nodes.insert( hash_pair_to_key( tskid_fw+1, kt->trg_id ) );
								map<size_t,set<size_t>>::iterator cyp = fwrelations.find( tskid_fw );
								if ( cyp != fwrelations.end() ) {
									fwrelations[tskid_fw].insert( hash_pair_to_key(kt->src_id, kt->trg_id) ); //assuming later that there is a node tskid_fw+1 trgid
								}
								else {
									fwrelations[tskid_fw] = set<size_t>();
									fwrelations[tskid_fw].insert( hash_pair_to_key(kt->src_id, kt->trg_id) );
								}
								//##MK::end testing

								break; //no need to test further
							}
						} //next target
					} //next node
				}
				else { //we need to find nodes with ids of objects in taskid +-1 which are at similar locations as are objects in taskid
					//but given there are no nodes, we can proceed and break out the while loop
					break;
				}
			}
			else { //we can go further into the future...
				break;
			}
			//still in the loop, report the volume of objects that we reidentified at locations of object targets
			double volume_sum = 0.;
			apt_uint atoms_total = 0;
			apt_uint atoms_cand = 0;
			apt_uint fragment_cnt = 0;
			//cout << "tskid_fw " << tskid_fw << "\n";
			//cout << "objs_trg ";
			//for( auto it = objs_trg.begin(); it != objs_trg.end(); it++ ) {
			//	cout << *it;
			//}
			//cout << "\n" << "objs_found ";
			map<size_t,map<apt_uint,obj_props>*>::iterator tsk = objs_properties.find(tskid_fw+1); //+1
			//because we want to look into the future from curr2next
			if ( tsk != objs_properties.end() ) {
				if ( tsk->second != NULL ) {
					if ( objs_found.size() > 0 ) {
						for( auto jt = objs_found.begin(); jt != objs_found.end(); jt++ ) {
							//cout << *jt;
							map<apt_uint,obj_props>::iterator keyval = tsk->second->find(*jt);
							if( keyval != tsk->second->end() ) {
								volume_sum += keyval->second.volume;
								fragment_cnt++;
								if ( ConfigIntersector::IOObjectComposition == true ) {
									atoms_total += keyval->second.atoms_total;
									atoms_cand += keyval->second.atoms_cand;
								}
							}
							else {
								cerr << "Unable to read a properties for a specific obj next " << *jt << " with the following objid " << keyval->first << "\n";
								return false; //return hard because these data have to exist otherwise our scientific accounting will be flawed
							}
						}

						//cout << "\n";
						//cout << "accumulated volume of re-identified object (fragments) in next state " << volume_sum << " atoms_total " << atoms_total << " atoms_cand " << atoms_cand << " fragment_cnt " << fragment_cnt << "\n";
						res->push_back( timedep_graph_node(volume_sum, atoms_total, atoms_cand, tskid_fw+1) );
					}
					objs_trg = objs_found;
					objs_found = set<apt_uint>();
				}
				else {
					cerr << "tsk->second == NULL !" << "\n"; return false;
				}
			}
			else {
				cerr << "No later, more next, ... steps !" << "\n"; break;
			}
		}
		else {
			break;
		}
	}

	//backward tracking
	//cout << "backward tracking" << "\n";
	objs_trg = set<apt_uint>();
	objs_trg.insert( target );
	for( size_t tskid_bk = tskid_start;        ; tskid_bk-- ) { //this is essentially a while loop
		if ( objs_trg.size() > 0 ) {
			//it will terminate when the map with nodes contains no longer entries for increasing taskid time/states
			map<size_t,vector<forward_link>*>::iterator qry_ovrlp = c2n_links_i.find( tskid_bk - 1 );
			if( qry_ovrlp != c2n_links_i.end() ) {
				if ( qry_ovrlp->second != NULL ) { //find the links
					for( auto kt = qry_ovrlp->second->begin(); kt != qry_ovrlp->second->end(); kt++ ) {
						for( auto lt = objs_trg.begin(); lt != objs_trg.end(); lt++ ) {
							//is that candidate any one of those to our interest?
							if ( kt->trg_id != *lt ) {
								continue;
							}
							else { //trg_id == *lt
								objs_found.insert( kt->src_id );

								//##MK::begin testing automated creation of cypher script
								nodes.insert( hash_pair_to_key( tskid_bk-1, kt->src_id ) );
								nodes.insert( hash_pair_to_key( tskid_bk, kt->trg_id ) );
								map<size_t,set<size_t>>::iterator cyp = fwrelations.find( tskid_bk-1 );
								if ( cyp != fwrelations.end() ) {
									fwrelations[tskid_bk-1].insert( hash_pair_to_key(kt->src_id, kt->trg_id) ); //assuming later that tskid_bk src_id exists
								}
								else {
									fwrelations[tskid_bk-1] = set<size_t>();
									fwrelations[tskid_bk-1].insert( hash_pair_to_key(kt->src_id, kt->trg_id) );
								}
								//##MK::end testing

								break; //no need to test further
							}
						} //next target
					} //next node
				}
				else { //we need to find nodes with ids of objects in taskid +-1 which are at similar locations as are objects in taskid
					//but given there are no nodes, we can proceed and break out the while loop
					break;
				}
			}
			else { //we can go further into the future...
				break;
			}
			//still in the loop, report the volume of objects that we reidentified at locations of object targets
			double volume_sum = 0.;
			apt_uint atoms_total = 0;
			apt_uint atoms_cand = 0;
			apt_uint fragment_cnt = 0;
			//cout << "tskid_bk " << tskid_bk << "\n";
			//cout << "objs_trg ";
			//for( auto it = objs_trg.begin(); it != objs_trg.end(); it++ ) {
			//	cout << *it;
			//}
			//cout << "\n" << "objs_found ";
			map<size_t,map<apt_uint,obj_props>*>::iterator tsk = objs_properties.find(tskid_bk-1); //-1
			//because we want to look from the present into the past
			if ( tsk != objs_properties.end() ) {
				if ( tsk->second != NULL ) {
					if ( objs_found.size() > 0 ) {
						for( auto jt = objs_found.begin(); jt != objs_found.end(); jt++ ) {
							//cout << *jt;
							map<apt_uint,obj_props>::iterator keyval = tsk->second->find(*jt);
							if( keyval != tsk->second->end() ) {
								volume_sum += keyval->second.volume;
								fragment_cnt++;
								if ( ConfigIntersector::IOObjectComposition == true ) {
									atoms_total += keyval->second.atoms_total;
									atoms_cand += keyval->second.atoms_cand;
								}
							}
							else {
								cerr << "Unable to read properties for a specific obj next " << *jt << " with the following objid " << keyval->first << "\n";
								return false; //return hard because these data have to exist otherwise our scientific accounting will be flawed
							}
						}

						//cout << "\n";
						//cout << "accumulated volume of re-identified object (fragments) in previous state " << volume_sum << " atoms_total " << atoms_total << " atoms_cand " << atoms_cand << " fragment_cnt " << fragment_cnt << "\n";
						res->push_back( timedep_graph_node(volume_sum, atoms_total, atoms_cand, tskid_bk-1) );
					}
					objs_trg = objs_found;
					objs_found = set<apt_uint>();
				}
				else {
					cerr << "tsk->second == NULL !" << "\n"; return false;
				}
			}
			else {
				cerr << "No earlier, more previous, ... steps !" << "\n";
				break;
			}
		}
		else {
			break;
		}
	}

	sort( res->begin(), res->end(), SortAscTimeDepNodes );

	//##MK::BEGIN DEBUG
	//sort trajectory for ascending "time"-step
	for( auto kt = res->begin(); kt != res->end(); kt++ ) {
		cout << kt->t << ";" << kt->volume_sum << ";" << kt->atom_cand_sum << ";" << kt->atom_total_sum << "\n";
	}
	//##MK::END DEBUG

	//##MK::begin testing automated creation of cypher script
	//create string with representing content of a neo4j cypher script to visualize these connections graphically via e.g. neo4j desktop
	string cypher = "";
	//add the nodes
	cypher += "CREATE ";
	for( auto itn = nodes.begin(); itn != nodes.end(); itn++ ) {
		pair<apt_uint,apt_uint> ab = unhash_key_to_pair(*itn);
		//                              "timestep"                     "obj_id"
		string node0_nm = "NodeX" + to_string(ab.first) + "X" + to_string(ab.second) + ":IsoSurface" + to_string(ab.first); //labelled node
		string node0_name = "phi: " + to_string(ab.first) + ", obj: " + to_string(ab.second);
		cypher += "(" + node0_nm + " { name: '" + node0_name + "'} ), ";
	}
	for( auto itr = fwrelations.begin(); itr != fwrelations.end(); itr++ ) {
		for( auto jtr = itr->second.begin(); jtr != itr->second.end(); jtr++ ) { //decodes timestep ids, know we have only forward +1 relations!
			pair<apt_uint,apt_uint> ab = unhash_key_to_pair(*jtr);
			//check if the following nodes exist:
			//ab.first, ab.second
			set<size_t>::iterator thisone0 = nodes.find( hash_pair_to_key(itr->first, ab.first) );
			set<size_t>::iterator thisone1 = nodes.find( hash_pair_to_key(itr->first+1, ab.second) );
			if ( thisone0 != nodes.end() && thisone1 != nodes.end() ) {
				pair<apt_uint,apt_uint> pair0 = unhash_key_to_pair(*thisone0);
				pair<apt_uint,apt_uint> pair1 = unhash_key_to_pair(*thisone1);
				//cout << "RELATION" << "\t\t" << pair0.first << "\t\t|\t\t" << pair0.second <<
				//		"\t\t -> \t\t" << pair1.first << "\t\t|\t\t" << pair1.second << "\n"; //to_string(ab.first) << "->" << "\n";
				string node0_nm = "NodeX" + to_string(pair0.first) + "X" + to_string(pair0.second);
				string node1_nm = "NodeX" + to_string(pair1.first) + "X" + to_string(pair1.second);
				cypher += "(" + node0_nm + ")-[r:OVERLAPS]->(" + node1_nm + "), ";
			}
			else {
				cerr << "LOGIC ERROR !!!!!!!!!!!!!" << "\n"; return true;
			}
 		}
	}
	cypher.pop_back();
	cypher.pop_back();
	cypher += "\n";

	cout << cypher << "\n";
	//##MK::end testing automated cypher script

	toc = MPI_Wtime();
	cout << "Tracking taskid " << taskid << " target " << target << " took " << (toc-tic) << " s" << "\n";
	return true;
}
*/


//OPTIONAL INTERSECTION VOLUME COMPUTATION
/*
if ( ConfigIntersector::OverlapMethod == TETGEN_TETRAHEDRALIZE && ovrlp_tskit->IOAnalyzeIntersectionVolume == true ) {
	//performing volume intersection analysis using nef polyhedra
	//knowing now with which objects the current obj_curr overlaps (if any) we can compute how much volume of the overlapping objects take
	for( size_t curridx = 0; curridx < objs_curr.size(); curridx++ ) {

		double ctic = omp_get_wtime();

		//for every current object ... get the confirmed objects with overlap
		for( auto it = objs_curr[curridx].obj_ovrlp_idx_vol.begin(); it != objs_curr[curridx].obj_ovrlp_idx_vol.end(); it++ ) {

			//for these objects get all the current objects tetrahedra and query with which to check those of the tetrahedra from the confirmed candidates for intersection volume
			//the construction of the intersection between two nef tetrahedra is the most costly step there we want to construct as few intersections as possible

			for( size_t tet_curr_idx = 0; tet_curr_idx < objs_curr[curridx].msh->cells_volume.size(); tet_curr_idx++ ) {
				//this time we cannot break, we need to test for all tetrahedra building objs_curr an on average log number of tetrahedra of the candidate object

				roi_tetrahedron tet_curr = objs_curr[curridx].msh->get_tetrahedron( tet_curr_idx );
				hedgesAABB cur_tet_bvh = objs_curr[curridx].bvh->getAABB( tet_curr_idx );

				//it->first gives me the index on objs_next
				vector<apt_uint> obj_next_cand_tet_cand = objs_next[it->first].bvh->query( cur_tet_bvh );
				apt_real accumulated_volume_cand = MYZERO;

				for( size_t kidx = 0; kidx < obj_next_cand_tet_cand.size(); kidx++ ) {
					apt_uint tet_next_idx = obj_next_cand_tet_cand[kidx];
					roi_tetrahedron tet_next = objs_next[it->first].msh->get_tetrahedron(tet_next_idx);

					tetIntersectionVolume tet_a_tet_isct;
					pair<apt_real,int> retval = tet_a_tet_isct.compute_intersection_volume( tet_curr, tet_next );
					if ( retval.second == MYCGAL_NEF_SUCCESS ) {
						accumulated_volume_cand += retval.first;
					}
					else {
						cout << "Computation of intersection volume using nef polyhedra failed !";
						cerr << "Computation of intersection volume using nef polyhedra failed !"; //##MK::###mark the respective field
						return;
					}
				} //evaluate numerical intersection volume between all tetrahedra of objs_next[it->first] around tet_curr

				it->second += accumulated_volume_cand;
			}
			//done testing all my, i.e. tetrahedra potentially overlapping with confirmed cases in obj_next
			cout << "Intersection volume " << objs_curr[curridx].msh->roiID << " with object " << objs_next[it->first].msh->roiID << " is " << it->second << " nm^3" << "\n";
		}
		//done testing overlap of intruding objects into objs_curr[curridx]

		double ctoc = omp_get_wtime();
		cout << "Intersection volume computation of object " << objs_curr[curridx].msh->roiID << " with all intruders from objs_next[...] took " << (ctoc-ctic) << " s" << "\n";
	} //analyze next current object
}
*/
