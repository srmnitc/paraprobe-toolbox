/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_IntersectorStructs.h"

/*
ostream& operator << (ostream& in, timedep_graph_node const & val)
{
	in << "volume sum " << val.volume_sum << "\n";
	in << "atom_total_sum " << val.atom_total_sum << "\n";
	in << "atom_cand_sum " << val.atom_cand_sum << "\n";
	in << "t " << val.t << "\n";
	return in;
}
*/


ostream& operator << (ostream& in, forward_link const & val)
{
	in << "source " << val.src_id << "\n";
	in << "target " << val.trg_id << "\n";
	in << "type " << (int) val.type << "\n";
	return in;
}


size_t hash_pair_to_key( const apt_uint a, const apt_uint b )
{
	/* usually will map in the following way
	hash_pair_to_key(0, 0) 0
	hash_pair_to_key(0, 1) 1
	hash_pair_to_key(1, 0) 4294967295
	hash_pair_to_key((size_t) numeric_limits<unsigned int>::max(), 0) 18446744065119617025
	hash_pair_to_key(0, (size_t) numeric_limits<unsigned int>::max()) 4294967295
	*/
	//##MK::assert that apt_uint maps to 4 byte and size_t to 8 byte, and both use unsigned types, otherwise this will not work !
	return ((size_t) a)*((size_t) numeric_limits<unsigned int>::max()) + (size_t) b;
}


size_t hash_pair_to_key( pair<apt_uint, apt_uint> ab )
{
	return hash_pair_to_key( ab.first, ab.second );
}


pair<apt_uint, apt_uint> unhash_key_to_pair( const size_t key )
{
	size_t a = key / ((size_t) numeric_limits<unsigned int>::max());
	size_t b = key - a* ((size_t) numeric_limits<unsigned int>::max());
	return pair<apt_uint, apt_uint>( a, b );
}
