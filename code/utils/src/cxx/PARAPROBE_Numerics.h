/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_NUMERICS_H__
#define __PARAPROBE_UTILS_NUMERICS_H__

//units
//paraprobe-toolbox internally uses the following:
//units of length in ns
//units of time in s


//precision for real values
//paraprobe-toolbox uses single precision by default except for a few cases where higher precision is needed
//for math however and computational geometry floating point precision can cause degeneracies, CGAL
//resolves these with using even arbitrary-precision floating point via GMP and MPFR libraries
//given that modern systems both CPU and accelerators have double precision support in the hardware we
//should invest time for precision over speed
//single vs double precision is usually faster due to less memory content per data element and thus more efficient
//use of the caches per number of atoms processed
//given that positions are reported in ns using single precision suffices as the physical position accuracy
//of atom probe as a technique is sub-nanometer to angstrom respectively

//precision for integer values
//paraprobe-toolbox uses 32-bit integer values by default as this suffices still way enough to hold even the largest
//datasets ever measured (which is to the best of my knowledge 2022/10 has 1.2 billion ions)
//paraprobe-toolbox can handle at most 2**32 - 1 ions
//in those few cases where 32-bit integer values for identifiers and array indices is not sufficient paraprobe uses
//64-bit integer by default

//MK::undefine to switch to use double precision
#define EMPLOY_SINGLE_PRECISION

#define EPSILON_F32							(1.0e-5)
#define EPSILON_F64							(1.0e-12)

#ifdef EMPLOY_SINGLE_PRECISION
	#define EPSILON							EPSILON_F32
	typedef float apt_real;

	typedef int apt_int;
	typedef unsigned int apt_uint;
	#define MYZERO 							(0.f)
	#define MYONE 							(1.f)
#elif
	#define EPSILON							EPSILON_F64
	typedef double apt_real;
	typedef long apt_int;
	typedef unsigned long apt_uint;
	#define MYZERO 							(0.0)
	#define MYONE 							(1.0)
#endif

//##MK::obsolete typedef size_t apt_sizet;


//type range
#define U8MX							(numeric_limits<unsigned char>::max())
#define U8MI							(numeric_limits<unsigned char>::lowest())
#define I8MX							(numeric_limits<char>::max())
#define I8MI							(numeric_limits<char>::lowest())
#define U16MX							(numeric_limits<unsigned short>::max())
#define U16MI							(numeric_limits<unsigned short>::lowest())
#define I16MX							(numeric_limits<short>::max())
#define I16MI							(numeric_limits<short>::lowest())
#define U32MX							(numeric_limits<unsigned int>::max())
#define U32MI							(numeric_limits<unsigned int>::lowest())
#define I32MX							(numeric_limits<int>::max())
#define I32MI							(numeric_limits<int>::lowest())
#define U64MX							(numeric_limits<unsigned long>::max())
#define U64MI							(numeric_limits<unsigned long>::lowest())
#define I64MX							(numeric_limits<long>::max())
#define I64MI							(numeric_limits<long>::lowest())
#define F32MX							(numeric_limits<float>::max())
#define F32MI							(numeric_limits<float>::lowest())
#define F64MX							(numeric_limits<double>::max())
#define F64MI							(numeric_limits<double>::lowest())

#ifdef EMPLOY_SINGLE_PRECISION
	#define UMI							(U32MI)
	#define UMX							(U32MX)
	#define IMI							(I32MI)
	#define IMX							(I32MX)
	#define RMI							(F32MI)
	#define RMX							(F32MX)
#elif
	#define UMI							(U64MI)
	#define UMX							(U64MX)
	#define IMI							(I64MI)
	#define IMX							(I64MX)
	#define RMI							(F64MI)
	#define RMX							(F64MX)
#endif

#define ANALYZE_YES			0x01
#define ANALYZE_NO			0x00


//ranging
//paraprobe-toolbox defines molecular ion, ions, and elements as so-called isotope_vector that is a construction plan build from specific isotopes
//for elements the number of neutrons is set to zero telling we do not know the number of neutrons
//a hash function is used with which each ever observed or synthetic isotope can be represented using its number of protons and number of neutrons

#define MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION 	32
#define MAX_NUMBER_OF_ION_TYPES					256		//has to be 256 because we use an unsigned char for representing ion types

//El-Zoka et al. reported even peaks from D_{X}H_{15-X}O_{7}}^+ with X=15,14,...,0 hydrated fragments (frozen water)
//however playing this for all combinations of nuclids would end up with approximately 3000-ish ^ 22 / 0.5 different variants if
//including radioactive or at least 254^22, too many combinations
//therefore internally we suggest to reduce the search space using specific nuclids only and allow for
//substantially fewer nuclids (nuclids do not need to be disjoint !) in the molecular ions, e.g. 3 the larger the
//used portion of the nuclid map

#define MAX_CHARGE_PER_MOLECULARION				7

#define MYNEUTRAL								(0x00)
#define MYPOSITIVE								(0x01)
#define MYNEGATIVE								(0xFF)

//unknown, default iontype
//paraprobe-toolbox assigns each ion the unknown type unless a specific mass-to-charge-state ratio value matches an ions value
#define UNKNOWNTYPE								0


#endif
