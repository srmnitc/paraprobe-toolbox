/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_GRAINS_H__
#define __PARAPROBE_UTILS_GRAINS_H__

#include "PARAPROBE_ThermodynamicPhase.h"


struct grain
{
	apt_uint grainid;
	apt_uint natoms;
	crystalstructure structure;
	squat ori;

	grain() : grainid(0), natoms(0), ori(squat()) {}
	grain( const apt_uint _gid, squat const & in ) : grainid(_gid), natoms(0),
		ori(squat(in.q0, in.q1, in.q2, in.q3)) {}
};

ostream& operator<<(ostream& in, grain const & val);


class grainAggregate
{
public:
	grainAggregate();
	~grainAggregate();

	vector<grain> grains;
};

#endif

