/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_ArgsAndGitSha.h"


void command_line_help( const string cmd_option, const string toolname )
{
	cout << "cmd_option " << cmd_option << "\n";
	if ( cmd_option.compare("-h") == 0 || cmd_option.compare("--help") == 0 ) {
		cout << "C/C++ paraprobe-" << toolname << " needs two arguments" << "\n";
		cout << "The first is a simulation SimID, a positive integer on the interval [1, " << UMX << "]" << "\n";
		char character = toupper(toolname.at(0));
		string capitalized_toolname = ""; capitalized_toolname.push_back(character);
		capitalized_toolname += toolname.substr(1, string::npos);
		cout << "The second argument is the configuration file PARAPROBE."
				<< capitalized_toolname << ".Config.SimID.<>.nxs" << "\n";
		return;
	}
	cout << "Type -h or --help to learn how to pass arguments to this paraprobe-tool!" << "\n";
}
