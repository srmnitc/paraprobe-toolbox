/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_NEXUSINTERFACE_H__
#define __PARAPROBE_UTILS_NEXUSINTERFACE_H__

#define NEXUS_VERSION  "https://gitlab.com/paraprobe/paraprobe-toolbox/-/commit/e814ebd4b222c099ac1f1343becc7a6ad48ee949"
//https://gitlab.com/paraprobe/paraprobe-toolbox/-/tree/composing-v0.5?ref_type=heads

#endif