/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_CITEME_H__
#define __PARAPROBE_UTILS_CITEME_H__

#include "PARAPROBE_TimeAndDateHandling.h"


struct bibtex
{
	string entrytype;
	string author;
	string title;
	string journal;		//or book comment
	string year;
	string vol;
	string pages;
	string doi;
	bibtex() : entrytype(""), author(""), title(""), journal(""), year(""), vol(""), pages(""), doi("") {}
	bibtex( const string _etyp, const string _auth, const string _ttl, const string _jou,
			const string _yr, const string _vol, const string _pgs, const string _doi ) :
				entrytype(_etyp), author(_auth), title(_ttl), journal(_jou),
				year(_yr), vol(_vol), pages(_pgs), doi(_doi) {}
};

ostream& operator<<(ostream& in, bibtex const & val);


class CiteParaprobe
{
public:
	CiteParaprobe();
	~CiteParaprobe();
	
	static vector<bibtex> Citations;
	static size_t cite();
};


#endif

