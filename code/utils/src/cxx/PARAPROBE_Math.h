/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_MATH_H__
#define __PARAPROBE_UTILS_MATH_H__

#include "PARAPROBE_Profiling.h"


struct t3x1
{
	//a column vector
	apt_real a11;
	apt_real a21;
	apt_real a31;
	t3x1() : 	a11(static_cast<apt_real>(MYZERO)),
				a21(static_cast<apt_real>(MYZERO)),
				a31(static_cast<apt_real>(MYZERO)) {} //initialize to neutral column vector
	t3x1(const apt_real _a11, const apt_real _a21, const apt_real _a31) :
				a11(_a11),
				a21(_a21),
				a31(_a31) {}
};

ostream& operator<<(ostream& in, t3x1 const & val);


struct t3x3
{
	//a second order rank tensor with row-column indexing
	apt_real a11;
	apt_real a12;
	apt_real a13;
	apt_real a21;
	apt_real a22;
	apt_real a23;
	apt_real a31;
	apt_real a32;
	apt_real a33;
	t3x3() :	a11(MYONE), a12(MYZERO), a13(MYZERO),
				a21(MYZERO), a22(MYONE), a23(MYZERO),
				a31(MYZERO), a32(MYZERO), a33(MYONE) {}	//initialize to identity tensor
	t3x3( const apt_real* matrix3x3 ) :
				a11(matrix3x3[0]), a12(matrix3x3[1]), a13(matrix3x3[2]),
				a21(matrix3x3[3]), a22(matrix3x3[4]), a23(matrix3x3[5]),
				a31(matrix3x3[6]), a32(matrix3x3[7]), a33(matrix3x3[8]) {}
	t3x3(	const apt_real _a11, const apt_real _a12, const apt_real _a13,
			const apt_real _a21, const apt_real _a22, const apt_real _a23,
			const apt_real _a31, const apt_real _a32, const apt_real _a33 ) :
				a11(_a11), a12(_a12), a13(_a13),
				a21(_a21), a22(_a22), a23(_a23),
				a31(_a31), a32(_a32), a33(_a33) {}
	void add( const t3x3 & increase, const apt_real weight );
	void div( const apt_real divisor );
	apt_real det();
	t3x3 leftmultiply( t3x3 const & T );
};

ostream& operator<<(ostream& in, t3x3 const & val);


//linear interpolation and quantile utilities
template<typename T> T lerp(const T v0, const T v1, const T t);

template<typename T> vector<T> quantiles_nosort( vector<T> const & in, vector<T> const & q );

template<typename T> T quantile( vector<T> const & in, const T q );


#endif
