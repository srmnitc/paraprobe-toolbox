/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_VOXELTREE_H__
#define __PARAPROBE_UTILS_VOXELTREE_H__

#include "PARAPROBE_ROIs.h"

//implements a self-threadlocal-memory-allocating spatial index structure
//for amortized constant time queries of Moore neighborhood about spherical ball
//the container knows how the buckets have been mapped to threadlocal memory
//thus upon querying the struct sequentially by the callee using the range_rball function
//the callee will just read the data from shared memory and potentially has to query
//addresses in buckets which reference pieces of threadlocal memory
//if instead the callee is running threadparallel the individual threads will query
//the data structure and can make use of reading more likely local pieces of memory
//which will be faster at higher thread count on ccNUMA systems specifically on
//systems where multi-level hierarchies on top of the usual L1,L2,L3 cache hierarchy have
//were built


//internal accounting
//#define SPACEBUCKETING_ACTIVATE_ACCOUNTING

template <class T>
class LinearTimeVolQuerier
{
public:
	//an axis-aligned bounding box discretized into buckets for O(N) querying of objects
	LinearTimeVolQuerier();
	LinearTimeVolQuerier( aabb3d const & box, const apt_real _di );
	~LinearTimeVolQuerier();

	void add( T const & obj );
	void query_noclear_nosort( aabb3d & box, vector<T> & candidates );
	/*
	#ifdef SPACEBUCKETING_ACTIVATE_ACCOUNTING
		vector<vector<unsigned short>*> bucketsinfo;
		void add_atom_info( const pos p, const unsigned short ifo );
	#endif
	*/

	voxelgrid gridinfo;
	vector<vector<T>*> buckets;

};

#endif
