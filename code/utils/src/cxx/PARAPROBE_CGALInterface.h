/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_CGALINTERFACE_H__
#define __PARAPROBE_UTILS_CGALINTERFACE_H__

#include "PARAPROBE_OriMath.h"

//https://doc.cgal.org/latest/Polygon_mesh_processing/Polygon_mesh_processing_2orient_polygon_soup_example_8cpp-example.html#a3

#define TRIANGLE_SOUP_ANALYSIS_ERROR					-1
#define TRIANGLE_SOUP_ANALYSIS_NO_SUPPORT				-2
#define TRIANGLE_SOUP_ANALYSIS_INSUFFICIENT_SUPPORT		-3
#define TRIANGLE_SOUP_ANALYSIS_ERROR_REPAIRING			-4
#define TRIANGLE_SOUP_ANALYSIS_SELFINTERSECTING			-5
#define TRIANGLE_SOUP_ANALYSIS_BORDEREDGES				-6
#define TRIANGLE_SOUP_ANALYSIS_CLOSEDPOLYHEDRON			0
#define TRIANGLE_SOUP_ANALYSIS_SURFACEPATCH				1


#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/optimal_bounding_box.h>
#include <CGAL/Polygon_mesh_processing/triangulate_faces.h>
#include <CGAL/Polygon_mesh_processing/measure.h>
#include <CGAL/Polygon_mesh_processing/repair_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/orient_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/polygon_soup_to_polygon_mesh.h>
#include <CGAL/Polygon_mesh_processing/orientation.h>
#include <CGAL/Real_timer.h>
#include <CGAL/Polygon_mesh_processing/compute_normal.h>
#include <boost/property_map/property_map.hpp>

#include <array>
typedef CGAL::Exact_predicates_inexact_constructions_kernel  KK;

struct Array_traits
{
	struct Equal_3
	{
		bool operator()(const array<KK::FT, 3>& p, const array<KK::FT, 3>& q) const {
			return (p == q);
		}
	};
	struct Less_xyz_3
	{
		bool operator()(const array<KK::FT, 3>& p, const array<KK::FT, 3>& q) const {
			return std::lexicographical_compare(p.begin(), p.end(), q.begin(), q.end());
		}
	};
	Equal_3 equal_3_object() const {
		return Equal_3();
	}
	Less_xyz_3 less_xyz_3_object() const {
		return Less_xyz_3();
	}
};

#include <CGAL/Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/Polyhedron_items_with_id_3.h>
//typedef boost::graph_traits<CGAL::Polyhedron_3<K, CGAL::Polyhedron_items_with_id_3>>::vertex_descriptor vertex_descriptor;
//typedef boost::graph_traits<CGAL::Polyhedron_3<K, CGAL::Polyhedron_items_with_id_3>>::face_descriptor   face_descriptor;


pair<double, int> check_if_closed_polyhedron( vector<tri3u> const & fcts, vector<p3d> const & vrts );


#endif
