/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_UNITCONVERSIONS_H__
#define __PARAPROBE_UTILS_UNITCONVERSIONS_H__

//unit conversions
#define CELSIUS2KELVIN(T)				((273.15) + (T))
#define KELVIN2CELSIUS(T)				((T) - (273.15))

#define DEGREE2RADIANT(theta)			((theta)/(180.0)*(MYPI))
#define RADIANT2DEGREE(rad)				((rad)/(MYPI)*(180.0))

//scaling conversions
#define NANOMETER2ANGSTROEM(nm)			((10.0)*(nm))
#define ANGSTROEM2NANOMETER(ang)		((ang)/(10.0))

#define METER2ANGSTROEM(m)				((1.0e10)*(m))
#define METER2NANOMETER(m)				((1.0e9)*(m))
#define METER2MICRON(m)					((1.0e6)*(m))
#define METER2MILLIMETER(m)				((1.0e3)*(m))

#define NANOMETER2METER(nm)				((1.0e-9)*(nm))
#define MICRON2METER(mum)				((1.0e-6)*(mum))
#define MILLIMETER2METER(mm)			((1.0e-3)*(mm))

//##MK::here meaning Gibibyte normally GiB based 1024, while GB is based 1000
#define BYTE2KIBIBYTE(b)				((b)/(1024.))
#define BYTE2MEBIBYTE(b)				((b)/((1024.)*(1024.)))
#define BYTE2GIBIBYTE(b)				((b)/((1024.)*(1024.)*(1024.)))
#define KIBIBYTE2BYTE(kb)				((kb)*(1024.))
#define MEBIBYTE2BYTE(mb)				((mb)*(1024.)*(1024.))
#define GIBIBYTE2BYTE(gb)				((gb)*(1024.)*(1024.)*(1024.))


#endif
