/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_ROIS_H__
#define __PARAPROBE_UTILS_ROIS_H__

#include "PARAPROBE_TwoPointStats3D.h"


enum MYPRIMITIVE_TYPES {
	CG_CUBOID,
	CG_CYLINDER,
	CG_SPHERE,
	CG_POLYHEDRON
};


struct proxigram_support
{
	apt_real d; //signed_distance;
	unsigned short isotope; //nprotons + nneutrons*256
	unsigned short pad;
	proxigram_support() : d(MYZERO), isotope(isotope_hash(0x00, 0x00)), pad(0) {}
	proxigram_support( const apt_real _d, const unsigned char _nprotons, const unsigned char _nneutrons ) :
		d(_d), isotope(isotope_hash(_nprotons, _nneutrons)), pad(0) {}
	proxigram_support( const apt_real _d, const unsigned short _hashval ) :
			d(_d), isotope(_hashval), pad(0) {}
};


inline bool SortAscProxigramSupport( const proxigram_support & a, const proxigram_support & b )
{
	return a.d < b.d;
}


struct roi_rotated
{
	apt_uint roiID;					//"name" of this region of interest
	apt_uint ownerID;				//owners can be e.g. microstructural objects, or triangles
	apt_uint nions;					//ions inside or on the surface of the primitive
	apt_uint natoms;				//number of atoms after atomic decomposition of molecular ions accounted for inside or on the surface of the primitive
	p3d ori;	   				 	//supplemental vector p2-p1 specifying the orientation of the geometric primitive
	//vector<vector<apt_real>*> ityp_ecdfs; //iontype-specific empirical cumulated distribution of projected heights relative to bottom/lowcap p1
	vector<unsigned char> itypes_whitelist;
	vector<unsigned char> elemtypes_whitelist;
	map<unsigned char,vector<apt_real>*> ityp_proxigrams;
	map<unsigned char,vector<apt_real>*> elemtyp_proxigrams;
	bool close_to_the_edge;
	roi_rotated() : roiID(0), ownerID(0), nions(0), natoms(0), ori(p3d(MYZERO, MYZERO, MYZERO)),
			itypes_whitelist(vector<unsigned char>()), elemtypes_whitelist(vector<unsigned char>()),
			ityp_proxigrams(map<unsigned char, vector<apt_real>*>()),
			elemtyp_proxigrams(map<unsigned char, vector<apt_real>*>()), close_to_the_edge(false) {} //ityp_ecdfs(vector<vector<apt_real>*>()),

	vector<proxigram_support> ityp_global_proxigram;
	vector<proxigram_support> elemtyp_global_proxigram;
	vector<proxigram_support> isotope_global_proxigram;

	void init_proxigrams();
	void update_proxigrams();
	void compute_joint_proxigrams();
	void clear_proxigrams();
};


struct roi_rotated_cuboid : public roi_rotated
{
	p3d barycenter; 	//barycenter of the cuboid
	apt_real Lx;		//edge length
	apt_real Ly;
	apt_real Lz;
	t3x3 Ractive;		//rotation matrix, actively rotating Cartesian coordinate axes unit vectors into an aligned coordinate system with unit vectors that are than parallel to the edges of the cuboid
	p3d p1;				//supplementary vertices used for precomputing quantities associated with point-in-cuboid inclusion tests
	p3d p2;
	p3d p3;
	p3d p4;
	p3d p5;
	p3d p6;
	p3d p7;
	p3d p8;
	roi_rotated_cuboid() : barycenter(p3d(MYZERO, MYZERO, MYZERO)), Lx(MYZERO), Ly(MYZERO), Lz(MYZERO),
			Ractive(t3x3(MYONE, MYZERO, MYZERO, MYZERO, MYONE, MYZERO, MYZERO, MYZERO, MYONE)),
			p1(p3d()), p2(p3d()), p3(p3d()), p4(p3d()),
			p5(p3d()), p6(p3d()), p7(p3d()), p8(p3d()) {}
	roi_rotated_cuboid( p3d const & _low, p3d const & _high ); //axis-aligned bounding box
	roi_rotated_cuboid( p3d const & _barycenter, const apt_real _lx, const apt_real _ly, const apt_real _lz, t3x3 const & _rmtrx );
	roi_rotated_cuboid( p3d const & _barycenter, const apt_real _lx, const apt_real _ly, const apt_real _lz, p3d const & _ounrm );
	roi_rotated_cuboid( vector<p3d> const & _cornerpoints );
	aabb3d get_aabb() const;
	bool is_inside( p3d const & p ) const;
	//void ecdf_add( p3d const & p, size_t ityp_idx );
	void proxigram_add( p3d const & p, unsigned char const & ityp );
};

ostream& operator << (ostream& in, roi_rotated_cuboid const & val);


#define SIGNED_DISTANCE_BASED_ON_BARYCENTER		0
#define SIGNED_DISTANCE_BASED_ON_LOWEST_CAP		1

struct roi_rotated_cylinder : public roi_rotated
{
	p3d p1;				//center of "lower" cap
	p3d p2;				//center of "upper" cap
	apt_real r;			//radius

	roi_rotated_cylinder() : p1(p3d(MYZERO, MYZERO, MYZERO)), p2(p3d(MYZERO, MYZERO, MYZERO)), r(MYZERO) {}
	roi_rotated_cylinder( p3d const & _p1, p3d const & _p2, const apt_real _r );
	roi_rotated_cylinder( p3d const & _bary, p3d const & _direction, const apt_real _h, const apt_real _r );

	aabb3d get_aabb() const;
	bool is_inside( p3d const & p ) const;
	apt_real projected_height( p3d const & p );
	void proxigram_add( p3d const & p, const unsigned char elemtyp, apt_uint mltply,
		const unsigned char cmp_sgn_dist_method );
};

ostream& operator << (ostream& in, roi_rotated_cylinder const & val);


struct roi_sphere : public roi_rotated
{
	p3d barycenter; 	//barycenter
	apt_real r;			//radius

	roi_sphere() : barycenter(p3d(MYZERO, MYZERO, MYZERO)), r(MYZERO) {}
	roi_sphere( const apt_real _r ) :
		barycenter(p3d(MYZERO, MYZERO, MYZERO)), r(_r) {}
	roi_sphere( p3d const & _barycenter, const apt_real _r ) :
		barycenter(_barycenter), r(_r) {}
	bool is_inside( p3d const & p ) const;
	aabb3d get_aabb();
};

ostream& operator << (ostream& in, roi_sphere const & val);


struct roi_tetrahedron
{
	//##MK::does not inherit for now
	p3d v1;
	p3d v2;
	p3d v3;
	p3d v4;

	roi_tetrahedron() : v1(p3d()), v2(p3d()), v3(p3d()), v4(p3d()) {}

	aabb3d get_aabb();
	apt_real get_volume();
};

ostream& operator << (ostream& in, roi_tetrahedron const & val);


struct roi_polyhedron
{
	apt_uint roiID;				//"name" of this region of interest
	apt_uint ownerID;			//owners can be e.g. microstructural objects, or triangles
	unsigned char m1;			//markers for different low-level property flags
	unsigned char m2;
	unsigned char m3;
	unsigned char m4;

	//##MK::implement a set of tetrahedra
	vector<p3d> vrts_surface;
	vector<tri3u> fcts_surface;

	vector<p3d> vrts_volume; //vertices supporting the piecewise-linear complex which is the surface of the polyhedron and internal points or Steiner points added upon tetrahedralization by Tetgen
	vector<quad3u> cells_volume; //the tetrahedra

	aabb3d bvh_volume; //bounding box with EPSILON guard about the volume of the polyhedron

	roi_polyhedron() :  roiID(0), ownerID(0),
			m1(0x00), m2(0x00), m3(0x00), m4(0x00), vrts_surface(vector<p3d>()), fcts_surface(vector<tri3u>()),
			vrts_volume(vector<p3d>()), cells_volume(vector<quad3u>()), bvh_volume(aabb3d()) {}

	aabb3d get_aabb( size_t const cellidx );
	roi_tetrahedron get_tetrahedron( const size_t cellidx );
};


enum WINDOWING_METHOD {
	ENTIRE_DATASET,
	CUBOIDAL_REGION,
	CYLINDRICAL_REGION,
	SPHERICAL_REGION,
	POLYHEDRAL_REGION,
	COMPOSITE_REGION,
	UNION_OF_PRIMITIVES,
	BITMASKED_POINTS
};

#define WINDOW_ION_EXCLUDE						0x00
#define WINDOW_ION_INCLUDE_DEFAULT_CLASS		0x01  //MK::up to 255 classes can be defined

#endif
