/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_BitMangling.h"


BitPacker::BitPacker()
{
}


BitPacker::~BitPacker()
{
}


void BitPacker::bool_to_bitpacked_uint8( bool const * in, const size_t n_in, vector<unsigned char> & out )
{
	cout << "sizeof(unsigned char) " << sizeof(unsigned char) << "\n";
	if ( sizeof(unsigned char) != 1 ) {
		cerr << "sizeof(unsigned char) != 1 Byte" << "\n";
		out = vector<unsigned char>( n_in, 0x00 );
		for( size_t i = 0; i < n_in; i++ ) {
			if ( in[i] == true ) {
				out[i] = 0x01;
			}
		}
		return;
	}
	if ( in != NULL && n_in > 0 ) {
		cout << "Packing boolean states stored in a boolean array into a bitfield" << "\n";
		cout << 8*sizeof(unsigned char) << " bits in an unsigned char" << "\n";

		size_t number_of_ions = n_in;
		size_t byte_length = ( number_of_ions % 8 != 0 ) ? number_of_ions/(8*1) + 1 : number_of_ions/(8*1);

		out = vector<unsigned char>( byte_length, 0 );
		cout << "n_in " << n_in << "\n";
		cout << "byte_length " << byte_length << "\n";
		cout << "out.size() " << out.size() << "\n";

		//each byte has eight bits that we can set, make an array large enough that each ion can be stored
		for( size_t i = 0; i < n_in; i++ ) {
			if ( in[i] == true ) {
				unsigned int byte = i / 8;
				unsigned int bit = i % 8;
				BIT_SET(out[byte], bit);
			}
		}
	}
	else {
		out = vector<unsigned char>( n_in, 0x00 );
		for( size_t i = 0; i < n_in; i++ ) {
			if ( in[i] == true ) {
				out[i] = 0x01;
			}
		}
		return;
	}
}


void BitPacker::uint8_to_bitpacked_uint8( vector<unsigned char> const & in, vector<unsigned char> & out )
{
	//https://linuxhint.com/bit-masking-cpp/
	cout << "sizeof(unsigned char) " << sizeof(unsigned char) << "\n";
	if ( sizeof(unsigned char) != 1 ) {
		cerr << "sizeof(unsigned char) != 1 Byte" << "\n";
		out = in;
		return;
	}
	if ( in.size() > 0 ) {
		cout << "Packing boolean states stored in unsigned char into a bitfield" << "\n";
		cout << 8*sizeof(unsigned char) << " bits in an unsigned char" << "\n";

		size_t number_of_ions = in.size();
		size_t byte_length = ( number_of_ions % 8 != 0 ) ? number_of_ions/(8*1) + 1 : number_of_ions/(8*1);

		out = vector<unsigned char>( byte_length, 0 );
		cout << "in.size() " << in.size() << "\n";
		cout << "byte_length " << byte_length << "\n";
		cout << "out.size() " << out.size() << "\n";

		//each byte has eight bits that we can set, make an array large enough that each ion can be stored
		for( size_t i = 0; i < in.size(); i++ ) {
			if ( in[i] == 0x01 ) {
				unsigned int byte = i / 8;
				//cout << "Byte " << byte << "\n";
				unsigned int bit = i % 8;
				//cout << "Bit " << bit << "\n";
				//cout << "Setting ion " << i << "\n";
				//unsigned int prev = bitmask[byte];
				//bitset<32> prv(prev);
				//cout << "Before " << prv << "\n";
				BIT_SET(out[byte], bit); //set a bit
				//unsigned int next = bitmask[byte];
				//bitset<32> nxt(next);
				//cout << "After " << nxt << "\n";
			}
		}
	}
	else {
		out = vector<unsigned char>();
	}
}
