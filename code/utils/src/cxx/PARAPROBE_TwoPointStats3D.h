/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_TWOPOINTSTATSTHREED_H__
#define __PARAPROBE_UTILS_TWOPOINTSTATSTHREED_H__

#include "PARAPROBE_Histogram2D.h"


struct d3dd
{
	apt_real dx;	//components of the difference vector
	apt_real dy;
	apt_real dz;
	apt_real dSQR;
	d3dd() : dx(MYZERO), dy(MYZERO), dz(MYZERO), dSQR(MYZERO) {}
	d3dd(const apt_real _dx, const apt_real _dy, const apt_real _dz ) :
		dx(_dx), dy(_dy), dz(_dz), dSQR( SQR(dx)+SQR(dy)+SQR(dz) ) {}
};

ostream& operator<<(ostream& in, d3dd const & val);


inline bool SortDiffForAscDistance(const d3dd & a, const d3dd & b)
{
	return a.dSQR < b.dSQR;
}


class sdm3d
{
public :
	sdm3d();
	sdm3d( const apt_real rmax, const apt_real width, const apt_int nmax );
	~sdm3d();

	void add( d3dd const & where );

	//##MK::if PCF is sparsely populated maybe a std::map is a better option
	vector<apt_uint> cnts;
	apt_uint cnts_unexpected;

private:
	voxelgrid box;
};

#endif
