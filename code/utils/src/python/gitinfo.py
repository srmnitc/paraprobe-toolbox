#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
# Utility function to identify the hash value of the last git commit to
# specify more details about in which state the tool was at least.
#

import git


def get_repo_last_commit() -> str:
    """Identify the last commit to the repository."""
    try:
        repo = git.Repo(search_parent_directories=True)
        sha = repo.head.object.hexsha
        # is_dirty = repo.is_dirty()
        return sha
    except Exception:
        return None


def get_nexus_submodule_last_commit() -> str:
    """Identify the last commit to the NeXus submodule."""
    try:
        repo = git.Repo(search_parent_directories=True)
        if len(repo.submodules) == 1:
            # repo.submodules[0].exists()
            return repo.submodules[0].hexsha
        return None
    except Exception:
        return None


PARAPROBE_VERSION \
    = f"https://gitlab.com/paraprobe/paraprobe-toolbox/-/commit/{get_repo_last_commit()}"

NEXUS_VERSION \
    = f"https://gitlab.com/paraprobe/paraprobe-toolbox/-/commit/{get_nexus_submodule_last_commit()}"
