#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#


def validate_argument_str(arg):
    """Check if the argument arg fulfills type and value constraints."""
    if not isinstance(arg, str):
        raise TypeError("Argument with value {arg} needs to be a string!")
    if arg == "":
        raise ValueError("Argument with value {arg} needs to be a non-empty string!")
    return True
