/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_NANOCHEM_STRUCTS_H__
#define __PARAPROBE_NANOCHEM_STRUCTS_H__

#include "PARAPROBE_NanochemPQPInterface.h"


struct p3d64
{
	double x;
	double y;
	double z;
	p3d64() : x(0.), y(0.), z(0.) {}
	p3d64(const double _x, const double _y, const double _z) :
		x(_x), y(_y), z(_z) {}
};

ostream& operator<<(ostream& in, p3d64 const & val);


struct arridx_binidx
{
	apt_uint binidx;
	apt_uint ionidx;
	unsigned char i_org;
	unsigned char pad1;
	unsigned char pad2;
	unsigned char pad3;
	arridx_binidx() : binidx(UMX), ionidx(UMX), i_org(0x00), pad1(0x00), pad2(0x00), pad3(0x00) {}
	arridx_binidx( const apt_uint _binidx, const apt_uint _ionidx, const unsigned char _ityp ) :
		binidx(_binidx), ionidx(_ionidx), i_org(_ityp), pad1(0x00), pad2(0x00), pad3(0x00) {}
};


struct binidx_ionidx
{
	apt_uint binidx;
	apt_uint ionidx;
	binidx_ionidx() : binidx(UMX), ionidx(UMX) {}
	binidx_ionidx( const apt_uint _binidx, const apt_uint _ionidx ):
			binidx(_binidx), ionidx(_ionidx) {}
};


struct bin_mgn
{
	size_t start;
	size_t onepastend;
	apt_uint binidx;
	bin_mgn() : start(0), onepastend(0), binidx(-1) {}
	bin_mgn( const size_t _start, const size_t _onepastend, const apt_uint _binidx ) :
		start(_start), onepastend(_onepastend), binidx(_binidx)  {}
};


inline bool SortBinIdxThenIonIdxAscending( const binidx_ionidx & a, const binidx_ionidx & b )
{
	/*
	return ( (a.binidx < b.binidx) ||
			((a.binidx == b.binidx) && (a.ionidx < b.ionidx)) );
	*/
	return a.binidx < b.binidx || (a.binidx == b.binidx && a.ionidx < b.ionidx);
	//first sort by bin, thereafter by ionid
}


struct unrm_info
{
	apt_real SQRmagn;		//||\nabla{x}||, quantifies how large is the gradient
	apt_real dangling_normal_magn;		//related to the length of the cross product used to compute a dangling normal of the triangle
	apt_real cosPhi;		//test unit normal * normalized \nabla{x}, quantifies how parallel is the projection
	unrm_info() : SQRmagn(RMX), dangling_normal_magn(RMX), cosPhi(RMX) {}
	unrm_info( const apt_real _sqrmagn, const apt_real _trimagn, const apt_real _cosphi ) :
		SQRmagn(_sqrmagn), dangling_normal_magn(_trimagn), cosPhi(_cosphi) {}
};


#endif
