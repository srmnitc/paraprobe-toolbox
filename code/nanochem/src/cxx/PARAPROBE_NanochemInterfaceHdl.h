/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_NANOCHEM_INTERFACE_HDL_H__
#define __PARAPROBE_NANOCHEM_INTERFACE_HDL_H__

#include "PARAPROBE_NanochemIsosurfaceHdl.h"


struct halfedge2edge
{
	halfedge2edge(const Mesh& m, vector<edge_descriptor>& edges) :
		m_mesh(m), m_edges(edges) {}
	void operator()(const halfedge_descriptor& h) const
	{
		m_edges.push_back(edge(h, m_mesh));
	}
	const Mesh& m_mesh;
	vector<edge_descriptor>& m_edges;
};


#define DCOM_UNKNOWN	0
#define DCOM_PRE		1
#define DCOM_POST		2

struct mesh_refine_step_info
{
	apt_real target_edge_length;			//nm
	apt_real dcom_radius;					//nm
	apt_uint smoothing_iterations;
	apt_uint stepid;
	unsigned char when;						//DCOM_PRE or DCOM_POST
	unsigned char pad1;
	unsigned char pad2;
	unsigned char pad3;
	mesh_refine_step_info() : 
		target_edge_length(1.0), dcom_radius(0.5), smoothing_iterations(1), stepid(UMX),
		when(DCOM_UNKNOWN), pad1(0x00), pad2(0x00), pad3(0x00) {}
	mesh_refine_step_info( const apt_real _ltarget, const apt_real _dcomr, const apt_uint _liter,
		const apt_uint _stepid, const unsigned char _dcomwhen ) :
		target_edge_length(_ltarget), dcom_radius(_dcomr), smoothing_iterations(_liter), stepid(_stepid),
		when(_dcomwhen), pad1(0x00), pad2(0x00), pad3(0x00) {}
};


class interfaceHdl
{
public:
	interfaceHdl();
	~interfaceHdl();

	//##MK::double tic = MPI_Wtime();

	bool step_1a_initial_interface_extract_roi_and_decorating_ions(
			vector<p3d> const & pp3, vector<p3dinfo> & ionifo, rangeTable & rngifo );
	bool step_1b_initial_interface_extract_report_roi(
			vector<p3d> const & pp3, vector<p3dinfo> & ionifo );
	bool step_2_initial_interface_create_pca_based();
	bool step_3_initial_interface_clip_by_domain( vector<p3d> const & pp3 );
	bool step_4_initial_interface_triangulate();

	//individual steps during the iterative refinement loop
	bool step_5a_intermediate_interface_remesh_isotropically( mesh_refine_step_info const & ifo );
	bool step_5b_intermediate_interface_compute_normals();
	bool step_5c_intermediate_interface_report_pre_dcom( mesh_refine_step_info const & ifo );
	bool step_5d_intermediate_interface_modify_dcom_step( mesh_refine_step_info const & ifo );
	bool step_5e_intermediate_interface_check_self_intersections();
	bool step_5f_intermediate_interface_report_post_dcom( mesh_refine_step_info const & ifo );
	bool step_5_initial_interface_refine_iteratively();

	bool step_6_final_interface_clip_by_edge_model();
	bool step_7_final_interface_report();

	bool write_interface_mesh( Mesh & msh, const string dsnm_prefix,
	mesh_refine_step_info const & ifo, const unsigned char when );

	vector<p3dm1> candidates;					//positions of ions and evaporation ID of the ions which were selected
												//as the decorating iontype defining the location of the interface
	kdTree<p3d> kauri;							//kd-tree of the candidates to speed up the DCOM step
	vector<p3d> tmp_out;						//##MK::reduce the need for this copy

	IE_Plane plane;								//PCA-detected plane representing the initial interface model
	aabb3d domain_aabb;							//axis-aligned bounding box about the entire point cloud
	Polylines polylines;						//polyline representing the intersection between the plane and the bounding box
	vector<CGAL::Triple<int, int, int>> patch;
	CGAL_Polygon ply;							//initial coarsely triangulated hole-filled polygon of the polyline

	vector<mesh_refine_step_info> parms;		//refinement parameter
	Mesh mesh_initial;							//the triangulated surface mesh representing the initial interface model

	Mesh mesh_curr;
	vector<p3d> vnrm_curr;						//##MK::reduce the need for this copy of the vertex normals
	vector<p3d> fnrm_curr;						//##MK::reduce the need for this copy of the face normals
	Mesh mesh_next;								//##MK::reduce the need for copies by
	apt_uint taskid;
	/*
	interface_task_info info;
	profiler isosurf_tictoc;
	*/
};


#endif
