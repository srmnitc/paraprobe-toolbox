v0.4 clean up old code stuff, modify utils to use only apt_* types

### paraprobe-nanochem source file inclusion dependency graph

The header files are included in the following chain:
 1. PARAPROBE_ConfigNanochem.h/.cpp
 2. PARAPROBE_NanochemCGAL.h (--> rename used CGAL datatypes like in surfacer but this can be done for v0.5)
 3. PARAPROBE_NanochemEigen.h
 4. PARAPROBE_NanochemTetGen.h (deactivated)
 5. PARAPROBE_NanochemMCInterface.h/.cpp
 6. PARAPROBE_NanochemMCLewiner2002.h/.cpp --> currently supports only single precision
 7. PARAPROBE_NanochemPQPInterface.h/.cpp
 8. PARAPROBE_NanochemStructs.h/.cpp
 9. PARAPROBE_NanochemSpatialDecomposition.h/.cpp
10. PARAPROBE_NanochemGPU.h/.cu
    PARAPROBE_NanochemTetrahedralize.h/.cpp (deactivated)
11. PARAPROBE_NanochemTriangleSoupClustering.h/.cpp
12. PARAPROBE_NanochemManifoldHdl.h/.cpp
13. PARAPROBE_NanochemIsosurfaceHdl.h/.cpp
14. PARAPROBE_NanochemInterfaceHdl.h/.cpp
15. PARAPROBE_NanochemCompositionHdl.h/.cpp
16. PARAPROBE_NanochemHDF5.h/.cpp (obsolete)
17. PARAPROBE_NanochemXDMF.h/.cpp (obsolete)
18. PARAPROBE_NanochemHdl.h/.cpp
19. PARAPROBE_Nanochem.cpp
