/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_SpatstatHdl.h"


spatstatHdl::spatstatHdl()
{
	window = NULL;
	ions_ion2feat = vector<apt_real>();
	cand = vector<query_info>();
	kauri = kdTree<p3dm5>();
	spatstat_tictoc = profiler();
}


spatstatHdl::~spatstatHdl()
{
	if ( window != NULL ) {
		delete [] window; window = NULL;
	}
}


bool spatstatHdl::read_relevant_input()
{
	double tic = MPI_Wtime();

	if ( read_xyz_from_h5( ConfigSpatstat::InputfileDataset, ConfigSpatstat::ReconstructionDatasetName ) == true &&
			read_ranging_from_h5( ConfigSpatstat::InputfileIonTypes, ConfigSpatstat::IonTypesGroupName ) == true &&
				read_ionlabels_from_h5( ConfigSpatstat::InputfileIonTypes, ConfigSpatstat::IonTypesGroupName ) == true ) {

		cout << "Rank " << "MASTER" << " all relevant input was loaded successfully" << "\n";

		//build also here a default array with pieces of information for all ions
		if ( ions.ionpp3.size() > 0 && ions.ionifo.size() != ions.ionpp3.size() ) {
			try {
				ions.ionifo = vector<p3dinfo>( ions.ionpp3.size(),
						p3dinfo( RMX, UNKNOWNTYPE, UNKNOWNTYPE, 0x01, WINDOW_ION_EXCLUDE ) );
				//assume all ions are infinitely large from the edge, of unknown (default ion type), and multiplicity one (i.e. 0x01)
			}
			catch (bad_alloc &croak) {
				cerr << "Allocation of information array for the ions failed !" << "\n";
				return false;
			}
		}

		if ( ConfigSpatstat::InputfileIonToEdgeDistances != "" ) {
			if ( read_ion2edge_distances() == true ) {
				cout << "Rank " << "MASTER" << " all ion-to-edge distances were loaded successfully" << "\n";
			}
			else {
				cerr << "Rank " << "MASTER" << " reading ion-to-edge distances failed !" << "\n";
				return false;
			}
		}
		//MK::use the fact that ions.ionifo was distance values were initialized to RMX, i.e. all ions are assumed far enough from the edge of the dataset
		//MK::if we are not using an edge model

		if ( ConfigSpatstat::InputfileIonToFeatureDistances != "" ) {
			if ( read_ion2feature_distances() == true ) {
				cout << "Rank " << "MASTER" << " all ion-to-feature distances were loaded successfully" << "\n";
			}
			else {
				cerr << "Rank " << "MASTER" << " reading ion-to-feature distances failed !" << "\n";
				return false;
			}
		}
		else {
			//if we have no clue how far ions are from a feature
			//we assume they are practically "sitting" on the feature so that they will always be analyzed
			ions_ion2feat = vector<apt_real>( ions.ionifo.size(), MYZERO );
		}

		//lets make a quick summary of ion labels ignoring multiplicity, ##MK::move to mpiHdl base class
		map<unsigned char, apt_uint> nominal_ranging;
		for( auto it = ions.ionifo.begin(); it != ions.ionifo.end(); it++ ) {
			map<unsigned char, apt_uint>::iterator thisone = nominal_ranging.find( it->i_org );
			if ( thisone != nominal_ranging.end() ) {
				nominal_ranging[it->i_org]++;
			}
			else {
				nominal_ranging[it->i_org] = 1;
			}
		}
		cout << "Relevant input has the following total counts for the following ion types" << "\n";
		cout << "Total ion count " << ions.ionifo.size() << "\n";
		for( auto jt = nominal_ranging.begin(); jt != nominal_ranging.end(); jt++ ) {
			cout << (int) jt->first << "\t\t" << jt->second << "\n";
		}

		double toc = MPI_Wtime();
		memsnapshot mm = memsnapshot();
		spatstat_tictoc.prof_elpsdtime_and_mem( "ReadRelevantInput", APT_XX, APT_IS_SEQ, mm, tic, toc);
		return true;
	}

	return false;
}


bool spatstatHdl::read_ion2edge_distances()
{
	double tic = MPI_Wtime();

	if ( ions.ionpp3.size() > 0 && ions.ionifo.size() == ions.ionpp3.size() ) {

		if ( read_distance_dsetedge_from_h5( ConfigSpatstat::InputfileIonToEdgeDistances,
				ConfigSpatstat::IonToEdgeDistancesDatasetName ) == true ) {
			cout << "Rank " << "MASTER" << " all ion to primitive distances were loaded successfully" << "\n";
		}
		else {
			cerr << "Rank " << "MASTER" << " reading ion-to-edge distances failed !" << "\n";
			return false;
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	spatstat_tictoc.prof_elpsdtime_and_mem( "ReadIon2EdgeDistances", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool spatstatHdl::read_ion2feature_distances()
{
	double tic = MPI_Wtime();

	if ( ions.ionpp3.size() > 0 ) {

		ions_ion2feat = vector<apt_real>();

		cout << "Loading ion-to-feature distances..." << "\n";
		HdfFiveSeqHdl h5r = HdfFiveSeqHdl( ConfigSpatstat::InputfileIonToFeatureDistances );

		if ( h5r.nexus_read(
				ConfigSpatstat::IonToFeatureDistancesDatasetName, ions_ion2feat ) != MYHDF5_SUCCESS ) { return false; }

		if ( ions_ion2feat.size() != ions.ionpp3.size() || ions_ion2feat.size() != ions.ionifo.size() ) {
			cerr << "Length of ion-to-feature distance array is different to ions.ionpp3 or was not readable !" << "\n";
			return false;
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	spatstat_tictoc.prof_elpsdtime_and_mem( "ReadIon2FeatureDistances", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


void spatstatHdl::randomize_iontype_labels()
{
	double tic = MPI_Wtime();

	mt19937 urng;
	urng.seed( ConfigSpatstat::PRNGWorldSeed );
	urng.discard( ConfigSpatstat::PRNGWarmup );

	vector<unsigned char> permute;
	permute.reserve( ions.ionifo.size() );
	for( size_t i = 0; i < ions.ionifo.size(); i++ ) {
		permute.push_back( ions.ionifo[i].i_org );
	}

	//randomize order on array via uniform Mersenne twister PRNG shuffling
	//while maintaining total number of ions of specific type
	shuffle( permute.begin(), permute.end(), urng);

	for( size_t j = 0; j < ions.ionifo.size(); j++ ) {
		ions.ionifo[j].i_rnd = permute[j];
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	spatstat_tictoc.prof_elpsdtime_and_mem( "RandomizeIonLabels", APT_XX, APT_IS_SEQ, mm, tic, toc);
}


void spatstatHdl::crop_reconstruction_to_analysis_window( spatstat_task const & ifo )
{
	double mytic = MPI_Wtime();

	if ( ions.ionpp3.size() >= UMX ) {
		cerr << "Current implementation does not allow to handle datasets with more than UMX ions !" << "\n";
		return;
	}

	if ( window != NULL ) {
		delete [] window; window = NULL;
	}
	try {
		window = new bool[ions.ionpp3.size()];
	}
	catch(bad_alloc &croak) {
		cerr << "Allocation error window for spatial statistics task " << ifo.taskid << " !" << "\n"; return;
	}

	//assume first that all ions are taken, set then those to false which we wish to filter out
	//the filter pipeline is additive, first take all, optionally filter out spatially, optionally sub-sample, optionally filter out based on types, hit_multiplicity
	for( size_t i = 0; i < ions.ionpp3.size(); i++ ) {
		window[i] = true;
	}

	switch(ifo.WdwMethod)
	{
		case ENTIRE_DATASET:
		{
			//for this filter nothing has to be done, window is already true for all
			break;
		}
		case UNION_OF_PRIMITIVES:
		{
			//for this filter we need to check, for each ion, if the ion is at least inside or on the boundary of one of the primitives
			#pragma omp parallel
			{
				vector<apt_uint> myres;
				apt_uint nions = (apt_uint) ions.ionpp3.size();
				#pragma omp for
				for( apt_uint idx = 0; idx < nions; idx++ ) {
					bool keep = false; //optimize for filtering strongly
					if ( keep == false ) {
						for( auto it = ifo.WdwSpheres.begin(); it != ifo.WdwSpheres.end(); it++ ) {
							if ( it->is_inside( ions.ionpp3[idx] ) == true ) { //in most cases ROI is much smaller than entire dataset
								keep = true;
							}
						}
					}
					if ( keep == false ) {
						for( auto jt = ifo.WdwCylinders.begin(); jt != ifo.WdwCylinders.end(); jt++ ) {
							if ( jt->is_inside( ions.ionpp3[idx] ) == true ) {
								keep = true;
							}
						}
					}
					if ( keep == false ) {
						for( auto kt = ifo.WdwCuboids.begin(); kt != ifo.WdwCuboids.end(); kt++ ) {
							if ( kt->is_inside( ions.ionpp3[idx] ) == true ) {
								keep = true;
							}
						}
					}
					if ( keep == false ) {
						myres.push_back( idx );
					}
				}

				#pragma omp critical
				{
					//flag those ions which should not be kept, i.e. filtered out, so that these wont get considered in the analysis
					for( auto it = myres.begin(); it != myres.end(); it++ ) {
						window[*it] = false;
					}
					myres = vector<apt_uint>();
				}
			}
			break;
		}
		case BITMASKED_POINTS:
		{
			cerr << "Bitmasked points is not implemented yet !" << "\n";
			break;
		}
		default:
		{
			break;
		}
	}

	//post-process linear-subsampling
	apt_uint idmin = ifo.LinearSubSamplingRange.min;
	apt_uint idincr = ifo.LinearSubSamplingRange.incr;
	apt_uint idmax = ifo.LinearSubSamplingRange.max;
	apt_uint nions = (apt_uint) ions.ionpp3.size();
	if ( idmax >= nions ) {
		idmax = nions - 1;
	}
	//MK::the filter switches those ions off (sets mask1 to ANALYZE_NO) which
	//should not be analyzed, because for sub-sampling very often the number of
	//ions chosen is smaller than those filtered off
	for( apt_uint i = 0; i < idmin; i++ ) { //clip evaporation IDs too small
		window[i] = false;
	}
	//omitted += (idmin - 0);
	for( apt_uint j = idmin; j <= idmax; j++ ) {
		if ( ((j - idmin) + 1) % idincr != 0 ) {
			window[j] = false;
			//omitted++; //##MK::make a more elegant solution
		}
	}
	for( apt_uint k = idmax+1; k < nions; k++ ) { //clip evaporation IDs too large
		window[k] = false;
	}
	//omitted += (nions - (idmax+1));

	//iontypes filter
	//apt_uint omitted = 0;
	//shared(omitted)
	#pragma omp parallel
	{
		//thread-local cache
		vector<apt_uint> my_omit_res;
		apt_uint nions = (apt_uint) ions.ionpp3.size();
		#pragma omp for
		for( apt_uint idx = 0; idx < nions; idx++ ) {
			unsigned char ityp = ions.ionifo[idx].i_org;
			if ( ifo.IontypeFilter.is_blacklist == true ) { //filtering for ityp_list acting as a blacklist
				for( auto it = ifo.IontypeFilter.candidates.begin(); it != ifo.IontypeFilter.candidates.end(); it++ ) {
					if ( *it == ityp ) {
						my_omit_res.push_back(idx);
						break;
					}
				}
			}
			if( ifo.IontypeFilter.is_whitelist == true ) { //filtering for ityp_list acting as a whitelist
				bool found = false;
				for( auto it = ifo.IontypeFilter.candidates.begin(); it != ifo.IontypeFilter.candidates.end(); it++ ) {
					if ( *it == ityp ) {
						found = true;
						break;
					}
				}
				if ( found == false ) {
					my_omit_res.push_back(idx);
				}
			}
		}
		#pragma omp barrier
		#pragma omp critical
		{
			//for( auto it = myres.begin(); it != myres.end(); it++ ) {
			for ( auto it = my_omit_res.begin(); it != my_omit_res.end(); it++ ) {
				//ions.ionifo.at(it->first).mask1 = it->second;
				//do not check whether ions have already set to ANALYZE_NO
				window[*it] = false;
			}
			//omitted += (apt_uint) my_omit_res.size();
			//myres = map<apt_uint, unsigned char>();
			my_omit_res = vector<apt_uint>();
		}
	}
	//cout << "Applied iontype remove filter omitted " << omitted << "\n";

	//##MK::hit multiplicity not implemented

	//check filter
	apt_uint accepted = 0;
	apt_uint omitted = 0;
	for( size_t i = 0; i < ions.ionifo.size(); i++ ) {
		if ( window[i] == true ) {
			continue;
		}
		else {
			omitted++;
		}
	}

	accepted = ((apt_uint) ions.ionifo.size()) - omitted;
	cout << "Filter state accepted " << accepted << " omitted " << omitted <<
			" total ion fraction accepted " << ((double) accepted) / ((double) (accepted + omitted)) << "\n";

	double mytoc = MPI_Wtime();
	cout << "Multithreaded cropping of the reconstructed volume to the analysis window took " << (mytoc-mytic) << "\n";
}


void spatstatHdl::analyse_role_of_ions( spatstat_task const & ifo )
{
	double mytic = MPI_Wtime();

	//build a lookup table for the multiplicities with respect to the role of ions as sources and targets respectively
	if ( rng.iontypes.size() >= static_cast<size_t>(U8MX) ) {
		cerr << "The current implementation can map at most 256 iontypes !" << "\n"; return;
	}

	apt_uint nitypes = (apt_uint) rng.iontypes.size();
	//##MK::add if size_t can be mapped on apt_uint
	vector<unsigned char> ityp2multiplicity = vector<unsigned char>( nitypes*2, 0x00 );
	//pair of multiplicity, one pair per iontype first value for source, second value for target
	//get a set of disjoint ivec for the src and trg search queries
	set<unsigned short> unique_src;
	switch(ifo.src_qtyp)
	{
		case RESOLVE_NONE: { break; }  //unique_src not required, all src_mult will be 0 in every case
		case RESOLVE_ALL: { break; } //unique_src not required, all src_mult will be 1 in every case
		case RESOLVE_UNKNOWN: { break; } //unique_src not required, src_mult will be either 1 if ityp == UNKNOWNTYPE, 0 otherwise
		case RESOLVE_ION: { break; } //unique_src not required mult will be either 1 if ityp == ityp, 0 otherwise
		case RESOLVE_ELEMENT:
		{
			//unique_src/trg required all isotope(s)/_hash with N != 0 will be mapped on the same with the respective Z
			for( auto it = ifo.src.begin(); it != ifo.src.end(); it++ ) {
				for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
					if ( it->ivec[i] != 0 ) {
						pair<unsigned char, unsigned char> ZN = isotope_unhash( it->ivec[i] );
						unique_src.insert( isotope_hash(ZN.first, 0) ); //element!, same number of protons, number of neutrons ignored!
					}
					else {
						break;
					} //premature out possible because ivec is sorted for decreasing ivec isotope_hash values
				}
			}
			break;
		}
		case RESOLVE_ISOTOPE:
		{
			//unique_src/trg required all isotope(s)/_hash with N != 0 will be mapped on the same with the respective Z
			for( auto it = ifo.src.begin(); it != ifo.src.end(); it++ ) {
				for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
					if ( it->ivec[i] != 0 ) {
						unique_src.insert( it->ivec[i] );
					}
					else
						break;
					//premature out, ivec is sorted for decreasing ivec isotope_hash values
				}
			}
			break;
		}
		default:
			break;
	}

	set<unsigned short> unique_trg;
	switch(ifo.trg_qtyp)
	{
		case RESOLVE_NONE: { break; }
		case RESOLVE_ALL: { break; }
		case RESOLVE_UNKNOWN: { break; }
		case RESOLVE_ION: { break; }
		case RESOLVE_ELEMENT:
		{
			for( auto jt = ifo.trg.begin(); jt != ifo.trg.end(); jt++ ) {
				for( int j = 0; j < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; j++ ) {
					if ( jt->ivec[j] != 0 ) {
						pair<unsigned char, unsigned char> ZN = isotope_unhash( jt->ivec[j] );
						unique_trg.insert( isotope_hash(ZN.first, 0) );
					}
					else {
						break;
					}
				}
			}
			break;
		}
		case RESOLVE_ISOTOPE:
		{
			for( auto jt = ifo.trg.begin(); jt != ifo.trg.end(); jt++ ) {
				for( int j = 0; j < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; j++ ) {
					if ( jt->ivec[j] != 0 ) {
						unique_trg.insert( jt->ivec[j] );
					}
					else {
						break;
					}
				}
			}
			break;
		}
		default:
		{
			break;
		}
	}

	//an iontype represents an element, isotope/nuclid, or molecular ion
	//an iontype is described by an ivec array which resolves the isotopes (if N != 0) and elements that build the ion
	//there are multiple ways how to interpret the multiplicity of a given ion to a candidate ion
	//historically in atom probe two questions are asked:
	//case 1: querying of exact ion types e.g. H_3O^+ which will result in OHHH
	//as an ivec with all neutron numbers set to 0 and charge +1
	//because O and H are specified as elements only no isotopes are distinguished
	//here the multiplicity is 1 exactly when the ivec's match OHHH == OHHH, multiplicity is zero 0 otherwise
	//case 2: querying elements e.g. carbon or hydrogen, can be done as elements or isotopes
	//here first of all the list of querying elements is understood as a list of OR relations
	//the multiplicity is the total number of times carbon or hydrogen is part of an ion
	//in the example of H3O+ the multiplicity of H is 3, in atom probe jargon this is atomic decomposition
	//if one were to ask though 2^H, 12^C the multiplicity is 0 because the iontype does not resolve isotopes
	//case 3: is a special filter
	for ( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
		unsigned char ityp = it->first;
		unsigned char srcmult = 0x00;
		unsigned char trgmult = 0x00;
		switch(ifo.src_qtyp)
		{
			case RESOLVE_NONE:
			{
				srcmult = 0x00; break;
			}
			case RESOLVE_ALL:
			{
				srcmult = 0x01; break;
			}
			case RESOLVE_UNKNOWN:
			{
				srcmult = ( ityp != UNKNOWNTYPE ) ? 0x00 : 0x01; break;
			}
			case RESOLVE_ION:
			{
				for( auto jt = ifo.src.begin(); jt != ifo.src.end(); jt++ ) {
					if ( it->second.is_the_same_ivec_as( *jt ) == true ) {
						srcmult = 0x01; break;
					}
					//do not break, ifo.src is understood as a list of OR candidates !
					//##MK::currently the charge state is not specified or tested for !
				}
				break;
			}
			case RESOLVE_ELEMENT:
			{
				int mlt = 0;
				for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
					if ( it->second.ivec[i] != 0 ) {
						pair<unsigned char, unsigned char> ZN = isotope_unhash( it->second.ivec[i] );
						//is the element with the number of protons Z in the list of unique_src candidates, remember OR comparison ?
						for( auto kt = unique_src.begin(); kt != unique_src.end(); kt++ ) {
							pair<unsigned char, unsigned char> ZNcand = isotope_unhash( *kt );
							if ( ZN.first == ZNcand.first ) {
								mlt++; break;
								//no need to test further an element can only be one from a set of disjoint elements
							}
						}
					}
					else {
						break;
					}
				}
				//##MK::currently supporting only at most 255 multiplicity
				//this should not be a problem though as MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION << U8MX
				srcmult = ( mlt < static_cast<int>(U8MX) ) ? ((unsigned char) mlt) : 0xFF;
				break;
			}
			case RESOLVE_ISOTOPE:
			{
				int mlt = 0;
				for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
					if ( it->second.ivec[i] != 0 ) {
						//pair<unsigned char, unsigned char> ZN = isotope_unhash( it->second.ivec[i] );
						//is the element with the number of protons Z in the list of unique_src candidates, remember OR comparison ?
						for( auto kt = unique_src.begin(); kt != unique_src.end(); kt++ ) {
							if ( it->second.ivec[i] == *kt ) {
								mlt++; break;
								//no need to test further an isotope can only be one from a set of disjoint isotope
							}
						}
					}
					else {
						break;
					}
				}
				//##MK::currently supporting only at most 255 multiplicity
				//this should not be a problem though as MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION << U8MX
				srcmult = ( mlt < static_cast<int>(U8MX) ) ? ((unsigned char) mlt) : 0xFF;
				break;
			}
			default:
			{
				break;
			}
		}
		switch(ifo.trg_qtyp)
		{
			case RESOLVE_NONE:
			{
				trgmult = 0x00; break;
			}
			case RESOLVE_ALL:
			{
				trgmult = 0x01; break;
			}
			case RESOLVE_UNKNOWN:
			{
				trgmult = ( ityp != UNKNOWNTYPE ) ? 0x00 : 0x01; break;
			}
			case RESOLVE_ION:
			{
				for( auto jt = ifo.trg.begin(); jt != ifo.trg.end(); jt++ ) {
					if ( it->second.is_the_same_ivec_as( *jt ) == true ) {
						trgmult = 0x01; break;
					}
					//do not break, ifo.src is understood as a list of OR candidates !
				}
				break;
			}
			case RESOLVE_ELEMENT:
			{
				int mlt = 0;
				for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
					if ( it->second.ivec[i] != 0 ) {
						pair<unsigned char, unsigned char> ZN = isotope_unhash( it->second.ivec[i] );
						//is the element with the number of protons Z in the list of unique_src candidates, remember OR comparison ?
						for( auto kt = unique_trg.begin(); kt != unique_trg.end(); kt++ ) {
							pair<unsigned char, unsigned char> ZNcand = isotope_unhash( *kt );
							if ( ZN.first == ZNcand.first ) {
								mlt++; break; //no need to test further an element can only be one from a set of disjoint elements
							}
						}
					}
					else {
						break;
					}
				}
				//##MK::currently supporting only at most 255 multiplicity, should not be a problem though as MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION << U8MX
				trgmult = ( mlt < static_cast<int>(U8MX) ) ? ((unsigned char) mlt) : 0xFF;
				break;
			}
			case RESOLVE_ISOTOPE:
			{
				int mlt = 0;
				for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
					if ( it->second.ivec[i] != 0 ) {
						//pair<unsigned char, unsigned char> ZN = isotope_unhash( it->second.ivec[i] );
						//is the element with the number of protons Z in the list of unique_src candidates, remember OR comparison ?
						for( auto kt = unique_trg.begin(); kt != unique_trg.end(); kt++ ) {
							if ( it->second.ivec[i] == *kt ) {
								mlt++; break;
								//no need to test further an isotope can only be one from a set of disjoint isotope
							}
						}
					}
					else {
						break;
					}
				}
				//##MK::currently supporting only at most 255 multiplicity
				//this should not be a problem though as MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION << U8MX
				trgmult = ( mlt < static_cast<int>(U8MX) ) ? ((unsigned char) mlt) : 0xFF;
				break;
			}
			default:
			{
				break;
			}
		}
		size_t itypidx = (size_t) ((int) ityp);
		ityp2multiplicity[2*itypidx+0] = srcmult;
		ityp2multiplicity[2*itypidx+1] = trgmult;
	}
	//ityp2multiplicity is a look-up table to avoid having to execute the above case analysis for every ion position
	//this works because iontypes just segment the point cloud into named sub-point clouds
	//for each of which the analysis case is always the same

	cand = vector<query_info>();
	#pragma omp parallel shared(cand, ityp2multiplicity)
	{
		/*
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();
		*/
		vector<unsigned char> mylookup = ityp2multiplicity;
		//now the lookup tables are more likely to end up in thread-local memory

		vector<query_info> mycand;
		//now multithreaded analysis which ions/positions are relevant at all in the analysis task
		bool use_original_labels = ( ifo.randomized == RANDOMIZE_NO ) ? true : false;

		#pragma omp for
		for( size_t ionidx = 0; ionidx < ions.ionpp3.size(); ionidx++ ) {
			if ( window[ionidx] == true ) { //most analyses are performed on the entire dataset, if not the if is substantially less frequently executed anyway
				//find how far to feature and edge of the dataset
				bool far_from_dedge = ( ions.ionifo[ionidx].dist >= ifo.far_from_dedge ) ? true : false;
				bool close_to_dfeat = ( ions_ion2feat[ionidx] <= ifo.close_to_dfeat ) ? true : false;

				//get iontype
				//apt_uint ityp = (apt_uint) UNKNOWNTYPE;
				apt_uint ityp = ( use_original_labels == true ) ?
						(apt_uint) ions.ionifo[ionidx].i_org : (apt_uint) ions.ionifo[ionidx].i_rnd;

				//otherwise costly case analysis becomes a table lookup
				//identify src_multiplicity
				unsigned char src_mult = ityp2multiplicity[ityp*2+0];
				//identify trg_multiplicity
				unsigned char trg_mult = ityp2multiplicity[ityp*2+1];

				//most spatial statistics analyses evaluate a sub set of ions, normally solutes so often it is more likely
				//that the multiplicity is 0x00 rather than != 0x00 eventually use this for a better case decision
				if ( src_mult != 0x00 || trg_mult != 0x00 ) {
					mycand.push_back( query_info(ionidx, src_mult, trg_mult, far_from_dedge, close_to_dfeat) );
				}
			} //ion in composite window
		} //next ion

		#pragma omp critical
		{
			cand.insert( cand.end(), mycand.begin(), mycand.end() );
			mycand = vector<query_info>();
		}
		//how the elements end up in cand is non-deterministic, entries will be organized properly though upon tree building
	}

	cout << "Multithreaded analysis of ion roles returned a total of " << cand.size() << " ions relevant for task " << ifo.taskid << "\n";

	double mytoc = MPI_Wtime();
	cout << "Multithreaded filtering and identifying role of ions for task " << ifo.taskid << " took " << (mytoc-mytic) << "\n";
}


void spatstatHdl::build_query_kdtree( spatstat_task const & ifo )
{
	double mytic = MPI_Wtime();

	if ( cand.size() >= static_cast<size_t>(UMX) ) {
		cerr << "The point cloud is too large, it cannot be used to construct a kdTree from !" << "\n"; return;
	}

	kauri = kdTree<p3dm5>();
	vector<p3dm5> tmp_pos = vector<p3dm5>();
	vector<apt_uint> tmp_prm = vector<apt_uint>();

	apt_uint n_cand = (apt_uint) cand.size();
	for( apt_uint i = 0; i < n_cand; i++ ) {
		size_t ionidx = (size_t) cand[i].ionidx;
		tmp_pos.push_back( p3dm5( ions.ionpp3[ionidx].x, ions.ionpp3[ionidx].y, ions.ionpp3[ionidx].z,
				cand[i].ionidx, cand[i].src_mult, cand[i].trg_mult, cand[i].far_from_dedge, cand[i].close_to_dfeat ) );
	}
	cand = vector<query_info>();

	kauri.kd_build( tmp_pos, tmp_prm, ConfigShared::MaxLeafsKDTree );
	kauri.kd_pack( tmp_pos, tmp_prm );
	if ( kauri.kd_verify() == false ) {
		kauri = kdTree<p3dm5>();
		cerr << "Verification error for kdTree !" << "\n"; return;
	}

	cout << "kauri.domain" << "\n";
	cout << kauri.domain << "\n";
	cout << "kauri.points.size()" << "\n";
	cout << kauri.points.size() << "\n";
	cout << "kauri.nodes.size()" << "\n";
	cout << kauri.nodes.size() << "\n";

	double mytoc = MPI_Wtime();
	cout << "Sequential building the kdtree of relevant ions for task " << ifo.taskid << " took " << (mytoc-mytic) << "\n";
}


void spatstatHdl::compute_statistics( spatstat_task const & ifo )
{
	double mytic = MPI_Wtime();
	double mytoc = mytic;

	switch(ifo.statstyp)
	{
		case SPATSTAT_KNN:
		{
			lival<double> binning = lival<double>();
			binning = lival<double>( ifo.knn_ifo.hist1d.min, ifo.knn_ifo.hist1d.incr, ifo.knn_ifo.hist1d.max );
			histogram hst1d = histogram( binning );

			#pragma omp parallel shared(binning, hst1d, ifo)
			{
				/*
				int mt = omp_get_thread_num();
				int nt = omp_get_num_threads();
				*/
				lival<double> mybinning = binning;
				histogram myhst1d = histogram( mybinning );
				knn_info myknn = ifo.knn_ifo;
				size_t kth = (size_t) myknn.kth;
				apt_real Rsqr = SQR(myknn.hist1d.max);
				#pragma omp master
				{
					cout << "SQR(myknn.hist1d.max) " << Rsqr << "nm^2" << "\n";
				}

				#pragma omp for schedule(dynamic) nowait
				for( size_t i = 0; i < kauri.points.size(); i++ ) { //query tree locations
					//get candidate check if this one has to be processed
					if ( kauri.points[i].m1 != 0x00 ) { //a source position, where we place a ROI
						if( kauri.points[i].m3 == true ) { //a position that is far enough from the edge of the dataset
							if ( kauri.points[i].m4 == true ) { //a position that is close enough to relevant feature

								p3dm5 me = kauri.points[i];
								vector<sqrd_mltply> neighbors;
								kauri.kd_query_exclude_target( me, Rsqr, neighbors );

								vector<apt_real> dsqr;
								for( auto kt = neighbors.begin(); kt != neighbors.end(); kt++ ) {
									if ( kt->mult > 0 ) {
										dsqr.push_back( kt->sqrd );
									}
								}
								if ( dsqr.size() >= kth ) { //there is a neighbor of relevant type within rmax-ball, distances between points here are squared

									nth_element( dsqr.begin(), dsqr.begin() + kth, dsqr.end() ); //##MK::##-1 ?
									apt_real dknn = sqrt( dsqr[kth - 1] ); //##MK::##-1 because me is not part of the list
									myhst1d.add_inside( dknn, (apt_uint) me.m1 );
									//bookkeep as many counts as multiplicity for the source point me
									//e.g. if me is assumed a C:C molecular ion we also state with the ranging that we
									//cannot disentangle the location of the two carbon atoms better than point.x,y,z
									//but there are still two carbon atoms, so multiplicity is 2
									//clearly there is a bias as the two carbon atoms are assumed at the same location
									//but this is the general limitation of techniques like reconstructed atom probe/field-ion microscopy data
									//because clearly the reconstruction may bias the distances, see overview paper about this from Gault Freysoldt and Klaes
								}
								/*else {
									myhst1d.add_upper( srcmltply );
								}*/
							}
						} //done processing the ROI
					} //next relevant ion
				}
				//consolidate thread-local buffers, because of nowait the thread does this as soon as it has processed its workload
				#pragma omp critical
				{
					hst1d.add_counts( myhst1d );
				}
			} //end of parallel region

			//hst1d.report();

			if ( write_knn_results( hst1d, ifo ) == MYHDF5_SUCCESS ) {
				mytoc = MPI_Wtime();
				cout << "Writing results for task " << ifo.taskid << " took " << (mytoc-mytic) << "\n";
			}
			break;
		}
		case SPATSTAT_RDF:
		{
			lival<double> binning = lival<double>();
			binning = lival<double>( ifo.rdf_ifo.hist1d.min, ifo.rdf_ifo.hist1d.incr, ifo.rdf_ifo.hist1d.max );
			histogram hst1d = histogram( binning );

			#pragma omp parallel shared(binning, hst1d, ifo)
			{
				/*
				int mt = omp_get_thread_num();
				int nt = omp_get_num_threads();
				*/
				lival<double> mybinning = binning;
				histogram myhst1d = histogram( mybinning );
				rdf_info myrdf = ifo.rdf_ifo;
				apt_real Rsqr = SQR(myrdf.hist1d.max);
				#pragma omp master
				{
					cout << "SQR(myrdf.hist1d.max) " << Rsqr << "nm^2" << "\n";
				}

				#pragma omp for schedule(dynamic) nowait
				for( size_t i = 0; i < kauri.points.size(); i++ ) { //query tree locations
					//get candidate check if this one has to be processed
					if ( kauri.points[i].m1 != 0x00 ) { //a source position, where we place a ROI
						if( kauri.points[i].m3 == true ) { //a position that is far enough from the edge of the dataset
							if ( kauri.points[i].m4 == true ) { //a position that is close enough to relevant feature

								p3dm5 me = kauri.points[i];
								vector<sqrd_mltply> neighbors;
								kauri.kd_query_exclude_target( me, Rsqr, neighbors );

								/*
								sort( nbors.begin(), nbors.end(), SortAscSquaredDistance );
								*/

								for( auto it = neighbors.begin(); it != neighbors.end(); it++ ) {
									if ( it->mult > 0 ) {
										myhst1d.add_inside( sqrt(it->sqrd), it->mult );
									}
								}
							}
						} //done processing the ROI
					} //next relevant ion
				}
				//consolidate thread-local buffers, because of nowait the thread does this as soon as it has processed its workload
				#pragma omp critical
				{
					hst1d.add_counts( myhst1d );
				}
			} //end of parallel region

			//hst1d.report();

			if ( write_rdf_results( hst1d, ifo ) == MYHDF5_SUCCESS ) {
				mytoc = MPI_Wtime();
				cout << "Writing results for task " << ifo.taskid << " took " << (mytoc-mytic) << "\n";
			}
			break;
		}
		default:
		{
			break;
		}
	}

	//write randomized labels to file although the above randomization is deterministic
	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string dsnm = "";

	apt_uint entry_id = 1;
	apt_uint proc_id = ifo.taskid + 1;
	grpnm = "/entry" + to_string(entry_id) + "/spatial_statistics" + to_string(proc_id);
	if ( h5w.nexus_path_exists( grpnm ) == false ) {
		if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return; }
	}

	dsnm = grpnm + "/iontypes_randomized";
	vector<unsigned char> u08 = vector<unsigned char>( ions.ionifo.size(), UNKNOWNTYPE );
	for( size_t i = 0; i < ions.ionifo.size(); i++ ) {
		u08[i] = ions.ionifo[i].i_rnd;
	}
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({u08.size()}, {u08.size()},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		u08,
		anno ) != MYHDF5_SUCCESS ) { return; }
	u08 = vector<unsigned char>();

	grpnm = "/entry" + to_string(entry_id) + "/spatial_statistics" + to_string(proc_id) + "/window";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcs_filter_boolean_mask") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return; }

	dsnm = grpnm + "/number_of_ions";
	apt_uint n_ions = (apt_uint) ions.ionifo.size();
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, n_ions, anno ) != MYHDF5_SUCCESS ) { return; }

	dsnm = grpnm + "/bitdepth";
	apt_uint bit_depth = sizeof(unsigned char) * 8;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, bit_depth, anno ) != MYHDF5_SUCCESS ) { return; }

	dsnm = grpnm + "/mask";
	vector<unsigned char> u08_bitfield;
	BitPacker pack_bits;
	pack_bits.bool_to_bitpacked_uint8( window, ions.ionpp3.size(), u08_bitfield );
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({u08_bitfield.size()}, {u08_bitfield.size()},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		u08_bitfield,
		anno ) != MYHDF5_SUCCESS ) { return; }
	u08_bitfield = vector<unsigned char>();

	mytoc = MPI_Wtime();
	cout << "Multithreaded computing of spatial statistics for task " << ifo.taskid << " took " << (mytoc-mytic) << "\n";
	mytic = MPI_Wtime();
}


void spatstatHdl::execute_spatial_statistics_workpackage()
{
	double tic = MPI_Wtime();

	//##MK::inject here a sophisticated task scheduler which executes tasks that could reutilized queried positions
	//##MK::in such a way that you only create the query structure once and reutilize

	for( auto tskit = ConfigSpatstat::SpatialStatisticsTasks.begin(); tskit != ConfigSpatstat::SpatialStatisticsTasks.end(); tskit++ ) {

		//filter from all ions of the dataset those which are in the analysis window
		crop_reconstruction_to_analysis_window( *tskit );

		//first use the original labels
		//identify which of the ions can make at all a contribution to this task to build a tree that is optimized for the given task
		analyse_role_of_ions( *tskit );

		//build a kdtree to query ion locations spatially in an efficient manner
		build_query_kdtree( *tskit );

		//process the query and create the statistics
		compute_statistics( *tskit );

		//##MK::repeat the above three steps but this time for randomized labels
	}

	double toc = MPI_Wtime();
	cout << "execute_spatial_statistics_workpackage took " << (toc-tic) << "\n";
}


int spatstatHdl::write_knn_results( histogram & hst, spatstat_task const & ifo )
{
	//write randomized labels to file although the above randomization is deterministic
	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string dsnm = "";

	apt_uint entry_id = 1;
	apt_uint proc_id = ifo.taskid + 1;
	grpnm = "/entry" + to_string(entry_id) + "/spatial_statistics" + to_string(proc_id);
	anno = ioAttributes();
	anno.add( "NX_class", string("NXapm_paraprobe_tool_results") );
	if ( h5w.nexus_path_exists( grpnm ) == false ) {
		if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }
	}

	grpnm += "/knn";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXprocess") );
	if ( h5w.nexus_path_exists( grpnm ) == false ) {
		if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }
	}

	switch(ifo.statstyp)
	{
		case SPATSTAT_KNN:
		{
			double total = hst.cnts_lowest;
			for( auto it = hst.cnts.begin(); it != hst.cnts.end(); it++ ) {
				total += *it;
			}
			total += hst.cnts_highest;

			if ( total > EPSILON ) {
				dsnm = grpnm + "/distance";
				vector<double> real;
				real.push_back(ifo.knn_ifo.hist1d.min);
				for( apt_uint b = 0; b < hst.nbins; b++ ) {
					real.push_back( hst.right(b) );
				}
				real.push_back( RMX );
				anno = ioAttributes();
				anno.add( "unit", string("nm") );
				if ( h5w.nexus_write(
					dsnm,
					io_info({real.size()}, {real.size()},
							MYHDF5_COMPRESSION_GZIP, 0x01),
					real,
					anno ) != MYHDF5_SUCCESS ) { return false; }
				real = vector<double>();

				dsnm = grpnm + "/probability_mass";
				real.push_back( hst.cnts_lowest );
				for( apt_uint b = 0; b < hst.nbins; b++ ) {
					real.push_back( hst.report(b) );
				}
				real.push_back( hst.cnts_highest );
				anno = ioAttributes();
				if ( h5w.nexus_write(
					dsnm,
					io_info({real.size()}, {real.size()},
							MYHDF5_COMPRESSION_GZIP, 0x01),
					real,
					anno ) != MYHDF5_SUCCESS ) { return false; }
				real = vector<double>();

				dsnm = grpnm + "/cumulated";
				double cumsum = MYZERO + hst.cnts_lowest;
				real.push_back( cumsum );
				for( apt_uint b = 0; b < hst.nbins; b++ ) {
					cumsum += hst.report(b);
					real.push_back( cumsum );
				}
				cumsum += hst.cnts_highest;
				real.push_back( cumsum );
				anno = ioAttributes();
				if ( h5w.nexus_write(
					dsnm,
					io_info({real.size()}, {real.size()},
							MYHDF5_COMPRESSION_GZIP, 0x01),
					real,
					anno ) != MYHDF5_SUCCESS ) { return false; }

				//reuse real in-place and divide by total
				dsnm = grpnm + "/cumulated_normalized";
				for( apt_uint j = 0; j < real.size(); j++ ) {
					real[j] /= total;
				}
				anno = ioAttributes();
				if ( h5w.nexus_write(
					dsnm,
					io_info({real.size()}, {real.size()},
							MYHDF5_COMPRESSION_GZIP, 0x01),
					real,
					anno ) != MYHDF5_SUCCESS ) { return false; }
				real = vector<double>();
			}
			break;
		}
		default:
			break;
	}
  	return MYHDF5_SUCCESS;
}


int spatstatHdl::write_rdf_results( histogram & hst, spatstat_task const & ifo )
{
	//write randomized labels to file although the above randomization is deterministic
	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string dsnm = "";

	apt_uint entry_id = 1;
	apt_uint proc_id = ifo.taskid + 1;
	grpnm = "/entry" + to_string(entry_id) + "/spatial_statistics" + to_string(proc_id);
	anno = ioAttributes();
	anno.add( "NX_class", string("NXapm_paraprobe_tool_results") );
	if ( h5w.nexus_path_exists( grpnm ) == false ) {
		if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }
	}

	grpnm += "/rdf";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXprocess") );
	if ( h5w.nexus_path_exists( grpnm ) == false ) {
		if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }
	}

	switch(ifo.statstyp)
	{
		case SPATSTAT_RDF:
		{
			double total = hst.cnts_lowest;
			for( auto it = hst.cnts.begin(); it != hst.cnts.end(); it++ ) {
				total += *it;
			}
			total += hst.cnts_highest;

			if ( total > EPSILON ) {
				dsnm = grpnm + "/distance";
				vector<double> real;
				real.push_back(ifo.rdf_ifo.hist1d.min);
				for( apt_uint b = 0; b < hst.nbins; b++ ) {
					real.push_back( hst.right(b) );
				}
				real.push_back( RMX );
				anno = ioAttributes();
				anno.add( "unit", string("nm") );
				if ( h5w.nexus_write(
					dsnm,
					io_info({real.size()}, {real.size()},
							MYHDF5_COMPRESSION_GZIP, 0x01),
					real,
					anno ) != MYHDF5_SUCCESS ) { return false; }
				real = vector<double>();

				dsnm = grpnm + "/probability_mass";
				real.push_back( hst.cnts_lowest );
				for( apt_uint b = 0; b < hst.nbins; b++ ) {
					real.push_back( hst.report(b) );
				}
				real.push_back( hst.cnts_highest );
				anno = ioAttributes();
				if ( h5w.nexus_write(
					dsnm,
					io_info({real.size()}, {real.size()},
							MYHDF5_COMPRESSION_GZIP, 0x01),
					real,
					anno ) != MYHDF5_SUCCESS ) { return false; }
				real = vector<double>();

				dsnm = grpnm + "/cumulated";
				double cumsum = MYZERO + hst.cnts_lowest;
				real.push_back( cumsum );
				for( apt_uint b = 0; b < hst.nbins; b++ ) {
					cumsum += hst.report(b);
					real.push_back( cumsum );
				}
				cumsum += hst.cnts_highest;
				real.push_back( cumsum );
				anno = ioAttributes();
				if ( h5w.nexus_write(
					dsnm,
					io_info({real.size()}, {real.size()},
							MYHDF5_COMPRESSION_GZIP, 0x01),
					real,
					anno ) != MYHDF5_SUCCESS ) { return false; }

				//reuse real in-place and divide by total
				dsnm = grpnm + "/cumulated_normalized";
				for( apt_uint j = 0; j < real.size(); j++ ) {
					real[j] /= total;
				}
				anno = ioAttributes();
				if ( h5w.nexus_write(
					dsnm,
					io_info({real.size()}, {real.size()},
							MYHDF5_COMPRESSION_GZIP, 0x01),
					real,
					anno ) != MYHDF5_SUCCESS ) { return false; }
				real = vector<double>();
			}
			break;
		}
		default:
			break;
	}
	return MYHDF5_SUCCESS;
}
