/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_SpatstatHdl.h"

bool parse_configuration_from_nexus()
{
	tool_startup( "paraprobe-spatstat" );
	if ( ConfigSpatstat::read_config_from_nexus( ConfigShared::ConfigurationFile ) == true ) {
		cout << "Configuration from file " << ConfigShared::ConfigurationFile << " was accepted" << "\n";
		cout << "This analysis has SimulationID " << "SimID." <<  ConfigShared::SimID << "\n";
		cout << "Results of this analysis are written to " << ConfigShared::OutputfileName << "\n";
		cout << endl;
		return true;
	}
	cerr << "Parsing configuration failed !" << "\n";
	return false;
}


void characterize_spatial_statistics( const int r, const int nr, char** pargv )
{

	double tic = MPI_Wtime();

	//allocate process-level instance of a nanochemHdl. The instance handles all process-internal
	//processing, the instances synchronize across and communicate with each other during execution
	spatstatHdl* sps = NULL;
	int localhealth = 1;
	try {
		sps = new spatstatHdl;
		sps->set_myrank(r);
		sps->set_nranks(nr);
		sps->commit_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Rank " << r << " allocating spatstatHdl class object failed !" << "\n"; localhealth = 0;
	}

	//do we have all processes on board?
	if ( sps->all_healthy_still( localhealth ) == false ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete sps; sps = NULL; return;
	}

	//let master read input and broadcast to all processes
	if ( sps->get_myrank() == MASTER ) {

		if ( sps->read_relevant_input() == false ) {
			cout << "Rank " << sps->get_myrank() << " failed reading essential pieces of the relevant input !" << "\n";
			localhealth = 0;
		}
	}
	/*
	//MPI_Barrier( MPI_COMM_WORLD );
	if ( sps->all_healthy_still( localhealth ) == false ) {
		cerr << "Rank " << sps->get_myrank() << " has recognized that not all data have arrived at the master !" << "\n";
		delete sps; sps = NULL; return;
	}
	*/

	sps->randomize_iontype_labels();

	sps->execute_spatial_statistics_workpackage();

	//sps->spatstat_tictoc.spit_profiling( "Spatstat", ConfigShared::SimID, sps->get_myrank() );

	delete sps; sps = NULL;

	double toc = MPI_Wtime();
	cout << "characterize_spatial_statistics on rank " << r << " took " << (toc-tic) << "\n";
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
	string start_time = timestamp_now_iso8601();

	if ( argc == 1 || argc == 2 ) {
		string cmd_option = "--help";
		if ( argc == 2 ) { cmd_option = argv[1]; };
		command_line_help( cmd_option, "spatstat" );
		return 0;
	}
	else if ( argc == 3 ) {
		ConfigShared::SimID = stoul( argv[SIMID] );
		ConfigShared::ConfigurationFile = argv[CONTROLFILE];
		ConfigShared::OutputfilePrefix = "PARAPROBE.Spatstat.Results.SimID." + to_string(ConfigShared::SimID);
		ConfigShared::OutputfileName = ConfigShared::OutputfilePrefix + ".nxs";
		if ( init_results_nxs( ConfigShared::OutputfileName, "spatstat", start_time, 1 ) == false ) {
			return 0;
		}
	}
	else {
		return 0;
	}

//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	if ( parse_configuration_from_nexus() == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks!" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else {
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}

	if ( nr == SINGLEPROCESS ) {

		cout << "Rank " << r << " initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";

//EXECUTE SPECIFIC TASK
		characterize_spatial_statistics( r, nr, argv );
	}
	else {
		cerr << "Rank " << r << " currently paraprobe-nanochem is implemented for a single process only !" << "\n";
	}

//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	cout << "Rank " << r << " left MPI_COMM_WORLD deconstructed" << "\n";

	if ( finish_results_nxs( ConfigShared::OutputfileName, start_time, tic, 1 ) == true ) {
		cout << "paraprobe-spatstat success" << endl;
	}
	return 0;
}
