/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_ConfigDistancer.h"


ostream& operator<<(ostream& in, trimesh_info const & val)
{
	in << "h5fn " << val.h5fn << "\n";
	in << "dsnm_verts " << val.dsnm_verts << "\n";
	in << "dsnm_facets " << val.dsnm_facets << "\n";
	in << "dsnm_vnormals " << val.dsnm_vnormals << "\n";
	in << "dsnm_fnormals " << val.dsnm_fnormals << "\n";
	in << "dsnm_patchid " << val.dsnm_patchid << "\n";
	in << "patch_identifier_filter" << "\n";
	in << val.patch_identifier_filter << "\n";
	return in;
}


ostream& operator<<(ostream& in, distancing_task const & val)
{
	in << "taskid " << val.taskid << "\n";
	in << "trimeshes" << "\n";
	for( auto it = val.trimeshes.begin(); it != val.trimeshes.end(); it++ ) {
		in << *it << "\n";
	}
	return in;
}


string ConfigDistancer::InputfileDataset = "";
string ConfigDistancer::ReconstructionDatasetName = "";
string ConfigDistancer::MassToChargeDatasetName = "";
string ConfigDistancer::InputfileIonTypes = "";
string ConfigDistancer::IonTypesGroupName = "";

WINDOWING_METHOD ConfigDistancer::WindowingMethod = ENTIRE_DATASET;
vector<roi_sphere> ConfigDistancer::WindowingSpheres = vector<roi_sphere>();
vector<roi_rotated_cylinder> ConfigDistancer::WindowingCylinders = vector<roi_rotated_cylinder>();
vector<roi_rotated_cuboid> ConfigDistancer::WindowingCuboids = vector<roi_rotated_cuboid>();
vector<roi_polyhedron> ConfigDistancer::WindowingPolyhedra = vector<roi_polyhedron>();
lival<apt_uint> ConfigDistancer::LinearSubSamplingRange = lival<apt_uint>( 0, 1, UMX );
match_filter<unsigned char> ConfigDistancer::IontypeFilter = match_filter<unsigned char>();
match_filter<unsigned char> ConfigDistancer::HitMultiplicityFilter = match_filter<unsigned char>();

DISTANCING_METHOD ConfigDistancer::DistancingMethod = DISTANCING_NONE;
apt_real ConfigDistancer::DistancingRadiusMax = MYZERO;
vector<distancing_task> ConfigDistancer::DistancingTasks = vector<distancing_task>();

lival<apt_real> ConfigDistancer::AdvDistancingBinWidthRange = lival<apt_real>( 1., 2., 10. );
//for compute approximate distances during iterative pruning start with 10nm bins and reduce by 2nm down to 1nm
//use then these approximate distances for the exact distance computation
bool ConfigDistancer::IOStoreDistances = true;
apt_real ConfigDistancer::ItrvQryDistIfoHdlBinWidthMin = MYONE; //nm
apt_real ConfigDistancer::ItrvQryDistEpsilon = 10.*EPSILON; //nm


bool ConfigDistancer::read_config_from_nexus( const string nx5fn )
{
	if ( ConfigShared::SimID > 0 ) {
		cout << "ConfigDistancer::" << "\n";
		cout << "Reading configuration from " << nx5fn << "\n";

		HdfFiveSeqHdl h5r = HdfFiveSeqHdl( nx5fn );
		string grpnm = "";
		string dsnm = "";

		//##MK::currently this tool assumes and works only with a single analysis task defined

		apt_uint number_of_processes = 1;
		apt_uint entry_id = 1;
		grpnm = "/entry" + to_string(entry_id);
		for ( apt_uint tskid = 0; tskid < number_of_processes; tskid++ ) {
			//##MK::currently this tool assumes and works only with a single analysis task defined
			cout << "Reading configuration for task " << tskid << "\n";
			//apt_uint proc_id = tskid + 1;

			grpnm = "/entry" + to_string(entry_id) + "/point_to_triangle";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				string path = "";
				if ( h5r.nexus_read( grpnm + "/reconstruction/path", path ) != MYHDF5_SUCCESS ) { return false; }
				InputfileDataset = path_handling( path );
				cout << "InputfileDataset " << InputfileDataset << "\n";
				if ( h5r.nexus_read( grpnm + "/reconstruction/position", ReconstructionDatasetName ) != MYHDF5_SUCCESS ) { return false; }
				cout << "ReconstructionDatasetName " << ReconstructionDatasetName << "\n";
				if ( h5r.nexus_read( grpnm + "/reconstruction/mass_to_charge", MassToChargeDatasetName ) != MYHDF5_SUCCESS ) { return false; }
				cout << "MassToChargeDatasetName " << MassToChargeDatasetName << "\n";
				path = "";
				if ( h5r.nexus_read( grpnm + "/ranging/path", path ) != MYHDF5_SUCCESS ) { return false; }
				InputfileIonTypes = path_handling( path );
				cout << "InputfileIonTypes " << InputfileIonTypes << "\n";
				if ( h5r.nexus_read( grpnm + "/ranging/ranging_definitions", IonTypesGroupName ) != MYHDF5_SUCCESS ) { return false; }
		    	cout << "IonTypesGroupName " << IonTypesGroupName << "\n";
			}

			DistancingTasks = vector<distancing_task>();
			DistancingTasks.push_back( distancing_task() );
			DistancingTasks.back().taskid = tskid; //UMX;

			//optional spatial filtering
			WindowingMethod = ENTIRE_DATASET;
			grpnm = "/entry" + to_string(entry_id) + "/point_to_triangle/spatial_filter";
			string str = "";
			if ( h5r.nexus_read( grpnm + "/windowing_method", str ) != MYHDF5_SUCCESS ) { return false; }

			if ( str.compare("union_of_primitives") == 0 ) {
				WindowingMethod = UNION_OF_PRIMITIVES;

				grpnm = "/entry" + to_string(entry_id) + "/point_to_triangle/spatial_filter/ellipsoid_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_ellipsoids = 0;
					if ( h5r.nexus_read( grpnm + "/cardinality", n_ellipsoids ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_ellipsoids > 0 ) {
						vector<apt_real> center;
						if ( h5r.nexus_read( grpnm + "/center", center ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> half_axes;
						if ( h5r.nexus_read( grpnm + "/half_axes_radii", half_axes ) != MYHDF5_SUCCESS ) { return false; }
						//orientation is not read because ellipsoids are here assumed as spheres
						if ( center.size() / 3 == n_ellipsoids && half_axes.size() / 3 == n_ellipsoids ) {
							for ( apt_uint i = 0; i < n_ellipsoids; i++ ) {
								WindowingSpheres.push_back( roi_sphere(
									p3d(center[3*i+0], center[3*i+1], center[3*i+2]), half_axes[3*i+0]) );
							}
						}
						center = vector<apt_real>();
						half_axes = vector<apt_real>();
					}
				}

				grpnm = "/entry" + to_string(entry_id) + "/point_to_triangle/spatial_filter/cylinder_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_cylinders = 0;
					if ( h5r.nexus_read( grpnm + "/cardinality", n_cylinders ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_cylinders > 0 ) {
						vector<apt_real> center;
						if ( h5r.nexus_read( grpnm + "/center", center ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> height;
						if ( h5r.nexus_read( grpnm + "/height", height ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> radii;
						if ( h5r.nexus_read( grpnm + "/radii", radii ) != MYHDF5_SUCCESS ) { return false; }
						if ( center.size() / 3 == n_cylinders &&
								height.size() / 3 == n_cylinders &&
									radii.size() == n_cylinders ) {
							for ( apt_uint i = 0; i < n_cylinders; i++ ) {
								WindowingCylinders.push_back( roi_rotated_cylinder(
										p3d(center[3*i+0] - 0.5*height[3*i+0],
											center[3*i+1] - 0.5*height[3*i+1],
											center[3*i+2] - 0.5*height[3*i+2]),
										p3d(center[3*i+0] + 0.5*height[3*i+0],
											center[3*i+1] + 0.5*height[3*i+1],
											center[3*i+2] + 0.5*height[3*i+2]),
										radii[i] ) );
							}
						}
						center = vector<apt_real>();
						height = vector<apt_real>();
						radii = vector<apt_real>();
					}
				}

				grpnm = "/entry" + to_string(entry_id) + "/point_to_triangle/spatial_filter/hexahedron_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_hexahedra = 0;
					if ( h5r.nexus_read( grpnm + "/cardinality", n_hexahedra ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_hexahedra > 0 ) {
						vector<apt_real> vrts;
						if ( h5r.nexus_read( grpnm + "/hexahedra/vertices", vrts ) != MYHDF5_SUCCESS ) { return false; }
						if ( vrts.size() / (8 * 3) == n_hexahedra ) {
							for ( apt_uint i = 0; i < n_hexahedra; i++ ) {
								vector<p3d> corners;
								for ( int j = 0; j < 8; j++ ) {
									corners.push_back( p3d(
											vrts[8*3*i+3*j+0], vrts[8*3*i+3*j+1], vrts[8*3*i+3*j+2]) );
								}
								WindowingCuboids.push_back( roi_rotated_cuboid( corners ) );
								corners = vector<p3d>();
							}
						}
						vrts = vector<apt_real>();
					}
				}

				cout << "Analyzed union_of_primitives" << "\n";
				cout << "WindowingSpheres.size() " << WindowingSpheres.size() << "\n";
				cout << "WindowingCylinders.size() " << WindowingCylinders.size() << "\n";
				cout << "WindowingCuboids.size() " << WindowingCuboids.size() << "\n";
			}

			//optional sub-sampling filter for ion/evaporation ID
			grpnm = "/entry" + to_string(entry_id) + "/point_to_triangle/evaporation_id_filter";
			if( h5r.nexus_path_exists( grpnm ) == true ) {
				vector<apt_uint> uint;
				if ( h5r.nexus_read( grpnm + "/min_incr_max", uint ) != MYHDF5_SUCCESS ) { return false; }
				if ( uint.size() == 3 ) {
					LinearSubSamplingRange = lival<apt_uint>( uint[0], uint[1], uint[2] );
				}
			}
			cout << LinearSubSamplingRange << "\n";

			//optional match filter for iontypes
			grpnm = "/entry" + to_string(entry_id) + "/point_to_triangle/iontype_filter";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				string filter_kind = "";
				if ( h5r.nexus_read( grpnm + "/method", filter_kind ) != MYHDF5_SUCCESS ) { return false; }
				vector<apt_uint> uint;
				if ( h5r.nexus_read( grpnm + "/match", uint ) != MYHDF5_SUCCESS ) { return false; }
				vector<unsigned char> lst;
				for( auto it = uint.begin(); it != uint.end(); it++ ) {
					if ( *it < 256 ) {
						lst.push_back( (unsigned char) (int) *it );
					}
				}
				IontypeFilter = match_filter<unsigned char>( lst, filter_kind );
			}
			cout << IontypeFilter << "\n";

			//optional match filter for hit multiplicity
			grpnm = "/entry" + to_string(entry_id) + "/point_to_triangle/hit_multiplicity_filter";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				string filter_kind = "";
				if ( h5r.nexus_read( grpnm + "/method", filter_kind ) != MYHDF5_SUCCESS ) { return false; }
				vector<apt_uint> uint;
				if ( h5r.nexus_read( grpnm + "/match", uint ) != MYHDF5_SUCCESS ) { return false; }
				vector<unsigned char> lst;
				for( auto it = uint.begin(); it != uint.end(); it++ ) {
					if ( *it < 256 ) {
						lst.push_back( (unsigned char) (int) *it );
					}
				}
				HitMultiplicityFilter = match_filter<unsigned char>( lst, filter_kind );
			}
			cout << HitMultiplicityFilter << "\n";

			grpnm = "/entry" + to_string(entry_id) + "/point_to_triangle";
			apt_uint n_files = 1;
			//TODO::auto support more triangle sets
			//if ( h5r.nexus_read( grpnm + "/number_of_triangle_sets", n_files ) != MYHDF5_SUCCESS ) { return false; }
			cout << "n_files " << n_files << "\n";

			if ( n_files > 0 ) {
				DistancingTasks.back().trimeshes = vector<trimesh_info>();

				for( apt_uint i = 0; i < n_files; i++ ) {
					string subgrpnm = grpnm + "/triangle_set" + to_string(i + 1);

					DistancingTasks.back().trimeshes.push_back( trimesh_info() );
					string file_name = "";
					if ( h5r.nexus_read( subgrpnm + "/path", file_name ) != MYHDF5_SUCCESS ) { return false; }
					DistancingTasks.back().trimeshes.back().h5fn = file_name;
					cout << "Load triangle data from " << DistancingTasks.back().trimeshes.back().h5fn << "\n";
					string dsnm = "";
					if ( h5r.nexus_read( subgrpnm + "/vertices", dsnm ) != MYHDF5_SUCCESS ) { return false; }
					DistancingTasks.back().trimeshes.back().dsnm_verts = dsnm;
					cout << "Load vertex coordinates from " << DistancingTasks.back().trimeshes.back().dsnm_verts << "\n";
					dsnm = "";
					if ( h5r.nexus_read( subgrpnm + "/indices", dsnm ) != MYHDF5_SUCCESS ) { return false; }
					DistancingTasks.back().trimeshes.back().dsnm_facets = dsnm;
					cout << "Load facet indices from " << DistancingTasks.back().trimeshes.back().dsnm_facets << "\n";
					/*
					//if ( h5r.nexus_path_exists( subgrpnm + "/vertex_normals" ) == true ) {
					dsnm = "";
					if ( h5r.nexus_read( subgrpnm + "/vertex_normals", dsnm ) != MYHDF5_SUCCESS ) {
						DistancingTasks.back().trimeshes.back().dsnm_vnormals = "";
					}
					else {
						DistancingTasks.back().trimeshes.back().dsnm_vnormals = dsnm;
					}
					cout << "Load vertex normals from " << DistancingTasks.back().trimeshes.back().dsnm_vnormals << "\n";
					//}
					*/
					//if ( h5r.nexus_path_exists( subgrpnm + "/face_normals" ) == true ) {
					dsnm = "";
					if ( h5r.nexus_read( subgrpnm + "/face_normals", dsnm ) != MYHDF5_SUCCESS ) {
						DistancingTasks.back().trimeshes.back().dsnm_fnormals = "";
					}
					else {
						DistancingTasks.back().trimeshes.back().dsnm_fnormals = dsnm;
					}
					cout << "Load face normals from " << DistancingTasks.back().trimeshes.back().dsnm_fnormals << "\n";
					//}
					/*
					//if ( h5r.nexus_path_exists( subgrpnm + "/patch_identifier" ) == true ) {
					dsnm = "";
					if ( h5r.nexus_read( subgrpnm + "/patch_identifier", dsnm ) != MYHDF5_SUCCESS ) {
						DistancingTasks.back().trimeshes.back().dsnm_patchid = "";
					}
					else {
						DistancingTasks.back().trimeshes.back().dsnm_patchid = dsnm;
					}
					cout << "Load triangle cluster indices from " << DistancingTasks.back().trimeshes.back().dsnm_patchid << "\n";
					//}
					*/
					if( h5r.nexus_path_exists(subgrpnm + "/patch_filter") == true ) {
						vector<apt_uint> uint;
						if ( h5r.nexus_read( subgrpnm + "/patch_filter/match", uint ) != MYHDF5_SUCCESS ) { return false; }
						string filter_kind = "";
						if ( h5r.nexus_read( subgrpnm + "/patch_filter/method", filter_kind ) != MYHDF5_SUCCESS ) { return false; }
						DistancingTasks.back().trimeshes.back().patch_identifier_filter =
								match_filter<apt_uint>( uint, filter_kind );
						cout << DistancingTasks.back().trimeshes.back().patch_identifier_filter << "\n";
					}
				} //read triangle set from the next file
			}
			else {
				cerr << "At least a single dataset with triangles need to be specified !" << "\n";
				return false;
			}

			grpnm = "/entry" + to_string(entry_id) + "/point_to_triangle";
			string method = "";
			if ( h5r.nexus_read( grpnm + "/method", method ) != MYHDF5_SUCCESS ) { return false; }
			if ( method.compare("default") == 0 ) {
				DistancingMethod = DISTANCING_COMPLETE;
				cout << "Computing distance of ions irregardless the triangle set !" << "\n";
			}
			else if ( method.compare("skin") == 0 ) {
				DistancingMethod = DISTANCING_SKIN;
				apt_real radius = MYZERO;
				if ( h5r.nexus_read( grpnm + "/threshold_distance", radius )  != MYHDF5_SUCCESS ) { return false; }
				DistancingRadiusMax = radius;
				cout << "Computing distance of ions only within a skin/threshold proximity of " << DistancingRadiusMax << " nm to the triangle set !" << "\n";
			}
			else {
				cerr << "Unknown distancing method !" << "\n"; return false;
			}
		}

		cout << DistancingTasks.back() << "\n";

		//sensible defaults
		cout << "ItrvQryDistEpsilon " << ItrvQryDistEpsilon << " nm" << "\n";
		cout << "ItrvQryDistIfoHdlBinWidthMin " << ItrvQryDistIfoHdlBinWidthMin << " nm" << "\n";
		if ( AdvDistancingBinWidthRange.is_valid_first_quadrant() == false ) {
			cerr << "AdvDistancingBinWidthRange needs to be a non-degenerate interval in the first quadrant !" << "\n";
			return false;
		}
		cout << "AdvDistancingBinWidthRange " << AdvDistancingBinWidthRange << "\n";
		cout << "IOStoreDistances " << IOStoreDistances << "\n";

		return true;
	}
	
	//special developer case SimID == 0

	return false;
}
