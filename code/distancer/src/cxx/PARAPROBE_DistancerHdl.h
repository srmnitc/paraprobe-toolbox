/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_DISTANCER_HDL_H__
#define __PARAPROBE_DISTANCER_HDL_H__


#include "PARAPROBE_DistancerItrvQryDistIfoHdl.h"


class distancerHdl : public mpiHdl
{
	//process-level class which implements the worker instance
public:
	distancerHdl();
	~distancerHdl();

	bool read_relevant_input();

	bool crop_reconstruction_to_analysis_window();
	bool compute_query_distances( distancing_task const & info );
	bool compute_exact_distances( distancing_task const & info );

	void execute_distancing_workpackage();

	bool write_results_distancing_task_h5( distancing_task const & info );

	vector<tri3d> triangles;						//triangles to which compute distances of positions in xyz
	vector<p3d> triangles_signed_normals;
	vector<apt_uint> triangles_patchid;

	vector<apt_real> distances;						//computed distance field
	vector<apt_uint> triangle_id;					//closest triangle with which the distance was computed
	vector<apt_real> querydist;						//value of the query distance used for every point to reduce individually number of triangles to evaluate per point

	vector<unsigned char> window;					//the portion of the dataset that we want to analyze

	tritreeHdl bvh;
	itrvQryTreeHdl distifo_tree;

	profiler dist_tictoc;
};


#endif

