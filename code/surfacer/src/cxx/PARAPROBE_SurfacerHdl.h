/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_SURFACER_HDL_H__
#define __PARAPROBE_SURFACER_HDL_H__

#include "PARAPROBE_SurfacerMesher.h"


class surfacerHdl : public mpiHdl
{
	//process-level class which implements the worker instance

public:
	surfacerHdl();
	~surfacerHdl();

	bool read_relevant_input();
	
	bool crop_reconstruction_to_analysis_window();
	bool write_analysis_window();
	void identify_ioncloud_dimensions();
	void identify_edge_approximately( surface_detection_task const & tsk );

	//identify which ions lay within a voxel kernel of size
	//(1+2*EdgeDetectionKernelWidth)^3 at the edge of the dataset
	void compute_dataset_edge( surface_detection_task const & info );

	oiMask ca;
	vector<p3d> ions_at_approximate_edge;

	/*
	bool write_alphashape_task_info_h5( surface_detection_task & info, rangeTable & rng );
	*/
	/*
	//##MK::####
	bool write_alphashape_set_geometry( surface_detection_task & info );
	*/
	
	//the portion of the dataset that we want to analyse
	vector<unsigned char> window;

	//the current alpha shape
	alphaShape ashp;

	profiler surf_tictoc;
};

#endif
