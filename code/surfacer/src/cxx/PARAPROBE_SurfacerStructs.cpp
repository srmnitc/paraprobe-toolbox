/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_SurfacerStructs.h"


ostream& operator<<(ostream& in, ashape_info const & val)
{
	in << "ncomponents " << val.ncomponents << "\n";
	in << "ninteriortets " << val.ninteriortets << "\n";
	in << "alpha " << val.alpha << " nm" << "\n";
	in << "offset " << val.offset << " nm" << "\n";
	in << "volume interior tetrahedra " << val.volume_interior_tetrahedra << " nm^3" << "\n";
	in << "volume watertight mesh " << val.volume_watertight_mesh << " nm^3" << "\n";
	in << "delaunay " << val.dt_delaunay << " s" << "\n";
	in << "a-shape compute " << val.dt_ashape_compute << " s" << "\n";
	in << "a-shape filter " << val.dt_ashape_filter << " s" << "\n";
	in << "a-shape get prims " << val.dt_ashape_getprims << " s" << "\n";
	in << "a-shape get vol " << val.dt_ashape_getvol << " s" << "\n";
	in << "a-shape get alphas " << val.dt_ashape_getalphas << " s" << "\n";
	if ( val.is_watertight == true )
		in << "a-shape exterior facet set is watertight " << "yes" << "\n";
	else
		in << "a-shape exterior facet set is watertight " << "no" << "\n";
	if ( val.is_closed == true )
		in << "a-shape exterior facet set can be used to form a closed mesh " << "yes" << "\n";
	else
		in << "a-shape exterior facet set can be used to form a closed mesh " << "no" << "\n";
	return in;
}


ostream& operator<<(ostream& in, p3d64 const & val)
{
	in << "x/y/z " << setprecision(32) << val.x << ", " << val.y << ", " << val.z << "\n";
	return in;
}
