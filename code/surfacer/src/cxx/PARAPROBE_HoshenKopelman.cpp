/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/


/*
	Implements functionalities to detect whether the microstructure has a percolating network of RX grains
	utilizes the Hoshen-Kopelman algorithm in 3D and implements a pathcompressed weighted union/find algorithm based
	on Kartik Kukreja's implementation http://kartikkukreja.wordpress.com
	Markus K\"uhbach, m.kuehbach (at) mpie.de

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_HoshenKopelman.h"


UF::UF( const apt_uint N )
{
	stillgood = true;
	id = NULL;
	try {
		id = new apt_uint[N];
	}
	catch (std::bad_alloc &hkexc) {
		cerr << "HoshenKopelman Union/Find Algorithm unable to allocate a hash container (id) for compressed union/find !" << "\n";
		stillgood = false;
	}
	sz = NULL;
	try {
		sz = new apt_uint[N];
	}
	catch (std::bad_alloc &hkexc) {
		cerr << "HoshenKopelman Union/Find Algorithm unable to allocate a hash container (sz) for compressed union/find !" << "\n";
		stillgood = false;
	}
	for( apt_uint i = 0; i < N; ++i ) {
		id[i] = i;
	}
	for( apt_uint i = 0; i < N; ++i ) {
		sz[i] = 1;
	}
}


UF::~UF()
{
	if ( id != NULL ) {
		delete [] id; id = NULL;
	}
	if ( sz != NULL ) {
		delete [] sz; sz = NULL;
	}
	stillgood = false;
}


apt_uint UF::initialAssgn( const apt_uint i )
{
	return i;
}


apt_uint UF::find_root( apt_uint p )
{
	apt_uint root = p;
	while (root != id[root])
		root = id[root]; //traversal operation because for a root node i == id[i] holds
	while (p != root) { //path compression
		apt_uint newp = id[p];
		id[p] = root;
		p = newp;
	}
	return root;
}


apt_uint UF::merge( apt_uint x, apt_uint y )
{
	apt_uint i = find_root(x);
	apt_uint j = find_root(y);
	if ( i == j )
		return i; //MK::return simply one...

	// make smaller root point to larger one
	if (sz[i] < sz[j])	{ //eq class j larger than eq class i
		id[i] = j;
		sz[j] += sz[i];
		return j;
	}
	else { //eq class j smaller or equal to eq class i
		id[j] = i;
		sz[i] += sz[j];
		return i;
	}
}


bool UF::still_good( void )
{
	return stillgood;
}


ostream& operator<<(ostream& in, hk_info const & val)
{
	in << "ncluster " << val.ncluster << "\n";
	in << "largest_npoints " << val.largest_npoints << "\n";
	in << "largest_id " << val.largest_id << "\n";
	in << "dt init " << val.dt_initializing << " s" << "\n";
	in << "dt init " << val.dt_kopeling << " s" << "\n";
	in << "dt init " << val.dt_compactifying << " s" << "\n";
	in << "dt init " << val.dt_checking << " s" << "\n";
	in << "dt init " << val.dt_characterizing << " s" << "\n";
	return in;
}


percolationAnalyzer::percolationAnalyzer()
{
	idd = NULL;
	finder = NULL;
	set_gridsize( 0, 0, 0 );
	info = hk_info();
};


percolationAnalyzer::~percolationAnalyzer()
{
	delete [] idd; idd = NULL;
	delete finder; finder = NULL;
	set_gridsize( 0, 0, 0 );
	info = hk_info();
};


void percolationAnalyzer::set_gridsize( const apt_uint nx, const apt_uint ny, const apt_uint nz )
{
	if ( nx < (size_t) UMX && ny < (size_t) UMX && nz < (size_t) UMX ) {
		if ( nx*ny < (size_t) UMX ) {
			if ( nx*ny*nz < (size_t) UMX ) {
				NX = nx;
				NY = ny;
				NZ = nz;
				NXY = NX*NY;
				NXYZ = NXY*NZ;
				return;
			}
		}
	}
	cerr << "percolationAnalyzer::setGridsize an unexpectedly too large grid was requested !" << "\n";
	NX = 0; NY = 0; NZ = 0; NXY = 0; NXYZ = 0;
}


bool percolationAnalyzer::initialize( const bool* inputdata, const apt_uint nxx, const apt_uint nyy, const apt_uint nzz )
{
	double tic = omp_get_wtime();

	set_gridsize( nxx, nyy, nzz );

	cout << "Preconditioning for elimination of false negatives inside tip volume" << "\n";
	cout << "3D, SC site perc NX/Y/Z = " << NX << ";" << NY << ";" << NZ << "\n";
	cout << "\t\tInitializing..." << "\n";

	//get memory for the HK analysis and the union/find
	try {
		idd = new apt_uint[NXYZ];
	}
	catch (bad_alloc &hkexc) {
		cerr << "percolationAnalyzer::initialize unable to allocate a label ID field !" << "\n";
		return false;
	}
	try {
		finder = new UF(NXYZ);
	}
	catch (bad_alloc &hkexc) {
		cerr << "percolationAnalyzer::initializer unable to allocate a finder class instance !" << "\n";
		return false;
	}
	//##MK::consider optimize as this is a very large preallocation

	for ( apt_uint c = 0; c < NXYZ; ++c ) {
		//MK::BE CAREFUL HERE WE WOULD LIKE TO DETECT CLUSTER OF percolation FALSE'S indicating vacuum (outside) or connected bins inside given object (here ioncloud volume)
		//thus we invert original concept for RX find percolating RX cluster //id field 0 (false, no ions in bin), 1 (true, ions in bin)
		/*
		if ( inputdata[c] == false )
			idd[c] = 1;
		else 
			idd[c] = 0;
		*/
		//##MK::cluster of percolating TRUE's of vacuum
		idd[c] = ( inputdata[c] == false ) ? 1 : 0; //now every voxel with ions is 0 and every voxel in the vacuum at the edge and some voxel in the center
		//that have by coincidence no ions either are 1 (above defined "flicker")
	}
	//cout << "\t\tHoshenKopelman::LabelingInitialized" << "\n";
	double toc = omp_get_wtime();
	info.dt_initializing = (toc-tic);
	return true;
}


void percolationAnalyzer::hk3d_core_nonperiodic( const apt_uint x, const apt_uint y, const apt_uint z )
{
	//in HK algo we walk sequentially through the grid the idd field keeps binarized information which voxels are vacuum
	//then idd[xyz] == 1 i.e. a voxel with initially vacuum being false
	//successively exchange entries in idd by labels of clusters
	//the label of the cluster is the value of voxel first encountered with that new label
	if ( idd[x+y*NX+z*NXY] == 1 ) { //only RXed (1) to analyze for percolation, periodic boundaries
		//by virtue of construction and the way this function is called percolationAnalyzer::hoshen_kopelman you start at false vacuum ie in true vacuum
		////apt_uint front = ( z == 0 ? 0 : idd[x+y*NX+(z-1)*NXY] );
		////apt_uint bottom = ( y == 0 ? 0 : idd[x+(y-1)*NX+z*NXY] );
		////apt_uint left = ( x == 0 ? 0 : idd[(x-1)+y*NX+z*NXY] );
		apt_uint front = ( z != 0 ) ? idd[x+y*NX+(z-1)*NXY] : 0; //if z is at boundary we know we are in a see of true vacuum, so we place 1
		apt_uint bottom = ( y != 0 ) ? idd[x+(y-1)*NX+z*NXY] : 0;
		apt_uint left = ( x != 0 ) ? idd[(x-1)+y*NX+z*NXY] : 0;

		/*
		apt_uint front = ( z != 0 ) ? idd[x+y*NX+(z-1)*NXY] : 1; //not at the edge of the container, take existent label
		//but if we are at any edge voxel we know we have a non-periodic voxelization and we know that this containers is
		//embedded in a sea of true vacuum by virtue of construction
		apt_uint bottom = ( y != 0 ) ? idd[x+(y-1)*NX+z*NXY] : 1;
		apt_uint left = ( x != 0 ) ? idd[(x-1)+y*NX+z*NXY] : 1;
		*/

		bool bl = false;
		bool bb = false;
		bool bf = false;
		if ( left > 0 ) bl = true; //read it like if the left nbor is relevant because it is not at the edge and not within idd of value 0 consider it
		//tricky here is that the value 0 is special and marks two qualities, that we are at the edge or that we are in a voxel where the initial isvacuum was set to true
		if ( bottom > 0 ) bb = true;
		if ( front > 0 ) bf = true;

		//tip covers most of the containers 
		//##MK::consider in further optimization that the likelihood for these cases is a function of the RX fraction to be able to test less ifs
		//##MK::utilize a checksum like bl*100+bb*10+bf*1 in a switch command maybe more efficient...
		//case 000 all neighbors are not-RXed
		if ( bl == false && bb == false && bf == false ) { //because UF assumes initially already NXYZ disjoint labels!
			idd[x+y*NX+z*NXY] = finder->initialAssgn( x+y*NX+z*NXY );
			return;
		}

		if ( bl == true && bb == false && bf == false ) { //100
			idd[x+y*NX+z*NXY] = left; return;
		}

		if ( bl == false && bb == true && bf == false ) { //010
			idd[x+y*NX+z*NXY] = bottom; return;
		}

		if ( bl == false && bb == false && bf == true ) { //001
			idd[x+y*NX+z*NXY] = front; return;
		}

		if ( bl == true && bb == true && bf == false ) { //110
			idd[x+y*NX+z*NXY] = this->finder->merge( left, bottom );
			return;
		}
		if ( bl == true && bb == false && bf == true ) { //101
			idd[x+y*NX+z*NXY] = this->finder->merge( left, front );
			return;
		}
		if ( bl == false && bb == true && bf == true ) { //011
			idd[x+y*NX+z*NXY] = this->finder->merge( bottom, front );
			return;
		}
		if ( bl == true && bb == true && bf == true ) {
			//##MK::check if correct?
			//apt_uint cand1 = this->finder->merge( left, bottom );
			//apt_uint cand2 = this->finder->merge( left, front );
			apt_uint cand3 = this->finder->merge( bottom, front );
			idd[x+y*NX+z*NXY] = cand3; //because call for cand1 and cand2 modify tree representation
		}
	} //next cell
}


bool percolationAnalyzer::hoshen_kopelman()
{
	double tic = omp_get_wtime();
	//cout << "\t\tInitial identification of true's cluster..." << "\n";
	for( apt_uint zz = 0; zz < NZ; zz++ ) {
		for( apt_uint yy = 0; yy < NY; yy++ ) {
			for( apt_uint xx = 0; xx < NX; xx++ ) {
				hk3d_core_nonperiodic( xx, yy, zz );
			}
		}
	}

	double toc = omp_get_wtime();
	info.dt_kopeling = (toc-tic);
	return true;
}

bool percolationAnalyzer::compactify()
{
	double tic = omp_get_wtime();

	//cout << "\t\tCompactifying..." << "\n";
	map<apt_uint, apt_uint> old2new;
	map<apt_uint, apt_uint>::iterator which;
	apt_uint nnew = 0;
	apt_uint croot = 0;

	for ( apt_uint c = 0; c < NXYZ; c++ ) {
		if ( idd[c] > 0 ) { //MK::leaves in our case bins with ions untouched because these where unmasked in the initialize and leaves the 0,0,0 voxel vacuum in the guard out
			croot = finder->find_root( idd[c] );
			which = old2new.find( croot );
			if ( which != old2new.end() ) { //key found
				idd[c] = which->second;
			}
			else {
				nnew++;
				old2new[croot] = nnew;
				idd[c] = nnew;
			}
		}
	}

	info.ncluster = nnew;
	//##MK::change format of verbosing
	//cout << "\t\tTotal number of clusters = " << to_string(results.nClusterTotal) << "\n";
	double toc = omp_get_wtime();
	info.dt_compactifying = (toc-tic);
	return true;
}


bool percolationAnalyzer::check_labeling()
{
	double tic = omp_get_wtime();

	//cout << "\t\tChecking the correct labeling with the compactified IDs..." << "\n";
	//bool labelingerror = false;
	apt_uint LE,RI,FR,RE,BO,TO;
	for ( apt_uint z = 0; z < NZ; z++ ) {
		for (apt_uint y = 0; y < NY; y++ ) {
			for ( apt_uint x = 0; x < NX; x++ ) {
				if ( idd[x+y*NX+z*NXY] > 0 ) {
					//original Tobin Fricke i-->y, j --> x working on NX unit cube in R^3
					/*
					LE = ( x == 0 	? id[(N-1)+y*N+z*NN]	: id[(x-1)+y*N+z*NN] );
					RI = ( x == N-1	? id[0+y*N+z*NN] 		: id[(x+1)+y*N+z*NN] );
					FR = ( y == 0	? id[x+(N-1)*N+z*NN]	: id[x+(y-1)*N+z*NN] );
					RE = ( y == N-1	? id[x+0*N+z*NN]		: id[x+(y+1)*N+z*NN] );
					BO = ( z == 0	? id[x+y*N+(N-1)*NN]	: id[x+y*N+(z-1)*NN] );
					TO = ( z == N-1	? id[x+y*N+0*NN]		: id[x+y*N+(z+1)*NN] );
					*/
					//non-periodic domain
					LE = ( x == 0 		? 0	: idd[(x-1)+y*NX+z*NXY] );
					RI = ( x == NX-1	? 0	: idd[(x+1)+y*NX+z*NXY] );
					FR = ( y == 0		? 0	: idd[x+(y-1)*NX+z*NXY] );
					RE = ( y == NY-1	? 0	: idd[x+(y+1)*NX+z*NXY] );
					BO = ( z == 0		? 0	: idd[x+y*NX+(z-1)*NXY] );
					TO = ( z == NZ-1	? 0	: idd[x+y*NX+(z+1)*NXY] );
							
					apt_uint cand = idd[x+y*NX+z*NXY]; //von Neumann nearest neighbors must have the same label if they are not 0!
					//MK::PRODUCTION
					/*
					if ( LE != 0 && LE != cand ) return false; //labelingerror = true; //cerr << "LE error " << x << ";" << y << ";" << z << "\t\t" << LE << "\t\t" << idd[x+y*NX+z*NXY] << "\n"; return false; }
					if ( RI != 0 && RI != cand ) return false; //labelingerror = true; //cerr << "RI error " << x << ";" << y << ";" << z << "\t\t" << RI << "\t\t" << idd[x+y*NX+z*NXY] << "\n"; return false; }
					if ( FR != 0 && FR != cand ) return false; //labelingerror = true; //cerr << "FR error " << x << ";" << y << ";" << z << "\t\t" << FR << "\t\t" << idd[x+y*NX+z*NXY] << "\n"; return false; }
					if ( RE != 0 && RE != cand ) return false; //labelingerror = true; //cerr << "RE error " << x << ";" << y << ";" << z << "\t\t" << RE << "\t\t" << idd[x+y*NX+z*NXY] << "\n"; return false; }
					if ( BO != 0 && BO != cand ) return false; //labelingerror = true; //cerr << "BO error " << x << ";" << y << ";" << z << "\t\t" << BO << "\t\t" << idd[x+y*NX+z*NXY] << "\n"; return false; }
					if ( TO != 0 && TO != cand ) return false; //labelingerror = true; //cerr << "TO error " << x << ";" << y << ";" << z << "\t\t" << TO << "\t\t" << idd[x+y*NX+z*NXY] << "\n"; return false; }
					*/

					//##MK::DEBUG
					if ( LE != 0 && LE != cand ) { cerr << "LE error " << x << ";" << y << ";" << z << "\t\t" << LE << "\t\t" << cand << "\n"; return false; }
					if ( RI != 0 && RI != cand ) { cerr << "RI error " << x << ";" << y << ";" << z << "\t\t" << RI << "\t\t" << cand << "\n"; return false; }
					if ( FR != 0 && FR != cand ) { cerr << "FR error " << x << ";" << y << ";" << z << "\t\t" << FR << "\t\t" << cand << "\n"; return false; }
					if ( RE != 0 && RE != cand ) { cerr << "RE error " << x << ";" << y << ";" << z << "\t\t" << RE << "\t\t" << cand << "\n"; return false; }
					if ( BO != 0 && BO != cand ) { cerr << "BO error " << x << ";" << y << ";" << z << "\t\t" << BO << "\t\t" << cand << "\n"; return false; }
					if ( TO != 0 && TO != cand ) { cerr << "TO error " << x << ";" << y << ";" << z << "\t\t" << TO << "\t\t" << cand << "\n"; return false; }

				} //label consistency for cluster cell x,y,z checked
			}
		}
	}
	//not yet an inconsistency found?
	double toc = omp_get_wtime();
	info.dt_checking = (toc-tic);
	return true;
}


bool percolationAnalyzer::determine_clustersize_distr()
{
	double tic = omp_get_wtime();

	//determine cluster size distribution first
	if ( info.ncluster < 1 ) {
		cerr << "No cluster were detected so nothing to determine a size distribution from!" << "\n";
		return false;
	}

	double toc = omp_get_wtime();
	info.dt_characterizing = (toc-tic);
	return true;
}


bool percolationAnalyzer::rebinarize( const apt_uint target, const apt_uint nsz, bool* bitmap )
{
	//get label at voxel with implicit coordinate target
	apt_uint vacuum_cluster_id = idd[target];
	cout << "\t\tRebinarizing against label " << vacuum_cluster_id << "\n";

	bitmap[0] = true; //by virtue of construction this is really vacuum because in the ghost layer!
	for ( apt_uint b = 1; b < nsz; ++b ) {
		if ( idd[b] == vacuum_cluster_id ) //so the percolating cluster of true vacuum excluding the flicker inside the dataset
			//because the flicker is not connect percolating to this cluster
			bitmap[b] = true;
		else //really an occupied voxel or a flicker in the center
			bitmap[b] = false;
	}

	//cout << "\t\tRebinarization successful!" << "\n";
	return true;
}
